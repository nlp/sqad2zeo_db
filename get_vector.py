#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
import fasttext
import os
import sys
from transformers import BertTokenizer, BertConfig, BertModel

dir_path = os.path.dirname(os.path.realpath(__file__))

class Word2vec:
    def __init__(self, dim='all'):
        """
        Load pretrained models
        """
        if dim == 'all' or dim == '100':
            self.model_100 = fasttext.load_model(f'{dir_path}/../../fasttext/models/cstenten_17_100_5_5_0.05_skip')
        if dim == 'all' or dim == '300':
            self.model_300 = fasttext.load_model(f'{dir_path}/../../fasttext/models/cstenten_17_300_5_5_0.05_skip')
        if dim == 'all' or dim == '500':
            self.model_500 = fasttext.load_model(f'{dir_path}/../../fasttext/models/cstenten_17_500_5_5_0.05_skip')

        # Bert embedding from slavic bert
        if dim == 'all' or dim == 'slavic_bert':
            bert_config = BertConfig.from_json_file(
                f'{dir_path}/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/bert_config.json')
            self.model_slavic_bert = BertModel.from_pretrained(
                f'{dir_path}/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/pytorch_model.bin',
                config=bert_config, local_files_only=True)
            self.tokenizer_slavic_bert = BertTokenizer.from_pretrained(
                f'{dir_path}/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/')

    def word2embedding_cls(self, word):
        """
        CLS token form bert output
        :param word: str, input word
        :return: vector
        """
        input_ids = self.tokenizer_slavic_bert.encode(word, return_tensors="pt", add_special_tokens=True)
        output = self.model_slavic_bert(input_ids)

        return output[0][0][0].detach().tolist()

    def word2embedding_average(self, word, verbose=False):
        """
        Average of sub-word embeddings
        :param word: str, input word
        :param verbose: debugging mode
        :return: vector
        """
        input_ids = self.tokenizer_slavic_bert.encode(word, return_tensors="pt", add_special_tokens=False)
        try:
            output = self.model_slavic_bert(input_ids)

            if verbose:
                input_ids_2 = self.tokenizer_slavic_bert.encode(word, add_special_tokens=False)
                for idx, i in enumerate(input_ids_2):
                    print(f'{self.tokenizer_slavic_bert.decode(i)} -> {i}')

            average_tensor = []
            for i in output[0][0]:
                average_tensor.append(i.detach().tolist())
            average_tensor = [sum(items)/len(average_tensor) for items in zip(*average_tensor)]

        except RuntimeError:
            sys.stderr.write(f'Cant create embedding for word: {word}\n')
            average_tensor = [0] * 768

        return average_tensor

    def get_vector(self, word, dim):
        """
        Return vector according dimension
        :param word: str
        :param dim: int
        :return: list
        """
        if dim == '100':
            return self.model_100[word].tolist()
        elif dim == '300':
            return self.model_300[word].tolist()
        elif dim == '500':
            return self.model_500[word].tolist()
        elif dim == 'slavic_bert':
            return self.word2embedding_average(word)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Word2vec fastext')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    args = parser.parse_args()
    w2v = Word2vec()
    result_vector =  w2v.get_vector(args.input.read(), '100')
    print(result_vector)
    print(len(result_vector))
    print(type(result_vector))


if __name__ == "__main__":
    main()
