#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
# according https://github.com/huggingface/transformers/issues/1950
from sqad_db import SqadDb
from sqad_db import id2word
import sys
import persistent.list
import transaction
import torch
import datetime
import re
import timeit
import os


dir_path = os.path.dirname(os.path.realpath(__file__))
device = "cuda:0" if torch.cuda.is_available() else "cpu"

class S2SlavicBert:
    def __init__(self):
        """
        Load BERT models
        """
        from transformers import BertTokenizer
        from transformers import BertConfig
        from transformers import BertModel

        config = BertConfig.from_json_file(
            f'{dir_path}/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/bert_config.json')
        self.model = BertModel.from_pretrained(
            f'{dir_path}/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/pytorch_model.bin',
            config=config, local_files_only=True).to(device)

        self.tokenizer = BertTokenizer.from_pretrained(
            f'{dir_path}/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/')

    def get_sent_embedding(self, sentence, verbose=False):
        """
        Get BERT sentence embedding from CLS token
        :param sentence:
        :param verbose:
        :return:
        """
        input_ids = self.tokenizer.encode(sentence, return_tensors="pt", add_special_tokens=True).to(device)
        if verbose:
            input_ids_2 = self.tokenizer.encode(sentence, add_special_tokens=True)
            for i in input_ids_2:
                print(f'{self.tokenizer.decode(i)} -> {i}')
        outputs = self.model(input_ids)
        cls_emb = outputs[0][0][0].cpu().detach().tolist()
        return cls_emb


class S2RobeCzech:
    def __init__(self):
        """
        Load BERT models
        """
        from transformers import RobertaModel
        from transformers import RobertaTokenizer

        self.model = RobertaModel.from_pretrained('ufal/robeczech-base').to(device)
        self.tokenizer = RobertaTokenizer.from_pretrained('ufal/robeczech-base')

    def get_sent_embedding(self, sentence, verbose=False):
        """
        Get BERT sentence embedding from CLS token
        :param sentence:
        :param verbose:
        :return:
        """
        input_ids = self.tokenizer.encode(sentence, return_tensors="pt", add_special_tokens=True).to(device)
        if verbose:
            input_ids_2 = self.tokenizer.encode(sentence, add_special_tokens=True)
            for i in input_ids_2:
                print(f'{self.tokenizer.decode(i)} -> {i}')
        outputs = self.model(input_ids)
        cls_emb = outputs[0][0][0].cpu().detach().tolist()
        return cls_emb


class S2Czert:
    def __init__(self):
        """
        Load BERT models
        """
        from transformers import AutoTokenizer
        from transformers import TFBertModel

        self.model = TFBertModel.from_pretrained('UWB-AIR/Czert-B-base-cased')
        self.tokenizer = AutoTokenizer.from_pretrained('UWB-AIR/Czert-B-base-cased')

    def get_sent_embedding(self, sentence, verbose=False):
        """
        Get BERT sentence embedding from CLS token
        :param sentence:
        :param verbose:
        :return:
        """
        input_ids = self.tokenizer.encode(sentence, return_tensors="tf", add_special_tokens=True)
        if verbose:
            input_ids_2 = self.tokenizer.encode(sentence, add_special_tokens=True)
            for i in input_ids_2:
                print(f'{self.tokenizer.decode(i)} -> {i}')
        outputs = self.model(input_ids)
        cls_emb = outputs[0][0][0].cpu().numpy().tolist()
        return cls_emb


class SentenceBert:
    def __init__(self):
        """
        Load BERT models
        """
        from sentence_transformers import SentenceTransformer
        self.model = SentenceTransformer('paraphrase-xlm-r-multilingual-v1')

    def get_sent_embedding(self, sentence, verbose=False):
        """
        Get BERT sentence embedding from CLS token
        :param sentence:
        :param verbose:
        :return:
        """
        return self.model.encode(sentence).tolist()


def add_vector_text(db, model, compute_vector, name, ctx, verbose):
    """
    Add sentence vector to text and title
    :param db: ZODB object
    :param model: vector model
    :param compute_vector: function
    :param verbose: bool
    :return: None
    """
    vocabulary, _, kb = db.get_dicts()
    db_size = len(kb.url2doc)
    id = 1

    for url, text in kb.url2doc.items():
        sys.stderr.write(f'============== {url} ============\n')
        data_size = 0
        start = timeit.timeit()
        data_size += compute_vector(text['text'], model, vocabulary, name, ctx, verbose)
        data_size += compute_vector(text['title'], model, vocabulary, name, False, verbose)

        db._p_changed = True
        transaction.commit()

        end = timeit.timeit()
        sys.stderr.write(f'{name}: {id}/{db_size} time:{end - start} size: {data_size}\n')
        id += 1


def add_vector_record(db, model, compute_vector, name, verbose):
    """
    Add sentence vector to record parts (question, answer, exact answer)
    :param db: ZODB object
    :param model: vector model
    :param compute_vector: function
    :param verbose: bool
    :return: None
    """
    vocabulary, _, kb = db.get_dicts()
    for rid in db.get_all_records_id():
        record = db.get_record(rid)
        compute_vector(record.question, model, vocabulary, name, False, verbose)
        compute_vector(record.answer_selection, model, vocabulary, name, False, verbose)
        compute_vector(record.answer_extraction, model, vocabulary, name, False, verbose)
        db._p_changed = True
        transaction.commit()


def compute_vector(data, model, vocabulary, name, ctx, verbose):
    """
    Add BERT vector to sentence
    :param data: dict
    :param model: S2ClsBert object
    :param vocabulary: dict
    :param ctx: bool
    :param verbose: bool
    :param name: str, name of used model
    :return: None
    """
    data_size = 0
    sent_no = len(data)
    s_idx = 1
    for sentence in data:
        sys.stderr.write(f'sentence {s_idx}/{sent_no}; {"with context" if ctx else "NO context"}\n')
        s_content = []
        for w_id in sentence['sent']:
            s_content.append(id2word(vocabulary, w_id, parts='w')['word'])
        sent_v = model.get_sent_embedding(' '.join(s_content), verbose=verbose)
        data_size += sys.getsizeof(sent_v)
        sentence[name] = persistent.list.PersistentList(sent_v)

        if verbose:
            print(f"s: {' '.join(s_content)}\t{sent_v[:10]}")

        if ctx:
            sys.stderr.write(f'computing ctx\n')
            for ctx_type, ctx_sents in sentence['ctx'].items():
                if re.match('(name_phrs.*)', ctx_type):
                    for s_phr in ctx_sents:
                        phr_content = []
                        for w_id in s_phr['sent']:
                            phr_content.append(id2word(vocabulary, w_id, parts='w')['word'])
                        if phr_content:
                            phr_sent_v = model.get_sent_embedding(' '.join(phr_content), verbose=verbose)
                            s_phr[name] = persistent.list.PersistentList(phr_sent_v)
                        else:
                            s_phr[name] = persistent.list.PersistentList([0]*768)
                        data_size += sys.getsizeof(s_phr[name])
        s_idx += 1

    return data_size



def main():
    import argparse
    parser = argparse.ArgumentParser(description='Compute CLS-BERT sentence embeddings')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-d', '--db_path', type=str,
                        required=False, default='',
                        help='Database path')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False,
                        help='Verbose mode')
    parser.add_argument('--ctx', action='store_true',
                        required=False, default=False,
                        help='Embeddings also in context')
    args = parser.parse_args()

    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    try:
        sys.stderr.write(f'=========== sentence_bert part start {datetime.datetime.now()}=============\n')
        model_sentence_bert = SentenceBert()
        add_vector_text(db, model_sentence_bert, compute_vector, 's_bert', ctx=args.ctx, verbose=args.verbose)
        add_vector_record(db, model_sentence_bert, compute_vector, 's_bert', verbose=args.verbose)
        sys.stderr.write(f'=========== sentence_bert part end {datetime.datetime.now()} =============\n')

        sys.stderr.write(f'=========== slavic_bert part start {datetime.datetime.now()}=============\n')
        model_slavic_bert = S2SlavicBert()
        add_vector_text(db, model_slavic_bert, compute_vector, 'slavic_bert', ctx=args.ctx, verbose=args.verbose)
        add_vector_record(db, model_slavic_bert, compute_vector, 'slavic_bert', verbose=args.verbose)
        sys.stderr.write(f'=========== slavic_bert part end {datetime.datetime.now()} =============\n')

        sys.stderr.write(f'=========== robeczech part start {datetime.datetime.now()} =============\n')
        model_robe_czech = S2RobeCzech()
        add_vector_text(db, model_robe_czech, compute_vector, 'robeczech', ctx=args.ctx, verbose=args.verbose)
        add_vector_record(db, model_robe_czech, compute_vector, 'robeczech', verbose=args.verbose)
        sys.stderr.write(f'=========== robeczech part end {datetime.datetime.now()} =============\n')

        sys.stderr.write(f'=========== czert part start {datetime.datetime.now()} =============\n')
        model_czert = S2Czert()
        add_vector_text(db, model_czert, compute_vector, 'czert', ctx=args.ctx, verbose=args.verbose)
        add_vector_record(db, model_czert, compute_vector, 'czert', verbose=args.verbose)
        sys.stderr.write(f'=========== czert part end {datetime.datetime.now()} =============\n')

        db.update()
        db._p_changed = True
        transaction.commit()
        db.close()
    except KeyboardInterrupt:
        db.close()
        sys.exit()


if __name__ == "__main__":
    main()
