#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
from sqad_db import SqadDb
from sqad_db import id2word
from sqad_db import id2qt
from pprint import pprint
import sys
import re


def print_ctx(phrs, name):
    """
    Print content of phrases
    :param phrs: dict
    :return: None
    """
    direction = re.match('.*?_(b|f)', name).group(1)

    for idx, sent_phr in enumerate(phrs):
        if direction == 'f':
            number = f'c({idx+1}):'
        elif direction == 'b':
            number = f'c(-{idx+1}):'
        for idx, p in enumerate(sent_phr):
            if idx == 0:
                print(f'\t\t\t{number} {" ".join([x["word"] for x in p["sent"]])}')
            else:
                print(f'\t\t\t{" "*len(number)} {" ".join([x["word"] for x in p["sent"]])}')


def get_ctx_sent(sent, vocabulary, part='', preloaded=False):
    """
    Transform each phrase into actual sentence with all addons
    :param phrs: dict
    :param vocabulary: dict
    :param part: str, specify word parts
    :param preloaded: preloaded data in memeory
    :return: list, lsit of phrases
    """
    result_sent = {}
    sent_content = []
    for w_id_cx in sent['sent']:
        if part:
            sent_content.append(id2word(vocabulary, w_id_cx, parts=part, preloaded=preloaded))
        else:
            sent_content.append(id2word(vocabulary, w_id_cx, preloaded=preloaded))
    result_sent['sent'] = sent_content

    result_sent['slavic_bert'] = sent.get('slavic_bert', None)
    result_sent['robeczech'] = sent.get('robeczech', None)
    result_sent['czert'] = sent.get('czert', None)
    result_sent['s_bert'] = sent.get('s_bert', None)

    return result_sent


def get_ctx(data, vocabulary, context_type, part='', preloaded=False):
    """
    Get sentence context ofr KB[text]
    :param data: dict
    :param vocabulary: dict
    :param part: str, specify word parts
    :param context_type: str, specify context type
    :param preloaded: preloaded data in memeory
    :return: dict
    """
    sentence_ctx = []
    for prev_sent in data:
        sent_part = []
        for ctx_part in prev_sent["ctx"][context_type]:
            # print(f'ctx_part["ctx"][{context_type}]: {prev_sent["ctx"][context_type]}')
            try:
                sent_part.append(get_ctx_sent(ctx_part, vocabulary, part=part, preloaded=preloaded))
            except KeyError:
                pass
        sentence_ctx.append(sent_part)
    return sentence_ctx


def get_senence(data, vocabulary, part='', preloaded=False):
    """
    Transform word ids to actual words
    :param data: list of word ids
    :param vocabulary: dict
    :param part: str, specify word parts
    :param preloaded: preloaded data in memeory
    :return:
    """
    sent = []
    for w_id in data:
        if part:
            sent.append(id2word(vocabulary, w_id, parts=part, preloaded=preloaded))
        else:
            sent.append(id2word(vocabulary, w_id, preloaded=preloaded))

    return sent


def get_sentence_content(sentence, vocabulary, part='', context_type='', preloaded=False):
    s_data = {}
    s_data['sent'] = get_senence(sentence['sent'], vocabulary, part=part, preloaded=preloaded)
    s_data['ctx'] = {}

    if sentence.get('ctx', ''):
        for ctx_type in context_type.strip().split(';'):
            if ctx_type.startswith('ctx_ner') or ctx_type.startswith('name_phrs') or ctx_type == 'all':
                if ctx_type.startswith('ctx_ner') or (ctx_type == 'all' and True in [x.startswith('ctx_ner') for x in sentence['ctx'].keys()]):
                    ner_sentence_ctx = []
                    for ctx_part in sentence['ctx']['ctx_ner']:
                        ner_sentence_ctx.append(get_ctx_sent(ctx_part, vocabulary,
                                                         part=part, preloaded=preloaded))
                    s_data['ctx'][f'ctx_ner'] = ner_sentence_ctx

                elif ctx_type.startswith('name_phrs_tlongest') or (ctx_type == 'all' and True in [x.startswith('name_phrs_tlongest') for x in sentence['ctx'].keys()]):
                    phr_sentence_ctx = []
                    for ctx_part in sentence['ctx']['name_phrs_tlongest']:
                        phr_sentence_ctx.append(get_ctx_sent(ctx_part, vocabulary,
                                                         part=part, preloaded=preloaded))
                    s_data['ctx'][f'name_phrs_tlongest'] = phr_sentence_ctx

    try:
        s_data['s_bert'] = sentence['s_bert']
    except (KeyError, TypeError):
        s_data['s_bert'] = None

    try:
        s_data['slavic_bert'] = sentence['slavic_bert']
    except (KeyError, TypeError):
        s_data['slavic_bert'] = None

    try:
        s_data['robeczech'] = sentence['robeczech']
    except (KeyError, TypeError):
        s_data['robeczech'] = None

    try:
        s_data['czert'] = sentence['czert']
    except (KeyError, TypeError):
        s_data['czert'] = None

    return s_data


def get_content_text(data, vocabulary, old, as_position, part='', context_type='', ctx_direction='a', preloaded=False,
                     title_ctx_always=False, first_sentence_always=False):
    """
    Harvest content from data
    :param data: list or dict
    :param vocabulary: dict
    :param part: str, specify word parts
    :param as_position: int, position of answer selection
    :param context_type: str, specify context type
    :param preloaded: preloaded data in memeory
    :return: list, content of data
    """

    result = []
    last_sent_idx = len(data['text']) - 1
    for sent_num, sentence in enumerate(data['text']):
        s_data = {}
        s_data['sent'] = get_senence(sentence['sent'], vocabulary, part=part, preloaded=preloaded)

        if sentence.get('ctx', ''):
            s_data['ctx'] = {}
            for ctx_type in context_type.strip().split(';'):

                # =========================
                # Get window value
                if ctx_type == 'all':
                    window = 5
                else:
                    try:
                        window = int(re.match('[^0-9]+(\d+)', ctx_type).group(1))
                    except AttributeError:
                        window = 5
                # =========================

                # =========================
                # Name Phrases and NER items
                if ctx_type.startswith('ctx_ner') or ctx_type.startswith('name_phrs') or ctx_type == 'all':
                    context_senteces_list_backward = []
                    context_senteces_list_forward = []
                    as_in_context_b = False
                    as_in_context_f = False

                    if ctx_direction in ['b', 'a']:
                        for i in range(1, window + 1):
                            if sent_num - i < 0:
                                break
                            else:
                                context_senteces_list_backward.append(data['text'][sent_num - i])
                                if as_position == sent_num - i:
                                    as_in_context_b = True

                    if ctx_direction in ['f', 'a']:
                        for i in range(1, window + 1):
                            if sent_num + i > last_sent_idx:
                                break
                            else:
                                context_senteces_list_forward.append(data['text'][sent_num + i])
                                if as_position == sent_num + i:
                                    as_in_context_f = True

                    if ctx_type.startswith('ctx_ner') or ctx_type == 'all':
                        s_data['ctx'][f'ctx_ner_b'] = get_ctx(context_senteces_list_backward, vocabulary,
                                                                        'ctx_ner', part=part, preloaded=preloaded)
                        s_data['ctx'][f'ctx_ner_f'] = get_ctx(context_senteces_list_forward, vocabulary,
                                                                        'ctx_ner', part=part, preloaded=preloaded)

                    if ctx_type.startswith('name_phrs') or ctx_type == 'all':
                        s_data['ctx'][f'name_phrs_b'] = get_ctx(context_senteces_list_backward, vocabulary,
                                                                          'name_phrs_tlongest', part=part,
                                                                          preloaded=preloaded)
                        s_data['ctx'][f'name_phrs_f'] = get_ctx(context_senteces_list_forward, vocabulary,
                                                                          'name_phrs_tlongest', part=part,
                                                                          preloaded=preloaded)

                    s_data['as_in_context_b'] = as_in_context_b
                    s_data['as_in_context_f'] = as_in_context_f
                # =========================

                # =========================
                # Previous Sentences
                if ctx_type.startswith('prev_sent') or context_type == 'all':
                    s_data['ctx'][f'prev_sent_b'] = []
                    s_data['ctx'][f'prev_sent_f'] = []
                    as_in_context_b = False
                    as_in_context_f = False

                    if title_ctx_always:
                        # ====================================================
                        # test with title as context for all sentences
                        for i in range(1, window):
                            if sent_num - i < 0:
                                break
                            else:
                                s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['text'][sent_num - i],
                                                                                             vocabulary, part=part,
                                                                                             preloaded=preloaded)])
                                if as_position == sent_num - i:
                                    as_in_context_b = True

                        # always add title as context
                        s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['title'][0], vocabulary,
                                                                       part=part,
                                                                       preloaded=preloaded)])
                        # ====================================================
                    elif first_sentence_always:
                        # ====================================================
                        # test with firs sentence as context for all sentences
                        for i in range(1, window):
                            if sent_num - i <= 0:
                                break
                            else:
                                s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['text'][sent_num - i],
                                                                                             vocabulary, part=part,
                                                                                             preloaded=preloaded)])
                                if as_position == sent_num - i:
                                    as_in_context_b = True
                        if sent_num != 0:
                            # always add first sentence as context
                            s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['text'][0], vocabulary,
                                                                           part=part,
                                                                           preloaded=preloaded)])

                        # ====================================================

                    else:
                        for i in range(1, window + 1):
                            if sent_num == 0:
                                s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['title'][0], vocabulary, part=part, preloaded=preloaded)])
                                break
                            elif sent_num - i == -1:
                                s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['title'][0], vocabulary, part=part, preloaded=preloaded)])
                                break
                            else:
                                s_data['ctx'][f'prev_sent_b'].append([get_ctx_sent(data['text'][sent_num - i], vocabulary, part=part, preloaded=preloaded)])
                                if as_position == sent_num - i:
                                    as_in_context_b = True

                    for i in range(1, window + 1):
                        if sent_num + i > last_sent_idx:
                            break
                        else:
                            s_data['ctx'][f'prev_sent_f'].append([get_ctx_sent(data['text'][sent_num + i], vocabulary, part=part, preloaded=preloaded)])
                            if as_position == sent_num + i:
                                as_in_context_f = True

                    s_data['as_in_context_b'] = as_in_context_b
                    s_data['as_in_context_f'] = as_in_context_f
                # =========================

        try:
            s_data['s_bert'] = sentence['s_bert']
        except (KeyError, TypeError):
            s_data['s_bert'] = None

        try:
            s_data['slavic_bert'] = sentence['slavic_bert']
        except (KeyError, TypeError):
            s_data['slavic_bert'] = None

        try:
            s_data['robeczech'] = sentence['robeczech']
        except (KeyError, TypeError):
            s_data['robeczech'] = None

        try:
            s_data['czert'] = sentence['czert']
        except (KeyError, TypeError):
            s_data['czert'] = None

        result.append(s_data)

        # print('===============================================')
    return result


def get_content(data, vocabulary, old, part='', preloaded=False):
    """
    Harvest content from data
    :param data: list or dict
    :param vocabulary: dict
    :param part: str, specify word parts
    :param context_type: str, specify context type
    :param preloaded: preloaded data in memeory
    :return: list, content of data
    """

    result = []
    for sentence in data:
        s_data = {}
        try:
            s_data['sent'] = get_senence(sentence['sent'], vocabulary, part=part, preloaded=preloaded)
        except (KeyError, TypeError):
            s_data['sent'] = get_senence(sentence, vocabulary, part=part, preloaded=preloaded)

        try:
            s_data['s_bert'] = sentence['s_bert']
        except (KeyError, TypeError):
            s_data['s_bert'] = None

        try:
            s_data['slavic_bert'] = sentence['slavic_bert']
        except (KeyError, TypeError):
            s_data['slavic_bert'] = None

        try:
            s_data['robeczech'] = sentence['robeczech']
        except (KeyError, TypeError):
            s_data['robeczech'] = None

        try:
            s_data['czert'] = sentence['czert']
        except (KeyError, TypeError):
            s_data['czert'] = None

        result.append(s_data)
    return result


def get_record(db, record_id, old=False, word_parts='', context_type='all', ctx_direction='b', vocabulary=None,
               qa_type_dict=None, kb=None, preloaded=False, title_ctx_always=False, first_sentence_always=False):
    """
    :param db: ZODB object, link to database
    :param record_id: str, record id
    :param word_parts: str, specify word parts
    :param context_type: str, specify context type
    :param vocabulary: dict
    :param qa_type_dict: dict
    :param kb: dict, Knowledge base
    :param preloaded: preloaded data in memeory
    :return:
    """
    record = db.get_record(record_id)
    if not vocabulary and not qa_type_dict and not kb:
        # print('Not preloaded data')
        vocabulary, qa_type_dict, kb = db.get_dicts()
    data = {}
    data['rec_id'] = record.rec_id
    try:
        data['yes_no_answer'] = record.yes_no_answer
    except AttributeError:
        data['yes_no_answer'] = None

    data['q_type'] = id2qt(qa_type_dict, record.q_type)
    data['a_type'] = id2qt(qa_type_dict, record.a_type)
    # sys.stderr.write('============= question ===============\n')
    data['question'] = get_content(record.question, vocabulary, old, part=word_parts, preloaded=preloaded)
    # sys.stderr.write('============= a_sel ===============\n')
    data['a_sel'] = get_content(record.answer_selection, vocabulary, old, part=word_parts, preloaded=preloaded)
    data['a_sel_pos'] = record.text_answer_position
    # sys.stderr.write('============= a_ext ===============\n')
    data['a_ext'] = get_content(record.answer_extraction, vocabulary, old, part=word_parts, preloaded=preloaded)
    data['similar_answers'] = dict(record.similar_answers)
    # sys.stderr.write('============= text_title ===============\n')
    data['text_title'] = get_content(kb.url2doc.get(record.text)["title"], vocabulary, old, part=word_parts,
                                     preloaded=preloaded)
    # sys.stderr.write('============= text ===============\n')
    data['text'] = get_content_text(kb.url2doc.get(record.text), vocabulary, old, record.text_answer_position,
                                    part=word_parts, context_type=context_type, ctx_direction=ctx_direction,
                                    preloaded=preloaded, title_ctx_always=title_ctx_always,
                                    first_sentence_always=first_sentence_always)

    data['contain_answer'] = len(record.similar_answers.get("sents_containing_ans_ext", []))
    data['contain_answer_partial'] = len(record.similar_answers.get("sents_containing_ans_ext_partial", []))
    data['not_contain_answer'] = len(data['text']) - len(record.similar_answers.get("sents_containing_ans_ext", [])) \
                                      - len(record.similar_answers.get("sents_containing_ans_ext_partial", []))
    data['url'] = record.text

    return data


def print_record(record_data, similar_answers_key):
    print(f'rec_id: {record_data["rec_id"]}')
    print(f'q_type: {record_data["q_type"]}')
    print(f'a_type: {record_data["a_type"]}')
    print(f'yes_no_answer: {record_data["yes_no_answer"]}')

    print('question:')
    for i in record_data['question']:
        print(f'\ts: {" ".join([x["word"] for x in i["sent"]])}')

    print('a_sel:')
    for i in record_data['a_sel']:
        print(f'\ts: {" ".join([x["word"] for x in i["sent"]])}')

    print(f'a_sel_pos: {record_data["a_sel_pos"]}')

    print('a_ext:')
    for i in record_data['a_ext']:
        print(f'\ts: {" ".join([x["word"] for x in i["sent"]])}')

    print(f'similar_answers ({similar_answers_key}):')
    if record_data.get("similar_answers"):
        if record_data["similar_answers"].get(similar_answers_key):
            for s_idx, score in record_data["similar_answers"][similar_answers_key]:
                for idx, sent_and_phrs in enumerate(record_data['text']):
                    if idx == s_idx:
                        print(f'\t\ts_{idx} ({score:.3f}): {" ".join([x["word"] for x in sent_and_phrs["sent"]])}')
    else:
        print('\t\tNone')

    print('sentences_containing_ans_ext:')
    if record_data.get("similar_answers"):
        if record_data["similar_answers"].get("sents_containing_ans_ext"):
            for s_idx in record_data["similar_answers"]["sents_containing_ans_ext"]:
                for idx, sent_and_phrs in enumerate(record_data['text']):
                    if idx == s_idx:
                        print(f'\t\ts_{idx}: {" ".join([x["word"] for x in sent_and_phrs["sent"]])}')
    else:
        print('\t\tNone')

    print(f'text_title:')
    for i in record_data['text_title']:
        print(f'\ts: {" ".join([x["word"] for x in i["sent"]])}')

    print('text:')
    for idx, sent_and_phrs in enumerate(record_data['text']):
        print(f'\ts_{idx}: {" ".join([x["word"] for x in sent_and_phrs["sent"]])}')
        if sent_and_phrs.get('ctx', ''):
            for key, phrs in sent_and_phrs['ctx'].items():
                as_in_ctx = sent_and_phrs[f'as_in_context_'+key[-1]]
                # try:
                print(f'\t\tctx_type: {key} (as_in_context {as_in_ctx})')
                print_ctx(phrs, key)
                # except KeyError:
                #     pass

    print('No. text sentences that contain exact answer')
    try:
        print(f'\t{len(record_data["similar_answers"]["sents_containing_ans_ext"])}')
    except KeyError:
        print('\tNo info')

    print('No. text sentences that does NOT contain answer')
    try:
        print(f'\t{len(record_data["text"]) - len(record_data["similar_answers"]["sents_containing_ans_ext"])}')
    except KeyError:
        print('\tNo info')


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Transfrom SQAD to one database file with all features')
    # ================================================================
    # Required parameters
    # ================================================================
    parser.add_argument('-d', '--database_file', type=str,
                        required=False, default='',
                        help='Database file name')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default=',',
                        help='DB server URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=0,
                        help='DB server port')
    parser.add_argument('-r', '--record_id', type=str,
                        required=False, default='',
                        help='Record id')
    parser.add_argument('-v', '--version', action='store_true',
                        required=False, default=False,
                        help='Database version')
    parser.add_argument('--list_ctx_types', action='store_true',
                        required=False, default=False,
                        help='List context types')
    parser.add_argument('--old', action='store_true',
                        required=False, default=False,
                        help='Old database format')
    # ================================================================
    # Optional parameters
    # ================================================================
    parser.add_argument('--simple', action='store_true',
                        required=False, default=False,
                        help='Simple output')
    parser.add_argument('--word_parts', type=str,
                        required=False, default='',
                        help='Which word parts will be provided. Semicolon separated. For example "w;l;t;v100" '
                             'will return word, lemma, tag and 100 dim. vector')
    parser.add_argument('--context_type', type=str,
                        required=False, default='all',
                        help='List of context types separated by semicolon. Example "name_phrs;prev_sent;ctx_ner"')
    parser.add_argument('--ctx_direction', type=str,
                        required=False, default='a',
                        help='Context direction. Available options: b (backward), f (forward), a (both directions)')
    parser.add_argument('--similar_answers_key', type=str,
                        required=False, default='tf_idf_full',
                        help='Similar answers key: tf_idf_doc, jaccard, fasttext, tf_idf_full')
    parser.add_argument('--title_ctx_always', action='store_true',
                        required=False, default=False,
                        help='Title will be added into context of all sentences ct_type: "prev_sent_b"')
    parser.add_argument('--first_sentence_always', action='store_true',
                        required=False, default=False,
                        help='First sentence will be added into context of all sentences ct_type: "prev_sent_b"')
    args = parser.parse_args()

    if args.database_file == '' and args.url == '':
        print("Specify database path or DB url. (-d or -u and -p)")
        sys.exit()

    if args.list_ctx_types:
        if args.database_file != '':
            db = SqadDb(file_name=args.database_file, read_only=True)
        else:
            db = SqadDb(read_only=True, url=args.url, port=args.port)

        print(db.get_ctx_types())
    elif args.version:
        if args.database_file != '':
            db = SqadDb(file_name=args.database_file, read_only=True)
        else:
            db = SqadDb(read_only=True, url=args.url, port=args.port)

        print(f'{db.get_version()}.{db.get_update()}')
    elif args.record_id:
        if args.database_file != '':
            db = SqadDb(file_name=args.database_file, read_only=True)
        else:
            db = SqadDb(read_only=True, url=args.url, port=args.port)

        if args.simple:
            record_data = get_record(db, args.record_id, args.old, args.word_parts, args.context_type, args.ctx_direction,
                                     args.title_ctx_always, args.first_sentence_always)
            print_record(record_data, args.similar_answers_key)
        else:
            pprint(get_record(db, args.record_id, args.old, args.word_parts, args.context_type, args.ctx_direction,
                              args.title_ctx_always, args.first_sentence_always))
        db.close()
    else:
        sys.stderr.write('Please specify one of attributes: record_id, list_ctx_types')
        sys.exit()


if __name__ == "__main__":
    main()
