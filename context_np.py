#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
import os
import re
import sys
from sqad_db import W2V
from sqad_db import SqadDb
import persistent.list
from sqad_db import id2word
from sqad_db import word2id
import transaction
from BTrees.OOBTree import BTree
struct_re = re.compile('^<[^>]+>$')

# ==================================================
# SET interface
# ==================================================
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(f'{dir_path}/set')
from grammar import Grammar
from setparser import Parser
from segment import Segment, Tree
import locale
locale.setlocale(locale.LC_ALL, '')


class SetInterface:
    def __init__(self, grammar_path):
        """
        Init set parser
        :param grammar_path: path to grammar file
        """
        self.grammar_path = grammar_path
        self.grammar = os.path.join(self.grammar_path)

    def parse_input(self, lines):
        """
        Pars input sentece and get all NP phrases
        :param lines: str
        :return: list, list of phrases
        """
        g = Grammar(self.grammar)
        p = Parser(g)
        s = Segment(lines)
        p.parse(s)
        return s.get_np_phrases()


def get_struct(data, struct_id):
    struct = []
    struct_start = False

    for line in data:
        if re.match("^<{}>$".format(struct_id), line.strip()) or \
                re.match("^<{} .*?>$".format(struct_id), line.strip()):
            struct_start = True
        if struct_start:
            struct.append(line.strip())
        if re.match("^</{}>$".format(struct_id), line.strip()):
            yield struct
            struct = []
            struct_start = False


def filter_longest(phrases):
    """
    Filter only the longest phrases. No intersection between results
    :param phrases: list of phases
    :return:
    """
    to_remove_idx = []
    for idx_i, i in enumerate(phrases):
        for idx_j, j in enumerate(phrases):
            if idx_i != idx_j:
                if i in j:
                    to_remove_idx.append(idx_i)
    result = []
    for idx, x in enumerate(phrases):
        if idx not in to_remove_idx:
            result.append(x)

    return result


def name_phrases(text, vocabulary, phr_type, w2v):
    """
    Harvest all NP phrases, reduce number according context window and type of phrases per sentence
    :param text: str
    :param vocabulary: dict
    :param phr_type: str
    :param w2v: word2vec model
    :return:
    """
    set_parser = SetInterface(f"{dir_path}/set/grammar.set")
    # Read file and create phrases for all sentences
    phrases_per_sentence = persistent.list.PersistentList()
    for sent_num, sentence in enumerate(get_struct(text, 's')):
        set_phrases = set_parser.parse_input(sentence)
        phrases = persistent.list.PersistentList()
        for p in set_phrases:
            phr = persistent.list.PersistentList()
            for token in p:
                if not struct_re.match(token[0]):
                    try:
                        word, lemma, tag = token
                    except ValueError as e:
                        sys.stderr.write(f'Something goes wrong while splitting line: "{token}" in:\n'
                                         f'{p}\n'
                                         f'splitted as {token}')
                        sys.stderr.write(f'{e}\n')
                        sys.exit()
                    wid = word2id(vocabulary, word, lemma, tag, w2v)
                    phr.append(wid)
            phrases.append(phr)
        if phr_type == 'longest':
            phrases_per_sentence.append(filter_longest(phrases))
        else:
            phrases_per_sentence.append(phrases)

    return phrases_per_sentence


def get_text_vert(text_by_url, vocabulary):
    """
    Text to vert
    :param text_by_url: str
    :param vocabulary: dict
    :return: str
    """
    vert = []
    for sentence in text_by_url:
        vert.append('<s>')
        for token_id in sentence['sent']:
            t = id2word(vocabulary, token_id)
            vert.append(f'{t["word"]}\t{t["lemma"]}\t{t["tag"]}')
        vert.append('</s>')

    return vert


def add_np_phrases(db, phr_type, w2v, verbose=False):
    """
    Assign phrases as context to sentences
    :param db: ZODB obj
    :param phr_type: str
    :param w2v: word2vec model
    :param verbose: bool
    :return: None
    """
    vocabulary, qa_type_dict, kb = db.get_dicts()
    for url, text in kb.url2doc.items():
        if verbose:
            print(f'Processing: {url}')
        text_vert = get_text_vert(text['text'], vocabulary)

        phrases = name_phrases(text_vert, vocabulary, phr_type, w2v)

        for sent_num, sent in enumerate(text['text']):
            if verbose:
                print(f"s:{' '.join([id2word(vocabulary, x)['word'] for x in sent['sent']])}")
                for phr in phrases[sent_num]:
                    print(f'\t\tp:{" ".join([id2word(vocabulary, x)["word"] for x in phr])}')

            if not sent['ctx'].get(f'name_phrs_t{phr_type}'):
                sent['ctx'][f'name_phrs_t{phr_type}'] = persistent.list.PersistentList()
                for phr in phrases[sent_num]:
                    sent['ctx'][f'name_phrs_t{phr_type}'].append(BTree({'sent': phr}))

            if verbose:
                from query_database import get_sentence_content
                from pprint import pprint
                print('=====================================================================')
                pprint(get_sentence_content(sent, vocabulary, part='w', context_type='all'))
            db._p_changed = True
            transaction.commit()


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Add noun phrases as context to sentences')
    parser.add_argument('--phr_per_sent', type=str,
                        required=False, default='longest',
                        help='Category of phreses as a context. Available: all, longest')
    parser.add_argument('--test', action='store_true',
                        required=False, default=False,
                        help='Dont load vectors - for testing purposes.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Verbose mode')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-d', '--db_path', type=str,
                        required=False, default='',
                        help='Database path')
    args = parser.parse_args()

    w2v = W2V(test=args.test)

    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    try:
        add_np_phrases(db, args.phr_per_sent, w2v, args.verbose)

        db.root['__ctx_types__'].append(f'name_phrs_t{args.phr_per_sent}')
        db.update()
        db._p_changed = True
        transaction.commit()
        db.close()
    except KeyboardInterrupt:
        db.close()
        sys.exit()


if __name__ == "__main__":
    main()
