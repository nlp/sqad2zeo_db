#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
import persistent.list
from BTrees.OOBTree import BTree
from persistent import Persistent
import sys
import persistent.list
from importlib import import_module


# =============================================
# Vocabulary
# =============================================
def word2id(vocabulary, word, lemma, tag, w2v):
    """
    Transform word to specific ID
    :param vocabulary: dict
    :param word: str
    :param lemma: str
    :param tag: str
    :param w2v: word2vec model
    :return: int
    """
    if vocabulary.wlt2id.get(f'{word}|{lemma}|{tag}', None):
        return vocabulary.wlt2id[f'{word}|{lemma}|{tag}']
    else:
        key = vocabulary.new_id()
        vocabulary.wlt2id[f'{word}|{lemma}|{tag}'] = key
        vocabulary.id2wlt[key] = {'word': word, 'lemma': lemma if lemma not in ['#num#', '[number]'] else word,
                                  'tag': tag}
        w2v.add_vector(vocabulary, key, word)
        return key


def id2word(vocabulary, key, parts='', preloaded=False):
    """
    Transform ID to word, lemma, tag
    :param vocabulary: dict
    :param key: int
    :param parts: str, word parts to be returned
    :param preloaded: list
    :return: all word inforamtion
    """
    result = {}
    word_parts = parts.strip().split(';')

    if preloaded:
        if 'w' in word_parts or not parts:
            result['word'] = vocabulary['id2wlt'][key]['word']
        if 'l' in word_parts or not parts:
            result['lemma'] = vocabulary['id2wlt'][key]['lemma']
        if 't' in word_parts or not parts:
            result['tag'] = vocabulary['id2wlt'][key]['tag']
        if 'v100' in word_parts or not parts:
            result['v100'] = vocabulary['vectors'][key]['v100']
        if 'v300' in word_parts or not parts:
            result['v300'] = vocabulary['vectors'][key]['v300']
        if 'v500' in word_parts or not parts:
            result['v500'] = vocabulary['vectors'][key]['v500']
        if 'slavic_bert' in word_parts or not parts:
            try:
                result['slavic_bert'] = vocabulary['vectors'][key]['slavic_bert']
            except KeyError:
                pass
                # sys.stderr.write(f'ERROR: not "v_bert" for: {vocabulary["id2wlt"][key]["word"]}\n')
        if 'id' in word_parts or not parts:
            result['id'] = key
    else:
        if 'w' in word_parts or not parts:
            result['word'] = vocabulary.id2wlt[key]['word']
        if 'l' in word_parts or not parts:
            result['lemma'] = vocabulary.id2wlt[key]['lemma']
        if 't' in word_parts or not parts:
            result['tag'] = vocabulary.id2wlt[key]['tag']

        # Backwards compatibility
        if isinstance(vocabulary.vectors[key], BTree):  # New
            if 'v100' in word_parts or not parts:
                result['v100'] = vocabulary.vectors[key]['v100']
            if 'v300' in word_parts or not parts:
                result['v300'] = vocabulary.vectors[key]['v300']
            if 'v500' in word_parts or not parts:
                result['v500'] = vocabulary.vectors[key]['v500']
        elif isinstance(vocabulary.vectors[key], persistent.list.PersistentList):  # Old
            if 'v100' in word_parts or not parts:
                result['v100'] = vocabulary.vectors[key][0]
            if 'v300' in word_parts or not parts:
                result['v300'] = vocabulary.vectors[key][1]
            if 'v500' in word_parts or not parts:
                result['v500'] = vocabulary.vectors[key][2]

        if 'slavic_bert' in word_parts or not parts:
            try:
                result['slavic_bert'] = vocabulary.vectors[key]['slavic_bert']
            except (KeyError, TypeError):
                pass
                # sys.stderr.write(f'ERROR: not "v_bert" for: {vocabulary.id2wlt[key]["word"]}\n')
        if 'id' in word_parts or not parts:
            result['id'] = key
    return result


class W2V:
    """
    Add word vectors to each word inside vocabulary
    """
    def __init__(self, test=False):
        from get_vector import Word2vec

        self.test = test
        if not self.test:
            self.w2v = Word2vec()

    def get_vect(self, word):
        """
        Get word vector
        :param word: str
        :return: list
        """
        result = BTree()
        if self.test:
            result['v100'] = None
            result['v300'] = None
            result['v500'] = None
        else:
            result['v100'] = persistent.list.PersistentList(self.w2v.get_vector(word, '100'))
            result['v300'] = persistent.list.PersistentList(self.w2v.get_vector(word, '300'))
            result['v500'] = persistent.list.PersistentList(self.w2v.get_vector(word, '500'))
            result['slavic_bert'] = persistent.list.PersistentList(self.w2v.get_vector(word, 'slavic_bert'))
        return result

    def add_vector(self, vocabulary, key, word):
        """
        Add vector to word form vocabulary
        :param vocabulary: dict
        :param key: id
        :param word: str
        :return: None
        """
        vocabulary.vectors[key] = self.get_vect(word)


class Vocabulary(Persistent):
    """
    Vocabulary of all words inside Knowledge base wit all features: word, lemma, tag, vectors
    """
    def __init__(self):
        self.id2wlt = BTree()  # key: id, value: word, lemma, tag
        self.wlt2id = BTree()  # key: word, value: id
        self.key = 0
        self.vectors = BTree()  # key: word_id, value: v100, v300, v500

    def new_id(self):
        """
        Create specific id for each word
        :return: int
        """
        self.key += 1
        return self.key


# =============================================
# QA type
# =============================================
def qt2id(qa_type, qt_type):
    """
    Question type to ID
    :param qa_type: dict
    :param qt_type: str
    :return:
    """
    if qa_type.t2id.get(qt_type, None):
        return qa_type.t2id[qt_type]
    else:
        key = qa_type.new_id()
        qa_type.id2t[key] = qt_type
        qa_type.t2id[qt_type] = key
        return key


def id2qt(qa_type, key):
    """
    ID to question type
    :param qa_type: dict
    :param key: int, the ID
    :return:
    """
    return qa_type.id2t.get(key, -1)


class QAType(Persistent):
    """
    Question type object to store question ans answer types
    """
    def __init__(self):
        self.id2t = BTree()  # key: id, value: type
        self.t2id = BTree()  # key: type, value: id
        self.key = 0

    def new_id(self):
        self.key += 1
        return self.key


# =============================================
# Knowledge base
# =============================================
def present_in_kb(kb, url):
    """
    Check if documentis in knowledge base
    :param kb: dict, kb object
    :param url: str
    :return: bool
    """
    return kb.url2doc.get(url)


def add2kb(kb, url, text):
    """
    Add processed text to knowledge base
    :param kb: dict, kb object
    :param url: str
    :param text: dict, text object
    :return: None
    """
    kb.url2doc[url] = text


class KnowledgeBase(Persistent):
    """
    QuestKnowledgeBase object to store all knowledge extracted form raw text
    """
    def __init__(self):
        self.url2doc = BTree()


# =============================================
# Record
# =============================================
class Record(Persistent):
    """
    Record object to store all information about each record
    """
    def __init__(self, rec_id):
        self.rec_id = rec_id
        self.question = persistent.list.PersistentList()  # List of sentences where each item contains list of words
        self.answer_selection = persistent.list.PersistentList()  # -/-
        self.answer_extraction = persistent.list.PersistentList()  # -/-
        self.yes_no_answer = ''
        self.text = ''  # -/-
        self.text_answer_position = -1
        self.q_type = -1
        self.a_type = -1
        self.similar_answers = BTree()


# =============================================
# Sqad database
# =============================================
class SqadDb:
    """
    SQAD database in ZODB form
    """
    def __init__(self, file_name=None, read_only=False, url=None, port=None):
        """
        Init ZODB database or connect database if already exists, via file or ZEO server
        :param file_name: str
        :param read_only: bool
        :param url: str
        :param port: str
        """
        import ZODB
        import ZODB.FileStorage
        from ZEO import ClientStorage

        if not read_only:
            self.transaction = import_module('transaction')

        if url and port and not file_name:
            sys.stderr.write(f'Connecting to url: "{url}"; port: "{port}"; read_only: {read_only}\n')
            addr = url, port

            try:
                self.storage = ClientStorage.ClientStorage(addr, read_only=read_only)
            except Exception as e:
                sys.stderr.write(f'ERROR: Problem with ZODB server connection {e}\n')
                sys.exit()
            sys.stderr.write('Connected\n')
        else:
            sys.stderr.write(f'Connecting to file_name: "{file_name}"; read_only: {read_only}\n')
            self.file_name = file_name
            try:
                self.storage = ZODB.FileStorage.FileStorage(self.file_name, read_only=read_only)
            except BlockingIOError:
                sys.stderr.write('ERROR: database currently unavailable.\n')
                sys.exit()
            sys.stderr.write('Connected\n')

        self.db = ZODB.DB(self.storage)
        self.connection = self.db.open()
        self.root = self.connection.root()

    def add(self, rec_id, record_object):
        """
        Add new record to database
        :param rec_id: int
        :param record_object: dict
        :return: None
        """
        self.root[rec_id] = record_object
        self.transaction.commit()

    def add_vocab(self, vocab):
        """
        Add vocabulary object to database
        :param vocab: dict
        :return: None
        """
        self.root['__vocabulary__'] = vocab
        self.transaction.commit()

    def add_qa_type(self, qa_type):
        """
        Add question/answer type object to database
        :param qa_type: dict
        :return: None
        """
        self.root['__qa_type__'] = qa_type
        self.transaction.commit()

    def add_kb(self, knowledge_base):
        """
        Add knowledge base object to database
        :param knowledge_base: dict
        :return: None
        """
        self.root['__knowledge_base__'] = knowledge_base
        self.transaction.commit()

    def get_ctx_types(self):
        """
        Add knowledge base object to database
        :param knowledge_base: dict
        :return: None
        """
        return self.root['__ctx_types__']

    def get_dicts(self):
        """
        Get vocabulary, question type and knowledge base objects
        :return: tuple
        """
        return self.root['__vocabulary__'], self.root['__qa_type__'], self.root['__knowledge_base__']

    def init_ctx_types(self):
        """
        Init context types object
        :return: None
        """
        self.root['__ctx_types__'] = persistent.list.PersistentList()
        self.transaction.commit()

    def version(self, version):
        """
        Assagin database a new version
        :param version: int
        :return: None
        """
        self.root['__version__'] = version
        self.transaction.commit()

    def get_version(self):
        """
        Get database version
        :return: int
        """
        return self.root['__version__']

    def update(self):
        """
        Assign database a new update
        :return: None
        """
        if self.root.get('__update__', None):
            self.root['__update__'] += 1
        else:
            self.root['__update__'] = 1
        self.transaction.commit()

    def get_update(self):
        """
        Get database update
        :return: int
        """
        return self.root['__update__']

    def get_record(self, rec_id):
        """
        Get record according to ID
        :param rec_id: int
        :return: None
        """
        try:
            return self.root[rec_id]
        except KeyError:
            sys.stderr.write(f'ERROR: Record {rec_id} not present in DB!')

    def close(self):
        """
        Close database connection
        :return: None
        """
        self.connection.close()
        self.db.close()
        self.storage.close()

    def get_all_records_id(self):
        """
        Get IDs of all records present in DB
        :return: dict
        """
        for i in self.root:
            if i not in ['__vocabulary__', '__knowledge_base__',
                         '__ctx_types__', '__qa_type__', '__update__', '__version__']:
                yield i
