DATE=$(shell date +"%d-%m-%Y_%H-%M-%S")
DB_PATH=sqad_db/devel/

DB_NAME=sqad_db/devel/sqad_v3_$(DATE)_base
DB_NAME_TEST=sqad_db/test/sqad_v3_$(DATE)_test
VERSION=$(shell cat ./sqad_db/version)
NEW_VERSION=$$(($(VERSION)+1))

DB_NAME_4=sqad_db/devel/sqad_v4_$(DATE)_base

# Need to specify bash in order for conda activate to work.
SHELL=/bin/bash
# Note that the extra activate is needed to ensure that the activate floats env to the front of PATH
CONDA_ACTIVATE=source $$(conda info --base)/etc/profile.d/conda.sh ; conda activate ; conda activate

create_v3:
	@echo -e "\e[0;31mPlease create DB only on asteria04 or apollo servers!!!\e[0m"
	@printf "SQAD to DB\n=======================\n" >> $(DB_NAME).log
	@cat ./Makefile >> $(DB_NAME).log
	@echo $(NEW_VERSION) > ./sqad_db/version
	($(CONDA_ACTIVATE) base; ./sqad2database.py -p /nlp/projekty/sqad/sqad_v3/data -n $(DB_NAME) -v $(NEW_VERSION) 2>> $(DB_NAME).log)
	@echo "$(hostname)" | mail -s "Done sqad_db V3 created" "xmedved1@fi.muni.cz"


create_v4:
	@echo -e "\e[0;31mPlease create DB only on asteria04 or apollo servers!!!\e[0m"
	@mkdir $(DB_PATH)/v4_$(DATE)
	@printf "SQAD to DB\n=======================\n" >> $(DB_PATH)/v4_$(DATE)/v4_$(DATE)_base.log
	@cat ./Makefile >> $(DB_PATH)/v4_$(DATE)/v4_$(DATE)_base.log
	($(CONDA_ACTIVATE) base; ./sqad2database.py -p /nlp/projekty/sqad/sqad_v4/data -n $(DB_PATH)/v4_$(DATE)/v4_$(DATE)_base -v "1" 2>> $(DB_PATH)/v4_$(DATE)/v4_$(DATE)_base.log)
	@echo "$(hostname)" | mail -s "Done sqad_db V4 created" "xmedved1@fi.muni.cz"

create_test:
	printf "SQAD to DB\n=======================\n" >> $(DB_NAME_TEST).log
	($(CONDA_ACTIVATE) base; ./sqad2database.py -p /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/test -n $(DB_NAME_TEST) -v 1 --test 2>> $(DB_NAME_TEST).log)

updates:
	@echo "creating updates $(DB)"
	@# Contains answer sentece
	./make_copy.sh $(DB) $(DB)_addAS
	@printf "Contains answer\n======================\n" >> $(DB)_addAS.log
	($(CONDA_ACTIVATE) base; ./add_contains_answer_sentences.py -d $(DB)_addAS --no_partial 2>> $(DB)_addAS.log)
	@# Similar sentences
	./make_copy.sh $(DB)_addAS $(DB)_addAS_simS
	@printf "Similar answers\n======================\n" >> $(DB)_addAS_simS.log
	($(CONDA_ACTIVATE) base; ./add_similar_sentences.py -n 0 -d $(DB)_addAS_simS 2>> $(DB)_addAS_simS.log)
	@# Context NP
	./make_copy.sh $(DB)_addAS_simS $(DB)_addAS_simS_cNP
	@printf "Contex NP phrases\n======================\n" >> $(DB)_addAS_simS_cNP.log
	($(CONDA_ACTIVATE) base; ./context_np.py --phr_per_sent "longest" -d $(DB)_addAS_simS_cNP 2>> $(DB)_addAS_simS_cNP.log)
	@# Context NER
	./make_copy.sh $(DB)_addAS_simS_cNP $(DB)_addAS_simS_cNP_cNER
	@printf "Context wiki entity NER\n======================\n" >> $(DB)_addAS_simS_cNP_cNER.log
	($(CONDA_ACTIVATE) base; python ./context_ner.py -m /nlp/projekty/question_answering/AQA_v2/ner/link_ner/BERT-NER/ner_model_cz_v2/ -d $(DB)_addAS_simS_cNP_cNER 2>> $(DB)_addAS_simS_cNP_cNER.log)
	@# CLS Bert
	./make_copy.sh $(DB)_addAS_simS_cNP_cNER $(DB)_addAS_simS_cNP_cNER_sBert
	@printf "Sentence to cls bert embedding\n======================\n" >> $(DB)_addAS_simS_cNP_cNER_sBert.log
	($(CONDA_ACTIVATE) base; python ./sentence_vectors.py -d $(DB)_addAS_simS_cNP_cNER_sBert --ctx 2>> $(DB)_addAS_simS_cNP_cNER_sBert.log)
	@echo "$(hostname)" | mail -s "Done AQA job" "xmedved1@fi.muni.cz"

run_ZODB_server:
	conda activate aqa-apollo; exec "python3" -m "ZEO.runzeo" -C /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database/zeo_server.conf
	#exec "/usr/bin/python3.6" -m "ZEO.runzeo" -C /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database/zeo_server.conf
	#cd "$(HOME)/.local/lib/python3.6/site-packages/"; exec "/usr/bin/python3.6" -m "ZEO.runzeo" -a "0.0.0.0:9001" -f "/nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/sqad_db/stable"

demo_query:
	./query_database.py -d sqad_db/stable -r 000180 --simple | head -n 38

demo_query_1:
	./query_database.py -d sqad_db/stable -r 000180 | less

demo_zeo:
	exec "python3" -m "ZEO.runzeo" -C /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database/zeo_server_9003.conf

demo_zeo_query:
	./query_database.py -u "0.0.0.0" -p 9003 -r 000180 --simple | head -n 38

sqad_stats:
	./sqad_stats.py -d sqad_db/devel/v3_2_20-09-2022_19-28-39/v3_2_20-09-2022_19-28-39_base > $@

