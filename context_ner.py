#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
import re
from sqad_db import SqadDb
import persistent.list
from sqad_db import id2word
import transaction
from BTrees.OOBTree import BTree
import sys
struct_re = re.compile('^<[^>]+>$')


def normalize_out(ner_out, sent):
    """
    Match result from NER recognizer with original sentence
    :param ner_out: dict, output form NER
    :param sent: list, original sentence
    :return: list, corrected output
    """
    result = []
    for idx, token in enumerate(sent):
        if token['word'] != ner_out[idx]['word']:
            if idx+1 < len(ner_out) and f'{ner_out[idx]["word"]}{ner_out[idx+1]["word"]}' == token['word']:
                if ner_out[idx]["tag"] != 'O' and ner_out[idx+1]["tag"] == 'O':
                    tag = ner_out[idx]["tag"]
                elif ner_out[idx]["tag"] == 'O' and ner_out[idx+1]["tag"] != 'O':
                    tag = ner_out[idx+1]["tag"]
                elif ner_out[idx]["tag"] != 'O' and ner_out[idx + 1]["tag"] != 'O':
                    tag = 'B'
                else:
                    tag = 'O'

                confidence = None
                result.append({'word': token['word'], 'tag': tag, 'confidence': confidence})
        else:
            result.append(ner_out[idx])
    return result


def ner_phrases(text, model):
    """
    Compute all named entities per sentence
    :param text: list, text content
    :param model: NER model
    :return: list
    """
    ner_per_sentence = persistent.list.PersistentList()
    for sent in text:
        sent_ners = persistent.list.PersistentList()
        sent_w = ' '.join([x["word"] for x in sent])

        w_idx = 0
        try:
            ner_out = model.predict(sent_w[:512])
        except RuntimeError as e:
            sys.stderr.write(f'ERROR predict: {e}\n')
            sys.stderr.write(f'ERROR predict sent: {sent_w}\n')
            sys.exit(2)

        ner_out = normalize_out(ner_out, sent)

        while w_idx < len(ner_out):
            if ner_out[w_idx]['tag'] == 'B':
                if w_idx+1 < len(ner_out):
                    if ner_out[w_idx+1]['tag'] != 'I':
                        sent_ners.append([sent[w_idx]['id']])
                        w_idx += 1
                    elif ner_out[w_idx+1]['tag'] == 'I':
                        composite_ner = persistent.list.PersistentList()
                        while w_idx < len(ner_out) and ner_out[w_idx]['tag'] != 'O':
                            composite_ner.append(sent[w_idx]['id'])
                            w_idx += 1
                        sent_ners.append(composite_ner)
                else:
                    sent_ners.append([sent[w_idx]['id']])
                    w_idx += 1
            else:
                w_idx += 1
        ner_per_sentence.append(sent_ners)

    return ner_per_sentence


def get_content(text_by_url, vocabulary):
    """
    Sentence content
    :param text_by_url: dict, text
    :param vocabulary: dict
    :return: list
    """
    text_content = []
    for sentence in text_by_url:
        sent = []
        for token_id in sentence['sent']:
            t = id2word(vocabulary, token_id)
            sent.append({"word": t["word"], "id": t["id"]})
        text_content.append(sent)

    return text_content


def add_ner(db, model, verbose=False):
    """
    Harvest entities and put them as context
    :param db: ZODB object
    :param model: kNER model
    :param verbose: bool
    :return: None
    """
    vocabulary, qa_type_dict, kb = db.get_dicts()
    for url, text in kb.url2doc.items():
        if verbose:
            print(f'Processing: {url}')

        text_vert = get_content(text['text'], vocabulary)
        ner_per_sent = ner_phrases(text_vert, model)

        for sent_num, sent in enumerate(text['text']):

            if verbose:
                print(f"s:{' '.join([id2word(vocabulary, x)['word'] for x in sent['sent']])}")
                for ner in ner_per_sent[sent_num]:
                    print(f'\t\tp:{" ".join([id2word(vocabulary, x)["word"] for x in ner])}')

            if not sent['ctx'].get(f'ctx_ner'):
                sent['ctx'][f'ctx_ner'] = persistent.list.PersistentList()
                for ner in ner_per_sent[sent_num]:
                    sent['ctx'][f'ctx_ner'].append(BTree({'sent': ner}))

            if verbose:
                from query_database import get_sentence_content
                from pprint import pprint
                print('=====================================================================')
                pprint(get_sentence_content(sent, vocabulary, part='w', context_type='all'))
            db._p_changed = True
            transaction.commit()


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Add named entity as a context to sentence - Local option')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-d', '--db_path', type=str,
                        required=False, default='',
                        help='Database path')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Verbose mode')
    parser.add_argument('-m', '--ner_model', type=str,
                        required=True,
                        help='NER model')
    args = parser.parse_args()

    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    try:
        from bert import Ner
        model = Ner(args.ner_model)

        add_ner(db, model, args.verbose)

        db.root['__ctx_types__'].append(f'ctx_ner')
        db.update()
        db._p_changed = True
        transaction.commit()
        db.close()
    except KeyboardInterrupt:
        db.close()
        sys.exit()


if __name__ == "__main__":
    main()

