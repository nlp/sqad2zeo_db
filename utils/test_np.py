#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import os
import re
#from sqad2database import get_struct

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(f'{dir_path}/set')
from grammar import Grammar
from setparser import Parser
from segment import Segment, Tree
import locale
locale.setlocale(locale.LC_ALL, '')
struct_re = re.compile('^<[^>]+>$')

def get_struct(data, struct_id):
    """
    Get structure from vertical
    :param data: str, vertical
    :param struct_id: str
    :return: str, senteces
    """
    struct = []
    struct_start = False
    struct_end = False

    for line in data:
        if re.match("^<{}>$".format(struct_id), line.strip()) or \
                re.match("^<{} .*?>$".format(struct_id), line.strip()):
            struct_start = True
        if re.match("^</{}>$".format(struct_id), line.strip()):
            struct_end = True
        if struct_start:
            struct.append(line.strip())
        if struct_start and struct_end:
            yield struct
            struct = []
            struct_start = False
            struct_end = False

class SetInterface:
    def __init__(self, grammar_path):
        """
        Init set parser
        :param grammar_path: path to grammar file
        """
        self.grammar_path = grammar_path
        self.grammar = os.path.join(self.grammar_path)

    def parse_input(self, lines):
        """
        Pars input sentece and get all NP phrases
        :param lines: str
        :return: list, list of phrases
        """
        g = Grammar(self.grammar)
        p = Parser(g)
        s = Segment(lines)
        p.parse(s)
        return s.get_np_phrases()


def filter_longest(phrases):
    """
    Filter only the longest phrases. No intersection between results
    :param phrases: list of phases
    :return:
    """
    to_remove_idx = []
    for idx_i, i in enumerate(phrases):
        for idx_j, j in enumerate(phrases):
            if idx_i != idx_j:
                if i in j:
                    to_remove_idx.append(idx_i)
    result = []
    for idx, x in enumerate(phrases):
        if idx not in to_remove_idx:
            result.append(x)

    return result


def name_phrases_test(text, phr_type='longest'):
    """
    Harvest all NP phrases, reduce number according context window and type of phrases per sentence
    :param text: str
    :param vocabulary: dict
    :param phr_type: str
    :param w2v: word2vec model
    :return:
    """
    set_parser = SetInterface(f"{dir_path}/set/grammar.set")
    # Read file and create phrases for all sentences
    phrases_per_sentence = []
    for sent_num, sentence in enumerate(get_struct(text, 's')):
        print('sentence: \n{}'.format("\n".join(sentence)))
        set_phrases = set_parser.parse_input(sentence)
        phrases = []
        for p in set_phrases:
            phr = []
            for token in p:
                word, lemma, tag = token
                phr.append(word)
            phrases.append(phr)
        if phr_type == 'longest':
            phrases_per_sentence.append(filter_longest(phrases))
        else:
            phrases_per_sentence.append(phrases)

    return phrases_per_sentence

def main():
    import argparse
    parser = argparse.ArgumentParser(description='TODO')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    args = parser.parse_args()

    test_sentence = ['<s>',
                    'Ke\tk\tk7c3',
                    'hradu\thrad\tk1gInSc3',
                    'Pernštejnu\tPernštejn\tk1gInSc3',
                    'jdeme\tjít\tk5eAaImIp1nP',
                    '<g/>',
                    '.\t.\tkIx.',
                    '</s>']
    print(name_phrases_test(test_sentence))


if __name__ == '__main__':
    main()
