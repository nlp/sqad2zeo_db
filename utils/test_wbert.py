#!/usr/bin/env python3
# coding: utf-8
from transformers import BertTokenizer, BertConfig, BertModel

class Bert_Embeddings:
    def __init__(self):
        config = BertConfig.from_json_file(
            '/nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/bert_config.json')
        self.model = BertModel.from_pretrained(
            '/nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/pytorch_model.bin',
            config=config, local_files_only=True)

        self.tokenizer = BertTokenizer.from_pretrained(
            '/nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/bert_embeder_models/bg_cs_pl_ru_cased_L-12_H-768_A-12_pt/')

    def word2embedding(self, word):
        input_ids = self.tokenizer.encode(word, return_tensors="pt", add_special_tokens=False)
        output = self.model(input_ids)

        average_tensor = []
        input_ids_2 = self.tokenizer.encode(word, add_special_tokens=False)
        for idx, i in enumerate(input_ids_2):
            print(f'{self.tokenizer.decode(i)} -> {i}')
            print(type(output[0][0][idx]))
            average_tensor.append(output[0][0][idx].detach().numpy())
        average_tensor = [sum(items)/len(average_tensor) for items in zip(*average_tensor)]
        print(average_tensor)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Add bert embeddings to vocabulary')

    parser.add_argument('-w', '--word', type=str,
                        required=False, default='',
                        help='Word')
    args = parser.parse_args()



    model = Bert_Embeddings()
    for i in args.word.split(' '):
        model.word2embedding(i)

if __name__ == "__main__":
    main()
