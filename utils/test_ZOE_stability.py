#!/usr/bin/env python3
# coding: utf-8
import sys
from sqad_db import SqadDb
from query_database import print_record
import random


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Infinite database querying to monitor behaviour.')
    parser.add_argument('-u', '--url', type=str,
                        required=True,
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=True,
                        help='Server port')

    args = parser.parse_args()
    db = SqadDb(read_only=True, url=args.url, port=args.port)


    try:
        while True:
            random_list = ['{:06d}'.format(x) for x in random.sample(range(1, 13473), 10)]
            for i in random_list:
                print_record(db, i)
    except KeyboardInterrupt:
        db.close()
        sys.exit()

if __name__ == "__main__":
    main()
