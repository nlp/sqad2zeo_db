#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
from query_database import get_content
from sqad_db import SqadDb


def print_record(db, record_id, old, max_number):
    record = db.get_record(record_id)
    vocabulary, qa_type_dict, kb = db.get_dicts()

    sys.stdout.write(f'{record.rec_id}\n')
    # print(f'q_type: {id2qt(qa_type_dict, record.q_type)}')
    # print(f'a_type: {id2qt(qa_type_dict, record.a_type)}')

    sys.stdout.write('q: ')
    for i in get_content(record.question, vocabulary, old, part='w'):
        sys.stdout.write(f'{" ".join([x["word"] for x in i["sent"]])}\n')

    sys.stdout.write('a: ')
    for i in get_content(record.answer_selection, vocabulary, old, part='w'):
        sys.stdout.write(f'{" ".join([x["word"] for x in i["sent"]])}\n')

    sys.stdout.write(f'a_sel_pos: {record.text_answer_position}\n')

    sys.stdout.write('e: ')
    for i in get_content(record.answer_extraction, vocabulary, old, part='w'):
        sys.stdout.write(f'{" ".join([x["word"] for x in i["sent"]])}\n')

    for name, value in record.similar_answers.items():
        if name == 'sents_similar':
            sys.stdout.write(f'{name}:\n')
            for s_idx, score in value[:max_number]:
                for idx, sent_and_phrs in enumerate(get_content(kb.url2doc.get(record.text)['text'], vocabulary, old,
                                                                part='w')):
                    if idx == s_idx:
                        print(f'{idx:5.0f} ({score:.3f}): {" ".join([x["word"] for x in sent_and_phrs["sent"]])}')


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Print similar sentences for record')
    parser.add_argument('-d', '--database_file', type=str,
                        required=False, default='',
                        help='Database file name')

    parser.add_argument('-r', '--record_ids', type=str,
                        required=True,
                        help='Record id')
    parser.add_argument('-n', '--max_number', type=int,
                        required=False, default=20,
                        help='Number of best scored similar sentences')
    args = parser.parse_args()
    for rid in args.record_ids.strip().split(';'):
        db = SqadDb(file_name=args.database_file, read_only=True)
        print_record(db, rid, False, args.max_number)
        print('================================================================')


if __name__ == "__main__":
    main()
