#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
from sqad_db import SqadDb
from sqad_db import id2word
import sys
import os
import math
dir_path = os.path.dirname(os.path.realpath(__file__))


def precompute_tfidf(kb, vocabulary):
    """
    Copute term frequency per document and document frequency per word
    :param kb: ZODB object
    :param vocabulary: persistent list
    :return: number_of_documents, idf_per_doc, tf_per_doc
    """
    number_of_documents = 0
    idf_per_doc = {}
    tf_per_doc = {}

    for url, text in kb.url2doc.items():
        if url == 'https://cs.wikipedia.org/wiki/Kvark':
            number_of_documents += 1
            number_of_words_per_document = 0
            for sentence in text['text']:
                for word_id in sentence['sent']:
                    number_of_words_per_document += 1
                    word_lemma = id2word(vocabulary, word_id)['lemma']
                    print(word_lemma)
                    if word_lemma == 'předpovězet':
                        print('FOUND')
                        sys.exit()


                    if idf_per_doc.get(url):
                        idf_per_doc[url].add(word_lemma)
                    else:
                        idf_per_doc[url] = {word_lemma}

                    if tf_per_doc.get(url):
                        if tf_per_doc[url].get(word_lemma):
                            tf_per_doc[url][word_lemma] += 1
                        else:
                            tf_per_doc[url][word_lemma] = 1
                    else:
                        tf_per_doc[url] = {word_lemma: 1}
            for word_lemma in tf_per_doc[url].keys():
                tf_per_doc[url][word_lemma] = float(tf_per_doc[url][word_lemma]) / number_of_words_per_document

    return number_of_documents, idf_per_doc, tf_per_doc


def compute_tfidf(kb, vocabulary, verbose=False):
    """
    COpute final TF-IDF according to https://cs.wikipedia.org/wiki/Tf-idf
    :param kb: ZODB object
    :param vocabulary: persistent list
    :param verbose: bool
    :return: dict, final tf-idf
    """
    tf_idf = {}
    number_of_documents, idf_per_doc, tf_per_doc = precompute_tfidf(kb, vocabulary)

    idf = {}
    for url, value in idf_per_doc.items():
        for w in value:
            if idf.get(w):
                idf[w] += 1
            else:
                idf[w] = 1

    for url, value in tf_per_doc.items():
        for word_lemma, word_tf in value.items():
            if tf_idf.get(url):
                if tf_idf.get(url).get(word_lemma):
                    print('Error: multiple tf-idf')
                    sys.exit()
                else:
                    tf_idf[url][word_lemma] = word_tf * math.log(float(number_of_documents) / idf[word_lemma])
            else:
                tf_idf[url] = {word_lemma: word_tf * math.log(float(number_of_documents) / idf[word_lemma])}

    if verbose:
        # Check TF-IDF score manually
        for url, value in tf_per_doc.items():
            print(f'{url}')
            for w, c in value.items():
                print(f'{w}\t: tf:{c}, idf: {idf[w]}, tf-idf: {tf_idf[url][w]}')
        sys.exit()

    return tf_idf


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Similar senteces using TF-IDF')
    parser.add_argument('-n', '--number', type=int,
                        required=False, default=100,
                        help='Number of previous sentences used for searching similar sentences.'
                             ' For all similar sentences put "0".')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-d', '--db_path', type=str,
                        required=False, default='',
                        help='Database path')
    parser.add_argument('--sort_orig', action='store_true',
                        required=False, default=False,
                        help='Sort according cosine similarity on non enhanced vectors')
    parser.add_argument('-j', '--jaccard_n_gram_len', type=int,
                        required=False, default=3,
                        help='Jaccard n-gram length')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False,
                        help='Verbose mode')
    parser.add_argument('--export_tex', action='store_true',
                        required=False,
                        help='Export tech tables (not storing to databse)')

    args = parser.parse_args()

    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    sys.stderr.write('Loading DB\n')
    vocabulary, _, kb = db.get_dicts()

    sys.stderr.write('Precomputing tf-idf\n')
    # Opening stored tf-idf
    tf_idf_per_full_sqad = compute_tfidf(kb, vocabulary)
    db.close()
    sys.exit()


if __name__ == "__main__":
    main()
