#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys

from sentece2cls_bert import S2RobeCzech
from pympler import asizeof
from sentence_transformers import SentenceTransformer


def main():
    import argparse
    parser = argparse.ArgumentParser(description='TODO')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    args = parser.parse_args()
    model = S2RobeCzech()
    vector = model.get_sent_embedding('Ahoj jak se máš ?')
    print(type(vector))
    print(vector)
    print(list(vector))
    print(len(vector))
    a =  list(vector)
    print(type(a[30]))
    print(asizeof.asizeof(a))

    model_s = SentenceTransformer('paraphrase-xlm-r-multilingual-v1')
    sent_v = model_s.encode('Ahoj jak se máš ?').tolist()
    print(sent_v)
    print(len(sent_v))
    print(type(sent_v))


if __name__ == "__main__":
    main()
