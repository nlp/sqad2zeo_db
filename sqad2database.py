#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
import re
import os
from sqad_db import SqadDb
from sqad_db import Vocabulary
from sqad_db import W2V
from sqad_db import QAType
from sqad_db import KnowledgeBase
from sqad_db import Record
from sqad_db import word2id
from sqad_db import qt2id
from sqad_db import present_in_kb
from sqad_db import add2kb
from BTrees.OOBTree import BTree
import persistent.list
import urllib.parse
import sys

struct_re = re.compile('^<[^>]+>$')
q_type_re = re.compile('^<q_type>(.*)</q_type>$')
a_type_re = re.compile('^<a_type>(.*)</a_type>$')


def get_struct(data, struct_id):
    """
    Get structure from vertical
    :param data: str, vertical
    :param struct_id: str
    :return: str, senteces
    """
    struct = []
    struct_start = False
    read_counter = 0

    for line in data:
        read_counter += 1
        if read_counter > 500:
            sys.stderr.write('WARNING: get_struct has too much token for structure\n')
        if re.match("^<{}>$".format(struct_id), line.strip()) or \
                re.match("^<{} .*?>$".format(struct_id), line.strip()):
            struct_start = True
        if struct_start:
            struct.append(line.strip())
        if re.match("^</{}>$".format(struct_id), line.strip()):
            # Return only non empty structs
            if len(struct) > 2:
                yield struct
            struct = []
            read_counter = 0
            struct_start = False


def fill_with_sent(data, rec_part, vocabulary, w2v):
    """
    Fill content to database
    :param data: str
    :param rec_part: Record object
    :param vocabulary: dict
    :param w2v: word2vec model
    :return: None
    """
    for s in get_struct(data, 's'):
        sent = persistent.list.PersistentList()
        for token in s:
            if not struct_re.match(token) and token.strip():
                word, lemma, tag = token.strip().split('\t')[:3]
                wid = word2id(vocabulary, word, lemma, tag, w2v)
                sent.append(wid)
        rec_part.append(BTree({'sent': sent}))


def fill_not_sent(data, rec_part, vocabulary, w2v):
    """
    Fill content to database
    :param data: str
    :param rec_part: Record object
    :param vocabulary: dict
    :param w2v: word2vec model
    :return: None
    """
    sent = persistent.list.PersistentList()
    for token in data:
        if not struct_re.match(token) and token.strip():
            word, lemma, tag = token.strip().split('\t')[:3]
            wid = word2id(vocabulary, word, lemma, tag, w2v)
            sent.append(wid)
    rec_part.append(BTree({'sent': sent}))


def compare_sent(s1, s2):
    """
    Compare two senteces
    :param s1: str
    :param s2: str
    :return: bool
    """
    s1_words = [x.strip().split('\t')[0] for x in s1 if not re.match('^<[^>]*>$', x)]
    s2_words = [x.strip().split('\t')[0] for x in s2 if not re.match('^<[^>]*>$', x)]
    return s1_words == s2_words


def add_text(knowledge_base, url, text_content, title, answer_selection_content, vocabulary, w2v, rec_id,  no_annotation=False):
    """
    Add text to Knowledge base
    :param knowledge_base: kb object
    :param url: str
    :param text_content: str
    :param title: str
    :param answer_selection_content: str
    :param vocabulary: dict
    :param w2v: word2vec model
    :param rec_id: str
    :return: None
    """
    answer_sel_pos = -1
    answer_selection = []

    for i in get_struct(answer_selection_content, 's'):
        answer_selection.append(i)
    if len(answer_selection) > 1:
        sys.stderr.write(f'ERROR ({rec_id}): Answer selection has more than one sentence!\n')
        return '', -1

    if not present_in_kb(knowledge_base, url):
        text = BTree({'title': persistent.list.PersistentList(), 'text': persistent.list.PersistentList()})
        fill_not_sent(title, text['title'], vocabulary, w2v)

        for sent_num, s in enumerate(get_struct(text_content, 's')):
            if not no_annotation and compare_sent(s, answer_selection[0]):
                answer_sel_pos = sent_num
            sent = persistent.list.PersistentList()
            for token in s:
                if not struct_re.match(token):
                    word, lemma, tag = token.strip().split('\t')[:3]
                    wid = word2id(vocabulary, word, lemma, tag, w2v)
                    sent.append(wid)
            if len(sent) >= 1:
                text['text'].append(BTree({'sent': sent, 'ctx': BTree()}))
            else:
                sys.stderr.write(f'ERROR ({rec_id}): empty sentence\n')
        add2kb(knowledge_base, url, text)
    else:
        for sent_num, s in enumerate(get_struct(text_content, 's')):
            if not no_annotation and compare_sent(s, answer_selection[0]):
                answer_sel_pos = sent_num

    if answer_sel_pos != -1 or no_annotation:
        return url, answer_sel_pos
    else:
        sys.stderr.write(f'ERROR ({rec_id}): Answer selection not in text!\n')
        return '', -1


def fill_qa_type(data, qa_type):
    """
    Fill question answer types
    :param data:
    :param qa_type:
    :return:
    """
    q_type = -1
    a_type = -1
    for line in data:
        if q_type_re.match(line):
            q_type = qt2id(qa_type, q_type_re.match(line).group(1))
        elif a_type_re.match(line):
            a_type = qt2id(qa_type, a_type_re.match(line).group(1))
    return q_type, a_type


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Transform SQAD to one database file with all features')
    parser.add_argument('-p', '--path', type=str,
                        required=True,
                        help='Path to SQAD records root dir')
    parser.add_argument('-n', '--name', type=str,
                        required=True,
                        help='Resulting database name')
    parser.add_argument('--test', action='store_true',
                        required=False, default=False,
                        help='Testing switch. For development (no vectors are loaded)')
    parser.add_argument('-v', '--version', type=int,
                        required=True,
                        help='Version of database')
    parser.add_argument('--no_annotation', action='store_true',
                        required=False, default=False,
                        help='No manual annotation present - raw records from wiki')

    args = parser.parse_args()
    db_name = args.name
    db = SqadDb(file_name=db_name)
    rec_id_re = re.compile('(\d+)')
    counter = 0

    vocabulary = Vocabulary()
    qa_type = QAType()
    knowledge_base = KnowledgeBase()
    vectors = W2V(test=args.test)

    sys.stderr.write(f'Processing records:\n')
    for root, dirs, files in os.walk(args.path):
        if rec_id_re.match(root.split('/')[-1]):
            url = ''
            text_content = []
            answer_selection_content = []
            title = ''
            question = []
            answer_extraction = []
            rec_id = rec_id_re.match(root.split('/')[-1]).group(1)
            sys.stderr.write(f'{rec_id}({counter})\n')
            record = Record(rec_id)
            for file_name in files:
                if file_name in ['01question.vert', '03text.vert',
                                 '05metadata.txt', '06answer.selection.vert',
                                 '09answer_extraction.vert', '04url.txt',
                                 '10title.vert', '02yes_no_answer.vert']:
                    if os.path.exists(os.path.join(root, file_name)):
                        with open(os.path.join(root, file_name), 'r') as f:
                            if file_name == '01question.vert':
                                question = f.readlines()
                                fill_with_sent(question, record.question, vocabulary, vectors)
                            elif file_name == '05metadata.txt':
                                record.q_type, record.a_type = fill_qa_type(f.readlines(), qa_type)
                            elif file_name == '06answer.selection.vert':
                                answer_selection_content = f.readlines()
                                fill_with_sent(answer_selection_content, record.answer_selection, vocabulary, vectors)
                            elif file_name == '09answer_extraction.vert':
                                answer_extraction = f.readlines()
                                fill_not_sent(answer_extraction, record.answer_extraction, vocabulary, vectors)
                            elif file_name == '03text.vert':
                                text_content = f.readlines()
                            elif file_name == '02yes_no_answer.vert':
                                record.yes_no_answer = f.readline().strip()
                            elif file_name == '10title.vert':
                                title = f.readlines()
                            elif file_name == '04url.txt':
                                url = urllib.parse.unquote(f.readline()).strip()

            # Data checking
            if not args.no_annotation and not question:
                sys.stderr.write(f"ERROR: problem with question: {rec_id}: {question}\n")
                sys.exit()
            elif not args.no_annotation and (record.a_type == -1 or record.q_type == -1):
                sys.stderr.write(f"ERROR: problem with qa_type: {rec_id}: {record.a_type}/{record.q_type}\n")
                sys.exit()
            elif not args.no_annotation and (not answer_extraction and not record.yes_no_answer):
                sys.stderr.write(f"ERROR: problem with answer: {rec_id}: {answer_extraction}\n")
                sys.exit()

            if not args.no_annotation and (url and text_content and answer_selection_content and title):
                record.text, record.text_answer_position = add_text(knowledge_base, url, text_content, title,
                                                                    answer_selection_content, vocabulary, vectors,
                                                                    rec_id)
            elif args.no_annotation:
                record.text, record.text_answer_position = add_text(knowledge_base, url, text_content, title,
                                                                    answer_selection_content, vocabulary, vectors,
                                                                    rec_id, args.no_annotation)
            else:
                sys.stderr.write(f"ERROR: problem with text loading: \n"
                                 f"url: {url}\ntext_content: {len(text_content)}\ntile: {title}\n")
                sys.exit()
            counter += 1
            db.add(rec_id, record)

    sys.stderr.write(f'Storing vocabularies\n')

    db.add_vocab(vocabulary)
    db.add_qa_type(qa_type)
    db.add_kb(knowledge_base)
    db.init_ctx_types()
    db.version(args.version)
    db.update()
    db.close()
    sys.stderr.write(f'Created: {db_name}\n')


if __name__ == "__main__":
    main()
