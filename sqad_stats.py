#! /usr/bin/env python3
# coding: utf-8
import sys
from sqad_db import SqadDb
from query_database import get_record


def initialize_matrix(question_types, answer_types):
    matrix = {}

    for q_type in question_types:
        row = {}
        for a_type in answer_types:
            row.update({a_type: 0})
        matrix.update({q_type: row})
    return matrix


def count_usage(table, questions):
    for key, value in questions.items():
        q_type = key[0]
        a_type = key[1]
        table[q_type][a_type] += value


def pretty_print(table, question_types, answer_types):
    print("{:<24}".format(""), "".join("{:<15}".format(a_type) for a_type in answer_types))
    for q_type in question_types:
        print("{:<15}".format(q_type), end='')
        for a_type in answer_types:
            print("{:>15}".format(table[q_type][a_type]), end='')
        print()

def reverse_dict(dict_data):
    reverse = {}
    for position, count in dict_data.items():
        if reverse.get(count):
            reverse[count].append(position)
        else:
            reverse[count] = [position]
    return reverse

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Statistics on SQAD database metadata')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-d', '--database', type=str,
                        required=True,
                        help='Database path')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Debug mode')

    args = parser.parse_args()

    print(f'Database: {args.database}')

    q_type_dict = {}
    a_type_dict = {}
    q_a_type_dict = {}
    doc_stats = {}
    total = 0

    max_sentence_length = 0
    min_sentence_length = 1000
    average_sentence_length = []

    max_question_length = 0
    min_question_length = 1000
    average_question_length = []

    # For context type and its length compute length of items in tokens
    item_length_per_ctx_type = {}
    # For context type compute number of items (like number of phrases)
    item_number_per_ctx_type = {}
    # Answer position per Question Type
    a_pos_per_q_type = {}
    # General answer position
    a_pos_general = {}

    # Connect DB
    db = SqadDb(file_name=args.database, read_only=True)

    for record_id in db.get_all_records_id():
        record = get_record(db, record_id, False, word_parts='w')
        total += 1

        try:
            q_type_dict[record['q_type']] += 1
        except KeyError:
            q_type_dict[record['q_type']] = 1

        try:
            a_type_dict[record['a_type']] += 1
        except KeyError:
            a_type_dict[record['a_type']] = 1

        try:
            q_a_type_dict[(record['q_type'], record['a_type'])] += 1
        except KeyError:
            q_a_type_dict[(record['q_type'], record['a_type'])] = 1

        try:
            doc_stats[record['url']] += 1
        except KeyError:
            doc_stats[record['url']] = 1

        if args.verbose:
            print(f'ID: {record_id}')
            print(f'NO: {total}')
            print(f'q_type: {q_type_dict}')
            print(f'a_type: {a_type_dict}')
            print(f'doc_stats: {doc_stats}')

        # question stats
        for q_sent in record['question']:
            q_sent_length = len([x['word'] for x in q_sent['sent']])
            if args.verbose:
                print([x['word'] for x in q_sent['sent']])
                print(q_sent_length)
            if q_sent_length > max_question_length:
                max_question_length = q_sent_length
            if q_sent_length < min_question_length:
                min_question_length = q_sent_length
            average_question_length.append(q_sent_length)

        for sentence in record['text']:
            sent_length = len([x['word'] for x in sentence['sent']])
            if args.verbose:
                print([x['word'] for x in sentence['sent']])
                print(sent_length)
            if sent_length > max_sentence_length:
                max_sentence_length = sent_length
            if sent_length < min_sentence_length:
                min_sentence_length = sent_length
            average_sentence_length.append(sent_length)

            if sentence.get('ctx'):
                for ctx_type in sentence['ctx'].keys():

                    # create indexes
                    if not item_length_per_ctx_type.get(ctx_type, None):
                        item_length_per_ctx_type[ctx_type] = {}

                    if not item_number_per_ctx_type.get(ctx_type, None):
                        item_number_per_ctx_type[ctx_type] = {}

                    # compute stats
                    for prev_sent_idx in range(0, len(sentence['ctx'][ctx_type])):

                        # compute number stats
                        idx = prev_sent_idx
                        item_number = len(sentence['ctx'][ctx_type][idx])
                        while idx > 0:
                            idx -= 1
                            item_number +=  len(sentence['ctx'][ctx_type][idx])

                        # store number stats
                        if not item_number_per_ctx_type[ctx_type].get(prev_sent_idx, None):
                            item_number_per_ctx_type[ctx_type][prev_sent_idx] = [item_number]
                        else:
                            item_number_per_ctx_type[ctx_type][prev_sent_idx].append(item_number)

                        # compute length stats
                        idx = prev_sent_idx
                        item_length = sum([len(x["sent"]) for x in sentence['ctx'][ctx_type][idx]])
                        while idx > 0:
                            idx -= 1
                            item_length += sum([len(x["sent"]) for x in sentence['ctx'][ctx_type][idx]])

                        # store length stats
                        if not item_length_per_ctx_type[ctx_type].get(prev_sent_idx, None):
                            item_length_per_ctx_type[ctx_type][prev_sent_idx] = [item_length]
                        else:
                            item_length_per_ctx_type[ctx_type][prev_sent_idx].append(item_length)

            if args.verbose:
                print(f'item_length_per_ctx_type: {item_length_per_ctx_type}')
                print(f'item_number_per_ctx_type: {item_number_per_ctx_type}')

        # Count AS position per Question type
        if a_pos_per_q_type.get(record['q_type']):
            if a_pos_per_q_type[record['q_type']].get(record['a_sel_pos']):
                a_pos_per_q_type[record['q_type']][record['a_sel_pos']] += 1
            else:
                a_pos_per_q_type[record['q_type']][record['a_sel_pos']] = 1
        else:
            a_pos_per_q_type[record['q_type']] = {record['a_sel_pos']: 1}

        # Count general AS position in text
        if a_pos_general.get(record['a_sel_pos']):
            a_pos_general[record['a_sel_pos']] += 1
        else:
            a_pos_general[record['a_sel_pos']] = 1

        if args.verbose:
            print(f'=====================')

    print('\n=========================================================\n')
    print('Total: {}'.format(total))

    print('\n=========================================================\n')
    print('Q-Type statistics:')
    for key, value in q_type_dict.items():
        print('{:<15}{:>10}{:>10.2f}%'.format(key, value, float(value * 100) / total))

    print('\n=========================================================\n')
    print('A-Type statistics:')
    for key, value in a_type_dict.items():
        print('{:<15}{:>10}{:>10.2f}%'.format(key, value, float(value * 100) / total))

    print('\n=========================================================\n')
    print('Matrix:')
    matrix = initialize_matrix(q_type_dict.keys(), a_type_dict.keys())
    count_usage(matrix, q_a_type_dict)
    pretty_print(matrix, q_type_dict.keys(), a_type_dict.keys())

    print('\n=========================================================\n')
    single_doc_qa = 0
    multi_doc_qa = 0
    questions_per_doc = {}
    for key, value in doc_stats.items():
        if value == 1:
            single_doc_qa += 1
        else:
            multi_doc_qa += 1

        try:
            questions_per_doc[value].append(key)
        except KeyError:
            questions_per_doc[value] = [key]

    print('Number of question per document')
    for key, value in questions_per_doc.items():
        print('{:<10}{:>10}'.format(key, len(value)))

    print(f'Number of wikipedia articles used in SQAD datbase: {len(doc_stats.keys())}')
    print(f'Single question documents : {single_doc_qa}')
    print(f'Multi question documents : {multi_doc_qa}')

    print(f'Average sentence length: {sum(average_sentence_length) / len(average_sentence_length)}')
    print(f'Max sentence length: {max_sentence_length}')
    print(f'Min sentence length: {min_sentence_length}')

    print(f'Average question sentence length: {sum(average_question_length) / len(average_question_length)}')
    print(f'Max question sentence length: {max_question_length}')
    print(f'Min question sentence length: {min_question_length}')

    print('Average length of context items per N previous sentences')
    for ctx_type, prev_set_stats in item_length_per_ctx_type.items():
        for prev_sent_idx, stats in prev_set_stats.items():
            print(f'{ctx_type}\t{prev_sent_idx}\t{sum(stats) / len(stats):.2f}')

    print('Average number of context items per N previous sentences')
    for ctx_type, prev_set_stats in item_number_per_ctx_type.items():
        for prev_sent_idx, stats in prev_set_stats.items():
            print(f'{ctx_type}\t{prev_sent_idx}\t{sum(stats) / len(stats):.2f}')

    print(f'==========================================')
    print('General AS position')
    print('Count\tList of positions')
    reverse_a_pos_general = reverse_dict(a_pos_general)
    for count in sorted(reverse_a_pos_general):
        print(f'{count}:\t{sorted(reverse_a_pos_general[count])}')

    print(f'==========================================')
    print('AS position per question type')
    for q_tpye, position in a_pos_per_q_type.items():
        print(f'==========================================')
        print(f'{q_tpye}')
        reverse_position = reverse_dict(position)
        print('Count\tList of positions')
        for count in sorted(reverse_position):
            print(f'\t{count}:\t{sorted(reverse_position[count])}')


if __name__ == "__main__":
    main()

