#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
from sqad_db import SqadDb
import persistent.list
from query_database import get_content
import transaction
import itertools
from sqad_db import id2qt
import sys


def replace_number_lemma(sent):
    return [x['lemma'] if not x['lemma'] in ['[number]', '#num#'] else x['word'] for x in sent]


def get_all_partial(answer):
    result = []
    for i in range(1, len(answer)+1):
        result  += [x for x in itertools.combinations(answer, i)]
    return result


def partial_match(answer, sentence, a_type):
    answer_content = replace_number_lemma(answer)
    sentence_content = replace_number_lemma(sentence)

    # Only if question type is suitable for partial match and exact answer is more than one word
    if a_type in ['PERSON', 'DATETIME', 'ENTITY', 'LOCATION'] and len(answer_content) > 1:
        partial_answers = get_all_partial(answer_content)
        partial_answers.sort(key=len, reverse=True)
        while partial_answers:
            actual = partial_answers.pop(0)
            if ' '.join(actual) in ' '.join(sentence_content) and len(actual) >= float(len(answer_content) * 50) / 100:
                return True

    return False


def find_sentences_containing_answer(db, no_partial=False, verbose=False):
    """
    Searching for sentences containing the exact answer
    :param db: ZODB database
    :return: list of indexes with sentences within the text containing exact answer
    """
    vocabulary, qa_type_dict, kb = db.get_dicts()
    for rid in db.get_all_records_id():
        print(rid)
        record = db.get_record(rid)
        a_type = id2qt(qa_type_dict, record.a_type)
        containing_answer = persistent.list.PersistentList()
        containing_answer_partial = persistent.list.PersistentList()

        for sent in get_content(record.answer_extraction, vocabulary, old=False):
            ans_ext_lemma = ' '.join(replace_number_lemma(sent['sent']))
            for idx, sent_and_phrs in enumerate(get_content(kb.url2doc.get(record.text)['text'], vocabulary, old=False)):
                doc_sent_content = " ".join(replace_number_lemma(sent_and_phrs["sent"]))
                if ans_ext_lemma in doc_sent_content:
                    if verbose:
                        print(f'Full match: {ans_ext_lemma} -> {doc_sent_content}')
                    containing_answer.append(idx)

                elif not no_partial and partial_match(sent['sent'], sent_and_phrs["sent"], a_type):
                    if verbose:
                        print(f'Partial match: {ans_ext_lemma} -> {doc_sent_content}')
                    containing_answer_partial.append(idx)

        yield record, containing_answer, containing_answer_partial


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Sentences containing exact answer.')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-d', '--db_path', type=str,
                        required=False, default='',
                        help='Database path')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Verbose mode')
    parser.add_argument('--no_partial', action='store_true',
                        required=False, default=False,
                        help='Compute partial match')
    args = parser.parse_args()

    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    try:
        for record, sent_containing_answer, containing_answer_partial in find_sentences_containing_answer(db, no_partial=args.no_partial, verbose=args.verbose):
            if args.verbose:
                print(f'{record.rec_id}: {sent_containing_answer} ::: {containing_answer_partial}')
                print('==============================')
            record.similar_answers['sents_containing_ans_ext'] = sent_containing_answer
            record.similar_answers['sents_containing_ans_ext_partial'] = containing_answer_partial
            db._p_changed = True
            transaction.commit()
        db.update()
        db._p_changed = True
        transaction.commit()
        db.close()
    except KeyboardInterrupt:
        db.close()
        sys.exit()


if __name__ == "__main__":
    main()

#TODO testing on 001975