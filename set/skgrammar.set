# Copyright 2008 Vojtech Kovar
#
# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# This is the SET grammar for Slovak

##############################################################################
# word classes / aliases

# "verb", "noun", "prep" and "adj" are used by the parser internally, so
# it is a good idea to define them
# for --marx option, the following must be defined in addition:
#   "pron", "num", "adv", "abbr", "conj", "part", "punct"
# (if some of them does not exist in the tagset, just use a never-matching tag)

CLASS noun (tag k1)
CLASS adj (tag k2)
CLASS comma (word ,|-)
CLASS verbfin (tag k5.*m[IBRAPN])
CLASS verb (tag k5)
CLASS modalverb (lemma byť|bývať|mať|chcieť|môcť|musieť|smieť|hodlať|vedieť|doviesť|dokázať|trúrnuť|dať|nechať|možno|nutno|)
CLASS vpart (word by)
CLASS prep (tag k7)
CLASS num (tag k4)
CLASS infinitive (tag k5.*mF)
CLASS pron (tag k3)
CLASS relpron (tag k3.*yR|k3.*xQ)
CLASS adv (tag k6)
CLASS abbr (tag kA)
CLASS conj (tag k8)
CLASS cconj (tag k8.*xC)
CLASS sconj (tag k8.*xS)
CLASS part (tag k9)
CLASS punct (tag kI)


##############################################################################
::: relative clauses :::
# and other... TODO redesign labels

# subordinated clauses
TMPL: bound $CONJ ... verbfin ... comma  ... verbfin
        MARK 0 3 5 <clause>  HEAD 3  DEP 7  PROB 50
TMPL: verbfin ... comma $CONJ ... verbfin ... rbound
        MARK 2 5 7 <clause>  HEAD 3  DEP 0  PROB 50
	$CONJ(tag): k3.*yR k3.*xQ k8.*xS

TMPL: $LIKENOUN ... comma $CONJ ... verbfin ... rbound
        AGREE 0 3 gn  MARK 2 5 7 <clause>  HEAD 3  DEP 0
TMPL: $LIKENOUN ... comma prep $CONJ ... verbfin ... rbound
	AGREE 0 4 gn  AGREE 3 4 c  MARK 2 6 8 <clause>  HEAD 6  DEP 0
	$CONJ(tag): k3.*yR k3.*xQ

# coordinated clauses
TMPL: bound ... verbfin ... rbound	MARK 0 2 4 <clause>	HEAD 2	PROB 5

# interjections
TMPL: $START ... $END MARK 0 2 
MATCH $START(word) $END(word)
	(	)
END


# interjections
#TMPL: $START $...* $END 	MARK 0 2 <inter>	HEAD 2
#MATCH $START(word) $END(word)
#	(	)
#END
#	$...*(tag not): k5.*m(IBRAPN)
#
# aposition attributes
#TMPL: noun $...* (word ,) $...* (tag k[12]) ... rbound	AGREE 0 4 gnc	MARK 0 2 4 6 <attr>	HEAD 0
#	$...*(tag not): k5 k8

# tags that can be in position of a noun
MATCH $LIKENOUN(tag) $LIKENOUN(tag)
	k1	k1
	kA	kA
	k3.*xP	k3.*xP  PROB 50
	k3.*xO	k3.*xO	PROB 50
	k3.*xD	k3.*xD	PROB 50
	k3.*xT	k3.*xT	PROB 50
	k4.*xC	k4.*xC
	k4.*bN	k4.*bN
END


##############################################################################
::: coordinations :::

# coordinations of adjectives
TMPL: (tag k2) $...* $AND $...* (tag k2)	AGREE 0 4 c		MARK 0 2 4 <coord>	HEAD 2
TMPL: (tag k2) $...* (word ,) $BUT $...* (tag k2)	AGREE 0 5 c		MARK 0 2 3 5 <coord>	HEAD 3
	$...*(tag not): k5 k4 k3 k1 k7 k8 kA k2
	$BUT(word): ale nie avšak však

# coordinations of nouns, abbreviations and nums
TMPL: $N1 $...* $AND $...* $N2		AGREE 0 4 c		MARK 0 2 4 <coord>	HEAD 2  PROB 300
	$...*(tag not): k5 k8 kI k7

MATCH $N1(tag) $N2(tag):
    k4.*xC    k4.*xC
    k1  k1
    kA  kA
END

# coordination of infinitives
TMPL: infinitive $...* $AND $...2* infinitive  MARK 0 2 4 <coord>  HEAD 2

# coordinations of prepositional phrases
TMPL: prep $...* $AND $...2* prep	AGREE 0 4 c		MARK 0 2 4 <coord>	HEAD 2
#TMPL: prep $...* $AND $...2* prep	MARK 0 2 4 <coord>	HEAD 2  PROB 10
	$...*(tag not): k5
	$...2*(tag not): k5 kI

# atypical coordinations
TMPL: noun $...* $APOD (word .)	MARK 0 2 3 <coord>	HEAD 2	IMPORTANT 0
TMPL: noun $...* (word a) (word podobne)
		MARK 0 2 3 <coord>	HEAD 2	IMPORTANT 0
	$...*(tag not): k5 k8 k7

	$APOD(word): apod aj atď etc

MATCH $AND(word) $AND(word)
	a	a
	ani	ani
	alebo	alebo
	či	či
#	ale	ale
	,	,	PROB 10
	-	-	PROB 10
END

##############################################################################
::: dependencies :::

###############################################################################
# noun and prepositional phrases

# Pavel Bem
TMPL: noun (tag k1.*c[134567])  AGREE 0 1 nc  MARK 1 DEP 0  LABEL spec PROB 5000
TMPL: noun $...* (tag k1.*c[134567])  AGREE 0 2 gnc  MARK 2 DEP 0  LABEL spec PROB 300

# cerny pes
TMPL: $ATTR $...* noun	AGREE 0 2 gnc	MARK 0	DEP 2   PROB 6000   LABEL modifier
TMPL: $ATTR $...* (tag kA)	AGREE 0 2 gnc	MARK 0	DEP 2		PROB 6000	LABEL modifier
TMPL: $NUM noun	AGREE 0 1 gnc	MARK 0	DEP 1		PROB 6000	LABEL modifier
TMPL: $NUM2 $N367	AGREE 0 1 gnc	MARK 0	DEP 1		PROB 6000	LABEL modifier

    $N367(tag): k1.*c(367)
	$NUM(lemma): jeden jedna dve dva tri štyri oba
	$NUM2(tag): k4.*xC k4.*bN

# kyselina sirova
TMPL: noun $...* $ATTR	AGREE 0 2 gnc  MARK 2   DEP 0    PROB 200	LABEL modifier

# ve meste
TMPL: prep $...* $LIKENOUN		AGREE 0 2 c		MARK 2	DEP 0	PROB 5000   LABEL prep-object

# ministr skolstvi
TMPL: noun $...* (tag k1.*c2)		MARK 2	DEP 0		PROB 1000	LABEL genitiv
TMPL: noun (tag kA)		MARK 1	DEP 0	LABEL	genitiv   PROB 400

# paragraf 14
TMPL: noun num		MARK 1	DEP 0		PROB 1000	LABEL modifier
TMPL: noun $PART num		MARK 2	DEP 0		PROB 1000	LABEL modifier

# (ve) filmu Poupata
TMPL: noun (tag k1.*c1)		MARK 1	DEP 0		PROB 1000	LABEL modifier

# zakon cislo
TMPL: noun $NOUN		MARK 1	DEP 0		PROB 1000	LABEL modifier
	$NOUN(lemma): číslo 

	$...*(tag not): kI k5 k8 k9 k7
    $...*(word not): sa si

# ministr z (ODS)
TMPL: noun $...* prep	MARK 2	DEP 0		PROB 200	LABEL additional-prep
TMPL: adj $...* prep	MARK 2	DEP 0		PROB 120	LABEL additional-prep

	$...*(tag not): kI k5 k8 k9 k7 k6
    $...*(word not): sa si

TMPL: (tag kA) (tag kA)   MARK 1  DEP 0   PROB 300	LABEL additional-prep

###############################################################################
# numeral phrases

# pet psu
TMPL: $NUM $...* (tag k1.*c2.*nP|k1.*nP.*c2)  MARK 2  DEP 0  PROB 1000  LABEL genitiv
TMPL: $NUM2 $...* $AB_N		AGREE 0 2 gnc	MARK 2	DEP 0		PROB 1000	LABEL genitiv
TMPL: $NUM (tag kA)	MARK 1	DEP 0	LABEL genitiv
TMPL: $NUM2 (tag kA)	MARK 1	DEP 0	LABEL genitiv

	$AB_N(tag): k[A1]
	$NUM(tag): k4
	$NUM(lemma not): jeden jedna dve dva tri štyri oba 1 2 3 4
	$NUM2(lemma): jeden jedna dve dva tri štyri oba 1 2 3 4

	$...*(tag not): kI k5 k8 k9 k1

###############################################################################
# adjective phrases

# okamzite vypoveditelne
TMPL: (tag k2) $...2* $ADV	MARK 2	DEP 0  PROB 20  LABEL modifier
TMPL: $ADV $...* (tag k2)	MARK 0	DEP 2	LABEL modifier
TMPL: (tag k2) $...* prep	MARK 2	DEP 0	LABEL modifier
TMPL: prep $...* (tag k2)	MARK 0	DEP 2  PROB 20	LABEL modifier

	$...*(tag not): kI k5 k8 k7
	$...2*(tag not): kI k5 k8 k7 k1

# (smlouva) upravujici vztahy
TMPL: (tag k2.*rD) $...* $OBJ 	MARK 2	DEP 0	LABEL object
TMPL: (tag k2) $...* $OBJ	MARK 2	DEP 0		PROB 50	LABEL object
TMPL: $OBJ $...* (tag k2)   MARK 0   DEP 2		PROB 20	LABEL object

	$OBJ(tag): k1 kA k4.*xC k4.*bN
	$...*(tag not): kI k5 k8

###############################################################################
# verb phrases

# videl chlapa (all verb objects and subjects)
TMPL: $MAINVERB $...* $LIKESUBJ  AGREE 0 2 gn  MARK 2  DEP 0  PROB 602  LABEL subject
TMPL: $LIKESUBJ $...* $MAINVERB  AGREE 0 2 gn  MARK 0  DEP 2  PROB 602  LABEL subject
TMPL: $MAINVERB $...* $LIKESUBJ  MARK 2  DEP 0  PROB 601  LABEL subject-bad
TMPL: $LIKESUBJ $...* $MAINVERB  MARK 0  DEP 2  PROB 601  LABEL subject-bad
TMPL: $MAINVERB $...* $LIKENOUN	MARK 2	DEP 0	PROB 600	LABEL object
TMPL: $LIKENOUN $...* $MAINVERB	MARK 0	DEP 2	PROB 600	LABEL object
TMPL: $MAINVERB $...* (word sa|si)	MARK 2	DEP 0		PROB 600	LABEL object
TMPL: (word sa|si) $...* $MAINVERB	MARK 0	DEP 2		PROB 600	LABEL object

# sel do (mesta)
TMPL: $MAINVERB $...* prep		MARK 2	DEP 0		PROB 300	LABEL additional-prep
TMPL: prep $...* $MAINVERB		MARK 0	DEP 2		PROB 300	LABEL additional-prep

# videl dobre
TMPL: $MAINVERB $...* $ADV	MARK 2	DEP 0	LABEL adverb
TMPL: $ADV $...* $MAINVERB	MARK 0	DEP 2	LABEL adverb

    $ADV(tag): k6
    $ADV(word not): až
    $...*(tag not): kI k3.*yR k3.*xQ
    $...*(word not): a ani alebo

TMPL: (tag k5) $...* (tag k5) MARK 0 DEP 2


# sel jsem, mohl by, ...
TMPL: $PARTICIP $SUBVERB	MARK 1	DEP 0	PROB 1000	LABEL auxiliary-verb
TMPL: $SUBVERB $...* $PARTICIP	MARK 0	DEP 2	PROB 1000	LABEL auxiliary-verb

#sk
TMPL: $PARTICIP $...* $BY $BYBYT MARK 2 DEP 0    PROB 1000	LABEL auxiliary-verb
	$...*(tag not): kI k8

TMPL: $BY $...* $PARTICIP    MARK 0 DEP 2   PROB 1000	LABEL auxiliary-verb
TMPL: $BY $BYBYT $...* $PARTICIP  MARK 0 DEP 3 PROB 1000	auxiliary-verb
TMPL: $BY $BYBYT MARK 1 DEP 0 PROB 1000	auxiliary-verb

TMPL: (word je) $...* (tag k5.*mN)    MARK 0 DEP 2   PROB 1000	auxiliary-verb
TMPL: (word nie) (word je) $...* (tag k5.*mN)    MARK 1 DEP 3   PROB 1000	auxiliary-verb

	$PARTICIP(tag): k5.*m[NAI]
	$PARTICIP(lemma not): byť
	$BY(word): by
	$BYBYT(word): som si sme ste

	$...*(tag not): kI
	$SUBVERB(lemma): byť
	$SUBVERB(word not): je není

# byl cerny
TMPL: $BYT $...* $NOMINSTRADJ  MARK 2  DEP 0  PROB 800	LABEL adj-object

    $NOMINSTRADJ(tag): k2.*c(17)

# mohl udelat
TMPL: verbfin $...* infinitive     MARK 2 DEP 0	LABEL verb-object
TMPL: infinitive $...* verbfin     MARK 0 DEP 2	LABEL verb-object

    $...*(tag not): kI k5.*mN
    $...*(word not): a alebo či

###############################################################################
# other

# do kdy
TMPL: $MASTER $SLAVE		MARK 1	DEP 0		PROB 1000	LABEL mwe
MATCH $MASTER(word) $SLAVE(word)
	do	kedy
	súbežne	s
	vzhľadom	k
END

# strasne moc
TMPL: $SLAVE $MASTER  MARK 0  DEP 1  PROB 1000  LABEL modifier
MATCH $SLAVE(word) $MASTER(word)
    strašne moc
    hrozne moc
    veľmi spôsobne
    velmi spôsobne
    velmi dobre
    velmi dôkladne
END


# temer pet (psu)
TMPL: $PART (tag k[123456])	MARK 0	DEP 1		PROB 1000	LABEL modifier
TMPL: $PART $...* (tag k[123456])	MARK 0	DEP 2	LABEL modifier

	$PART(word): takmer i približne asi zhruba najmenej minimálne maximálne nejviac ešte len aspoň cca už iba až tak treba
    $...*(tag not): k8
    $...*(word not): keby

#TMPL: (tag k5) ... (tag .*)  MARK 2  DEP 0  PROB 3	LABEL modifier
#TMPL: (tag .*) ... (tag k5)  MARK 0  DEP 2  PROB 3	LABEL modifier
#TMPL: (tag .*) (tag .*)  MARK 1  DEP 0  PROB 1	LABEL modifier

###############################################################################
# general variable definitions

# tags that can be in position of an adjective
MATCH $ATTR(tag) $ATTR(tag)
	k2	k2
	k3.*xO	k3.*xO
	k3.*xD	k3.*xD
	k3.*yR	k3.*yR
	k3.*yQ	k3.*yQ
	k4.*xO	k4.*xO
	k4.*xR	k4.*xR
	k3.*xT	k3.*xT	PROB 30
	k3.*xT	k3.*xT	PROB 30
	k3.*yI	k3.*yI  PROB 15
	k3.*yN	k3.*yN  PROB 15
END

# the probability distribution of verb tags
MATCH $MAINVERB(tag) $MAINVERB(tag)
	k5.*mF	k5.*mF	PROB 110
	k5.*mA	k5.*mA	PROB 100
	k5.*mN	k5.*mN	PROB 100
    k5.*mI  k5.*mI    PROB 70
	k5	k5	PROB 20
END

# verb 'byt'
$BYT(lemma): být bývat stát stávat
MATCH $BYT(tag) $BYT(tag)
	k5.*mF	k5.*mF	PROB 100
	k5.*mA	k5.*mA	PROB 100
	k5	k5	PROB 45
END

# tags that can be in position of a noun
MATCH $LIKENOUN(tag) $LIKENOUN(tag)
	k1	k1
	k3.*xP	k3.*xP
	kA	kA
	k2	k2	PROB 4
	k3.*xO	k3.*xO	PROB 10
	k3.*xD	k3.*xD	PROB 10
	k3.*xT	k3.*xT	PROB 10
	k3.*yR	k3.*yR	PROB 30
	k3.*yQ	k3.*yQ	PROB 30
	k4.*xC	k4.*xC
	k4.*bN	k4.*bN
END

MATCH $LIKESUBJ(tag) $LIKESUBJ(tag)
	k1.*c1	k1.*c1
	k3.*c1.*xP	k3.*c1.*xP
	k3.*xP.*c1	k3.*xP.*c1
	kA	kA
	k2.*c1	k2.*c1	PROB 4
	k3.*c1.*xO	k3.*c1.*xO	PROB 10
	k3.*xO.*c1	k3.*xO.*c1	PROB 10
	k3.*c1.*xD	k3.*c1.*xD	PROB 10
	k3.*xD.*c1	k3.*xD.*c1	PROB 10
	k3.*c1.*xT	k3.*c1.*xT	PROB 10
	k3.*xT.*c1	k3.*xT.*c1	PROB 10
	k3.*c1.*yR	k3.*c1.*yR	PROB 30
	k3.*yR.*c1	k3.*yR.*c1	PROB 30
	k3.*c1.*yQ	k3.*c1.*yQ	PROB 30
	k3.*yQ.*c1	k3.*yQ.*c1	PROB 30
	k4.*c1.*xC	k4.*c1.*xC
	k4.*xC.*c1	k4.*xC.*c1
	k4.*c1.*bN	k4.*c1.*bN
	k4.*bN.*c1	k4.*bN.*c1
END




