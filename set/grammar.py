#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# performs loading grammar into a grammar object

import re

# grammar globals
# the first keyword in the following _must_ be the TMPL marker
keywords = ['TMPL:', 'MARK', 'AGREE', 'DEP', 'PROB', 'HEAD', 'IMPORTANT',
            'LABEL', 'LABELDEP']
    # the following is used for parsing the "$1[tag]: blah blah" lines ...
listdef_re = re.compile('^(\$[^$\(]+)\(([^\)]*)\):(.*)$')
    # ... and for the "MATCH $1[tag] $3[word]" lines
match_re = re.compile('^MATCH (\$[^$\(]+)\(([^\)]*)\) (\$[^$\(]+)\(([^\)]*)\)')

rule_groups = ['relclause coords', 'relative clauses', 'customs',
                                               'coordinations', 'dependencies']
DEFAULT_PROB = 100


def divide_tmpl_line(tmpl_line):
    if not tmpl_line.startswith('TMPL: '):
        raise RuntimeError('Grammar Error: Unrecognized template begin: %s'
                                                 % ' '.join(tmpl_line.split()))
    tmpl_tokens = []
    tmpl_pre = tmpl_line.split()[1:]
    i = 0
    fix_brackets = True
    while i < len(tmpl_pre): # fix bad division
        if tmpl_pre[i] in keywords:
            fix_brackets = False
        if fix_brackets and tmpl_pre[i].startswith('('):
            if i + 1 == len(tmpl_pre):
                raise RuntimeError('Grammar Error: Token not closed: %s: %s'
                                                    % (tmpl_pre[i], tmpl_line))
            elif not tmpl_pre[i+1].endswith(')'):
                raise RuntimeError('Grammar Error: Corrupted token: %s: %s'
                                                    % (tmpl_pre[i], tmpl_line))
            else:
                tmpl_tokens.append(tmpl_pre[i] + ' ' + tmpl_pre[i+1])
                i += 2
        else: # dollar or alias or spec
            tmpl_tokens.append(tmpl_pre[i])
            i += 1
    return tmpl_tokens


class Rule:

    def __init__(self, lines=[], alias={}):
        self.actions = {} # dict action: [list_of_arguments]
        self.template = [] # dictionaries
        self.probability = 100
        self.sisters = [] # tuples (pos1, pos2, field1, field2, prob_list)
        self.tmpl_tokens = []
        self.alias = alias
        if lines: self.parse_lines(lines)


    def view(self):
        return ' '.join(self.tmpl_tokens)


    def parse_lines(self, lines):
        tmpl_lines = [line for line in lines if line.split()[0] in keywords]
        tmpl_line = '\t'.join(tmpl_lines)
        self.tmpl_tokens = divide_tmpl_line(tmpl_line)
        dollar_search_list = self.create_dollar_search_list(lines)

        parse_action = ''
        for token in self.tmpl_tokens: # 'token' is string here
            if token in keywords:
                parse_action = token
                if not token in self.actions: self.actions[token] = []
                if token in ('LABEL', 'LABELDEP'):
                    self.actions[token].append('')
            elif parse_action:
                if parse_action in ('LABEL', 'LABELDEP'):
                    if self.actions[parse_action][-1]:
                        self.actions[parse_action][-1] += ' '
                    self.actions[parse_action][-1] += token
                else:
                    self.actions[parse_action].append(token)
            if not parse_action:
                self.parse_tmpl_token(token, dollar_search_list)
        if not self.actions.get('MARK'):
            raise RuntimeError('Grammar Error: Missing MARK action: %s'
                                                                 % self.view())
        if self.actions.get('MARK') and not self.actions.get('IMPORTANT'):
            self.actions['IMPORTANT'] = self.actions['MARK']


    def create_dollar_search_list(self, lines):
        # generates search list for dollar-variables expansion
        result = {}
        match_found = False
        for line in lines:
            if line == 'END':
                match_found = False
                if not result.get(dollar1): result[dollar1] = {}
                if not result.get(dollar2): result[dollar2] = {}
                if field1.startswith('tag'):
                    result[dollar1][field1] = [re.compile(x) for x in values1]
                else:
                    result[dollar1][field1] = values1
                if field1.startswith('tag'):
                    result[dollar2][field2] = [re.compile(x) for x in values2]
                else:
                    result[dollar2][field2] = values2
                index1 = self.tmpl_tokens.index(dollar1)
                index2 = self.tmpl_tokens.index(dollar2)
                self.sisters.append((index1, index2, field1, field2, probs))
            # read match list
            if match_found:
                line_list = line.split()
                # append probabilities
                if 'PROB' in line_list:
                    try:
                        prob = int(line_list[line_list.index('PROB') + 1])
                    except:
                        raise RuntimeError(
                                        'LoadGrammar Error: undefined PROB: %s'
                                                                 % self.view())
                else:
                    prob = DEFAULT_PROB
                values1.append(line_list[0])
                values2.append(line_list[1])
                probs.append (prob)
                if 'SYMM' in line_list:
                    values1.append(line_list[1])
                    values2.append(line_list[0])
                    probs.append (prob)
            # read normal list
            m = listdef_re.match(line)
            if m:
                dollar, field, values = m.group(1), m.group(2), m.group(3)
                if not result.get(dollar):
                    result[dollar] = {}
                if field.startswith('tag'):
                    result[dollar][field] = [re.compile(x)
                                               for x in values.strip().split()]
                else:
                    result[dollar][field] = set(values.strip().split())
            # match list begin detection
            m = match_re.match(line)
            if m:
                match_found = True
                dollar1, dollar2 = m.group(1), m.group(3)
                field1, field2 = m.group(2), m.group(4)
                values1, values2, probs = [], [], []
        return result


    def parse_tmpl_token(self, token, dollar_search_list):
        if token in self.alias:
            self.template.append(self.alias[token])
        elif token in ['...', 'bound', 'rbound', 'start']:
            self.template.append({ 'spec': token })
        elif token.startswith('('):
            spl_token = token.strip('()').split()
            key = spl_token[0]
            if key.startswith('tag'): # special format for tag
                value = [re.compile(spl_token[1])]
            else:
                value = set(spl_token[1].split('|'))
            self.template.append({ key: value })
        elif token.startswith('$'):
            try:
                self.template.append(dollar_search_list[token])
            except KeyError:
                raise RuntimeError('Grammar Error: Could not find %s: %s'
                                                        % (token, self.view()))
            if token[-1] == '*': # $VARIABLE*
                self.template[-1]['spec'] = '...'
        else: # not alias nor dollar nor standard token nor spec
            raise RuntimeError('Grammar Error: Unrecognized token: %s in %s'
                                                        % (token, self.view()))


class Grammar:

    def __init__ (self, filename='grammar.set'):
        from collections import defaultdict
        self.wclass = defaultdict(lambda:None, {})
        self.interjections = [] # tuples of strings
        self.ends = [] # regular expressions
        self.starts = [] # regular expressions
        self.hard_delimiters = [] # strings
        self.relative_clauses = [] # rules
        self.customs = [] # rules
        self.coordinations = [] # rules
        self.dependencies = [] # rules
        self.toplabel = ""
        self.load_grammar_from_file(filename)


    def load_grammar_from_file(self, filename):
        try:
            gfile = open(filename)
            grammar_lines = []
            for line in gfile:
                if line.startswith('CLASS'):
                    name, token = line.strip().split(None, 2)[1:]
                    spl_token = token.strip('()').split()
                    key = spl_token[0]
                    if key.startswith('tag'): # special format for tag
                        value = [re.compile(spl_token[1])]
                    else:
                        value = set(spl_token[1].split('|'))
                    self.wclass[name] = {key: value}
                elif line.startswith('LABELTOP'):
                    self.toplabel = line.strip().split(' ', 1)[1]
                else:
                    grammar_lines.append(line.strip())
            gfile.close()
        except IOError as detail:
            raise IOError('Grammar not found: %s' % detail)
        self.load_group('interjections', grammar_lines, self.interjections)
        self.load_group('starts', grammar_lines, self.starts)
        self.load_group('ends', grammar_lines, self.ends)
        self.load_group('hard delimiters', grammar_lines, self.hard_delimiters)
        self.load_group('relative clauses', grammar_lines,
                                                         self.relative_clauses)
        self.load_group('customs', grammar_lines, self.customs)
        self.load_group('coordinations', grammar_lines, self.coordinations)
        self.load_group('dependencies', grammar_lines, self.dependencies)


    def load_group(self, group, grammar_lines, list_to_add):
        relevant = False
        lines_to_process = []
        for line in grammar_lines:
            if not line or line.startswith('#'): continue
            if line == '::: %s :::' % group:
                relevant = True
            elif line.startswith(':::'):
                relevant = False
            if relevant and not line.startswith(':::'):
                # switch cases
                if group == 'interjections':
                    self.interjections.append(tuple(line.split()))
                if group == 'ends':
                    self.ends.append(re.compile('^%s$' % line))
                if group == 'starts':
                    self.starts.append(re.compile('^%s$' % line))
                if group == 'hard delimiters':
                    self.hard_delimiters.append(line)
                else:
                    lines_to_process.append(line)
        if group in rule_groups:
            self.load_rules(lines_to_process, list_to_add)


    def load_rules(self, lines, list_to_add):
        # finds right lines and inserts it into list_to_add
        for index, topline in enumerate(lines):
            if topline.startswith('TMPL'): # new rule
                rule_lines = []
                rule_lines.append(topline)
                for line in lines[index+1:]:
                    if line.split()[0] in keywords[1:]: rule_lines.append(line)
                    else: break
                dollars = [a for a in topline.split() if a.startswith('$')]
                # now find corresponding dollar lists
                finished_dollars = set([])
                for dollar in dollars:
                    match_found = False
                    for line in lines[index:]:
                        if line.startswith(dollar + '(') \
                                or line.startswith(dollar + ' '):
                            rule_lines.append(line) # usual definition
                            finished_dollars.add(dollar)
                        else:
                            if not match_found and dollar in finished_dollars:
                                break
                            m = match_re.match(line)
                            if m: # match definition
                                if m.group(1) == dollar or m.group(3) == dollar:
                                    finished_dollars.add(m.group(1))
                                    finished_dollars.add(m.group(3))
                                    match_found = True
                        if match_found:
                            rule_lines.append(line)
                        if line == 'END':
                            match_found = False
                for dollar in dollars:
                    if not dollar in finished_dollars:
                        raise RuntimeError(
                                  "Grammar Error: %s could not be found for %s"
                                                           % (dollar, topline))
                list_to_add.append(Rule(rule_lines, alias=self.wclass))

