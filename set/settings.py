#!/usr/bin/env python

# Copyright 2009 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# advanced settings of the parser

COLL_BONUS = 1.2    # multiplicative constant for collocations probability
VALENCY_BONUS = 100 # multiplicative constant for valency probability

SMALL_PENALTY = 2.0
    # division penalty for matching already found phrases instead of
    # surface tokens
CROSS_PENALTY = 10.0
    # division penalty for non-projective dependencies
IGNORETAGS = True # ignoring XML-like tags in vertical
IGNOREDOCTAGS = True # ignoring doc tags in vertical
IGNORE_WRONG_MORPHOLOGY = True # do not fall if tags are badly formed
