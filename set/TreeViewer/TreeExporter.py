#!/usr/bin/python

# Copyright 2011 Milos Jakubicek, Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


import sys, cairo, os.path

class Node():# represents a tree node in the graphical view

    def __init__(self, id, label, dep_id, dep_type, from_left, from_top,
                 deplabel=''):
        self.id = int(id)
        self.label = label.decode('utf8')
        self.dep_id = int(dep_id)
        self.dep_type = dep_type
        self.x = from_left
        self.y = from_top
        self.deplabel = deplabel

class TreeView():

    def __init__(self, trees, parent=None):
        self.treeNumber = None
        self.nodes = None
        self.trees = trees
        self.scale = 1
        self.width = 0
        self.height = 0
        self.sentence = None
        self.font_size = 10
        self.x_space = self.font_size
        self.y_space = self.font_size * 3
        # set a dummy context so that we can use sw(), will be overwritten later
        self.cr = cairo.Context(cairo.ImageSurface(cairo.FORMAT_A1, 1, 1))
        self.cr.set_font_size(self.font_size)
        self.cr.set_line_width(1)
        #cr.select_font_face("Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
        self.sh = self.cr.text_extents("X")[3] # height of character X according to the font metric
        self.from_top = self.sh * 2
        self.from_left = self.sw("XXXXX")


    def sw (self, s): # string width according to font metric
        return self.cr.text_extents(s)[2]

    def paint(self):
        sentence = []
        
        for node in self.nodes:
            if node.label.startswith('<'):
                self.cr.set_source_rgb(0, 0, 1)
            else:
                self.cr.set_source_rgb(0, 0, 0)
                sentence.append(node.label)
            self.cr.move_to(node.x - self.sw(node.label) / 2.0, node.y)
            self.cr.show_text(node.label)
            self.cr.move_to(node.x - self.sw(node.label) / 2.0, node.y + 7)
            self.cr.set_source_rgb(0, 0.7, 0)
            self.cr.show_text(node.deplabel)
            if node.dep_id > -1:
                if node.dep_id < 10000:
                    depNode = self.nodes[node.dep_id]
                else:
                    depNode = self.nodes[-1] # nasty hack
                if node.dep_type == 'p':
                    self.cr.set_source_rgb(0, 0, 1)
                else:
                    self.cr.set_source_rgb(0, 0, 0)
                self.cr.move_to(depNode.x, depNode.y + self.sh * 0.4)
                self.cr.line_to(node.x, node.y - self.sh * 1.4)
                self.cr.stroke()
       
    def setTree(self, treeNumber):
        self.treeNumber = treeNumber
        self.compute_tree()

    def compute_tree(self):
        # TODO divide the code
        nodes = []
        for line in self.trees[self.treeNumber - 1]:
            if len(line) == 0 or line.isspace():
                continue
            try:
                ll = line.strip().split('\t') + ['']
                id, label, dep_id, dep_type, deplabel = (ll[0], ll[1], ll[2],
                                                         ll[3], ll[4])
            except:
                print >> sys.stderr, "Invalid input: %s" % line
                sys.exit(1)
            nodes.append(Node(id, label, dep_id, dep_type, self.from_left,
                              self.from_top, deplabel))
        if not nodes: self.nodes = []; return
        # x coordinations
        x = self.from_left
        for index, node in enumerate(nodes):
            node.x = x
            if not node.label.startswith('<'):
                shift = (self.sw(node.label) + self.sw(node.deplabel)) / 2.0
                if index + 1 < len(nodes):
                    shift += self.sw(nodes[index+1].label) / 2.0
                x += self.x_space + shift
        # y coordinations
        nodes_from_bottom = []
        y = self.from_top
        next_level = [-1]
        next_next_level = []
        while next_level:
            for id in next_level:
                for node in nodes:
                    if node.dep_id == id:
                        nodes_from_bottom.append(node)
                        node.y = y
                        next_next_level.append(node.id)
            next_level = next_next_level
            next_next_level = []
            y += self.y_space

        nodes_from_bottom.reverse()
        # x coordinations for phrasal tokens
        for phr_node in nodes_from_bottom:
            if not phr_node.label.startswith('<'): continue
            first = -1; last = -1
            for node in nodes:
                if node.dep_id == phr_node.id: # take into account
                    last = node.x
                    if first < 0: first = node.x
            if first > 0: phr_node.x = (last + first) / 2.0
            for node in nodes: # check if not violating another token
                if phr_node.y == node.y and not phr_node == node:
                    dist = node.x - phr_node.x
                    shift = self.sw(node.label) / 2.0 + self.sw(phr_node.label) / 2.0 - abs(dist)
                    if shift > 0:
                        if dist > 0: phr_node.x -= shift
                        else: phr_node.x += shift

        self.nodes = nodes
        self.height = max([node.y for node in nodes]) + self.from_top - self.sh
        self.width = max([node.x + self.sw(node.label) for node in nodes]) + self.font_size

def parseTree(lines, hybrid_format):
    if not hybrid_format:
        newlines = []
        for line in lines:
            newlines.append(line.replace('\t-1\t', '\t10000\t'))
        newlines.append('\t'.join(['10000', '[root]', '-1', 'd']))
        return newlines
    else:
        return lines

def convertBrackets(line):
    import re

    lastId = 0
    treeStack = [-1]
    result = []
    # replace PRETERM\nterm to PRETERM {term }
    line = re.sub(r"\\n([^ \t\n}]+)",r" {[\1] }",line)
    def printNtermLine(nterm, id, parent):
        if nterm[0] == "[" and nterm[-1] == "]":
            nterm = nterm[1:-1]
        else:
            nterm = "<%s>" % nterm
        result.append("%d\t%s\t%d\tp\n" % (id, nterm, parent))
        id += 1
        return id

    pos = line.find("{") + 1
    obracket = line.find("{", pos)
    cbracket = line.find("}", pos)
    
    while cbracket != -1:
        if obracket < cbracket and obracket != -1:
            nterm = line[pos:obracket].strip()
            pos = obracket + 1
            obracket = line.find("{", pos)
            if len(nterm) > 0:
                peek = treeStack[-1]
                treeStack.append(lastId)
                lastId = printNtermLine(nterm, lastId, peek)
        else:
            nterm = line[pos:cbracket].strip()
            pos = cbracket + 1
            cbracket = line.find("}", pos)
            if len(nterm) > 0:
                lastId = printNtermLine(nterm, lastId, treeStack[-1])
            else:
                parent = treeStack.pop()

    return result

def exportTrees(trees, fileName):
    tv = TreeView(trees)
    tree_count = len(trees)
    fileName, imageType = os.path.splitext(fileName)
    if tree_count == 1:
        pat = fileName
    else:
        pat = "%s%%0%dd" % (fileName, len(str(tree_count - 1)))
    pngScaleFactor = 3

    for i in range(tree_count):
        if tree_count == 1: ipat = pat
        else: ipat = pat % i
        tv.setTree(i)
        if imageType == ".svg":
            surface = cairo.SVGSurface(ipat + '.svg', tv.width, tv.height)
        elif imageType in [".ps",".eps"]:
            surface = cairo.PSSurface(ipat + imageType, tv.width, tv.height)
            if imageType == ".eps":
                surface.set_eps(True)
        elif imageType == ".pdf":
            surface = cairo.PDFSurface(ipat + '.pdf', tv.width, tv.height)
        elif imageType == ".png":
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(pngScaleFactor * tv.width), int(pngScaleFactor * tv.height))
        else:
            print "%s not supported, use one of [svg,ps,epf,pdf,png]"
            return
        tv.cr = cairo.Context(surface)
        if imageType == ".png":
            tv.cr.transform (cairo.Matrix(pngScaleFactor, 0, 0, pngScaleFactor, 0, 0))
        tv.paint()
        if imageType == ".png":
            surface.write_to_png(ipat + '.png')
        surface.finish()
    return tv.width, tv.height

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print "Usage: %s <outfile>.<png|svg|pdf|eps|ps>" % sys.argv[0]
        sys.exit(1)
    trees = []
    treeStart = 0
    hybrid_format = False
    lines = sys.stdin.readlines()

    if ':clear' in lines[0]: # synt output (labeled brackets)
        linesOut = []
        for line in lines[2:]: # skip :clear, :words
            linesOut.extend(convertBrackets(line))
            linesOut.append("")
        lines = linesOut

    for lineNumber, line in enumerate(lines):
        lineSplit = line.split()
        if '<top>' in lineSplit or '<TOP>' in lineSplit or '<start>' in lineSplit or '<##start##>' in lineSplit or '<sentence>' in lineSplit:
            hybrid_format = True
        if line.isspace() or len(line) == 0 or len(lines) == lineNumber + 1:
            if len(lines) == lineNumber + 1:
                lineNumber += 1
            trees.append(parseTree(lines[treeStart:lineNumber], hybrid_format))
            treeStart = lineNumber + 1
            hybrid_format = False

    exportTrees(trees, sys.argv[1])
