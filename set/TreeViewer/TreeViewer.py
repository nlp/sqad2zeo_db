#!/usr/bin/python

# Copyright 2009 Milos Jakubicek, Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


import signal
signal.signal(signal.SIGINT, signal.SIG_DFL) # Quit on Ctrl+C

import sys, locale
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtSvg import *
import re


class Node(QPoint):# represents a tree node in the graphical view

    def __init__(self, id, label, dep_id, dep_type, from_left, from_top,
                 deplabel):
        QPoint.__init__(self, from_left, from_top)
        self.id = int(id)
        self.label = unicode(label, locale.getpreferredencoding())
        self.dep_id = int(dep_id)
        self.dep_type = dep_type
        self.deplabel = deplabel

class TreeViewerForm(QMainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        from TreeViewerUI import Ui_MainWindow
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.defaultFont = self.font()

    def hSliderSub(self):
        self.ui.hSlider.triggerAction(QSlider.SliderSingleStepSub)

    def hSliderAdd(self):
        self.ui.hSlider.triggerAction(QSlider.SliderSingleStepAdd)

    def vSliderSub(self):
        self.ui.vSlider.triggerAction(QSlider.SliderSingleStepSub)

    def vSliderAdd(self):
        self.ui.vSlider.triggerAction(QSlider.SliderSingleStepAdd)

    def scaleSliderSub(self):
        self.ui.scaleSlider.triggerAction(QSlider.SliderSingleStepSub)

    def scaleSliderAdd(self):
        self.ui.scaleSlider.triggerAction(QSlider.SliderSingleStepAdd)

    def firstTree(self):
        self.setTree(1)

    def lastTree(self):
        self.setTree(len(self.ui.treeview.trees))

    def nextTree(self):
        if self.ui.treeview.treeNumber != len(self.ui.treeview.trees):
            self.setTree(self.ui.treeview.treeNumber + 1)

    def previousTree(self):
        if self.ui.treeview.treeNumber != 1:
            self.setTree(self.ui.treeview.treeNumber - 1)

    def setTree(self, treeNumber):
        self.ui.treeview.treeNumber = treeNumber
        self.ui.treeNumber.setValue(treeNumber)
        self.ui.treeview.compute_tree()
        if treeNumber == 1:
            self.ui.previousButton.setEnabled(False)
            self.ui.firstButton.setEnabled(False)
        else:
            self.ui.previousButton.setEnabled(True)
            self.ui.firstButton.setEnabled(True)
        if treeNumber == len(self.ui.treeview.trees):
            self.ui.nextButton.setEnabled(False)
            self.ui.lastButton.setEnabled(False)
        else:
            self.ui.nextButton.setEnabled(True)
            self.ui.lastButton.setEnabled(True)
        self.ui.treeview.update()

    def restoreDefaults(self):
        self.ui.treeview.y_space_quotient = 3
        self.ui.treeview.x_space_quotient = 5
        self.ui.treeview.scale = 1
        self.ui.treeview.changeFont(self.defaultFont)
        self.ui.hSlider.setValue(5)
        self.ui.vSlider.setValue(3)
        self.ui.scaleSlider.setValue(100)
        self.ui.treeview.update()

    def exportImage(self):
        if self.sender().objectName() == "printButton":
            printer = QPrinter()
            printDialog = QPrintDialog(printer, self.parent())
            if printDialog.exec_() == QDialog.Accepted:
                self.ui.treeview.printer = printer
        else:
            self.ui.treeview.imageFile = QFileDialog.getSaveFileName(self)
            self.ui.treeview.imageType = str(self.ui.imageTypes.currentText())
        self.ui.treeview.sentence = self.ui.sentence.text()
        self.ui.treeview.update() # calls paintEvent(), only there Qt can paint


    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Control:
            self.ui.treeview.ctrlPressed = True

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Control:
            self.ui.treeview.ctrlPressed = False

class TreeView(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.treeNumber = None
        self.nodes = None
        self.y_space_quotient = 6
        self.x_space_quotient = 5
        self.updateYSpace()
        self.updateXSpace()
        self.updateFromTop()
        self.updateFromLeft()
        self.trees = None
        self.scale = 1
        self.width = 0
        self.height = 0
        self.ctrlPressed = False
        self.painter = QPainter()
        self.printer = None
        self.imageFile = None
        self.imageType = None
        self.sentence = None

    def updateYSpace(self):
        self.y_space = self.fontMetrics().height() * self.y_space_quotient * 0.5

    def updateXSpace(self):
        self.x_space = self.fontMetrics().averageCharWidth() * self.x_space_quotient

    def updateFromTop(self):
        self.from_top = self.fontMetrics().height() * 1.5

    def updateFromLeft(self):
        self.from_left = self.fontMetrics().averageCharWidth() * 5

    def changeXSpaceQuotient(self, quotient):
        self.x_space_quotient = quotient
        self.updateXSpace()
        self.compute_tree()
        self.update()

    def changeYSpaceQuotient(self, quotient):
        self.y_space_quotient = quotient
        self.updateYSpace()
        self.compute_tree()
        self.update()

    def chooseFont(self):
        font, ok = QFontDialog.getFont(self.font(), self)
        if ok:
            self.changeFont(font)

    def changeFont(self, font):
        self.setFont(font)
        self.updateYSpace()
        self.updateXSpace()
        self.updateFromTop()
        self.updateFromLeft()
        self.compute_tree()

    def changeScale(self, scale):
        if scale < 1:
            scale = 1
        self.scale = scale / 100.0
        self.update()

    def wheelEvent(self, event):
        if self.ctrlPressed:
            self.changeScale(self.scale * 100 + event.delta() / 10.0)
        else:
            event.ignore()

    def paintEvent(self, event):
        if self.printer:
            self.printImage()
            self.printer = None
        elif self.imageFile and len(self.imageFile) > 0:
            if self.imageType == "SVG":
                self.paintSVG(self.imageFile, self.sentence)
            else:
                self.paintImage(self.imageFile, self.imageType)
            self.imageFile = self.imageType = None
        else:
            self.painter.begin(self)
            self.paint(self.painter)
        self.setMinimumSize(self.width * self.scale, self.height * self.scale)

    def paintSVG(self, fileName, sentence):
        generator = QSvgGenerator()
        generator.setFileName(fileName)
        generator.setSize(QSize(self.width * self.scale, self.height * self.scale + self.fontMetrics().height()))
#        generator.setTitle("Syntactic tree")
#        generator.setDescription("Syntactic tree for the input sentence '" + sentence + "', generated by Tree Viewer.");
        self.painter.begin(generator)
        self.painter.setFont(self.font())
        self.paint(self.painter)

    def paintImage(self, fileName, imageType):
        image = QImage(self.width * self.scale, self.height * self.scale + self.fontMetrics().height(), QImage.Format_ARGB32_Premultiplied)
        # if the format does not support transparency, fill background with white
        if imageType.lower() != "png":
            image.fill(QColor(Qt.white).rgb())
        self.painter.begin(image)
        self.painter.setFont(self.font())
        self.paint(self.painter)
        image.save(fileName, imageType)

    def printImage(self):
        self.printer.setFullPage(True)
        self.printer.setPaperSize(QSizeF(self.width * self.scale, self.height * self.scale + self.fontMetrics().height()), QPrinter.DevicePixel)
        self.painter.begin(self.printer)
        self.painter.setFont(self.font())
        self.paint(self.painter)

    def paint(self, painter):
        penWidth = painter.font().pointSize() * 0.1
        if painter.font().bold():
            penWidth *= 1.5
        painter.scale(self.scale, self.scale)
        sentence = []
        maxX = 0
        maxY = 0
        bluePen = QPen(Qt.blue, penWidth, Qt.SolidLine)
        blackPen = QPen(Qt.black, penWidth, Qt.SolidLine)
        greyPen = QPen(Qt.gray, penWidth, Qt.SolidLine)

        for node in self.nodes:
            if node.label.startswith('<'):
                painter.setPen(bluePen)
            else:
                painter.setPen(blackPen)
                sentence.append(node.label)
            painter.drawText(node.x() - painter.fontMetrics().width(node.label) / 2.0, node.y(), node.label)
            if node.deplabel:
                painter.setPen(greyPen)
                painter.drawText(node.x() + painter.fontMetrics().width(node.label) / 2.0, node.y() + 5, node.deplabel)
            if node.dep_id > -1:
                if node.dep_id < 10000:
                    depNode = self.nodes[node.dep_id]
                else:
                    depNode = self.nodes[-1] # nasty hack
                if node.dep_type == 'p':
                    painter.setPen(bluePen)
                else:
                    painter.setPen(blackPen)
                painter.drawLine(depNode.x(), depNode.y() + painter.fontMetrics().height() * 0.25, node.x(), node.y() - painter.fontMetrics().height() * 0.85)
            if maxX < node.x() + painter.fontMetrics().width(node.label) / 2.0:
                maxX = node.x() + painter.fontMetrics().width(node.label) / 2.0
            if maxY < node.y():
                maxY = node.y()

        self.width = maxX
        self.height = maxY
        self.emit(SIGNAL("treeChanged(QString)"), " ".join(filter(lambda word: word != "[root]", sentence)))
        painter.end()

    def compute_tree(self):
        # TODO divide the code
        nodes = []
        for line in self.trees[self.treeNumber - 1]:
            if len(line) == 0 or line.isspace():
                continue
            try:
                ll = line.strip().split('\t') + ['']
                if re.match('-?\d+', ll[2]):
                    id, label, dep_id, dep_type, deplabel = (ll[0], ll[1], ll[2],
                                                             ll[3], ll[4])
                elif re.match('-?\d+', ll[4]):
                    id, label, dep_id, dep_type, deplabel = (ll[0], ll[1], ll[4],
                                                             ll[5], ll[6])
                else:
                    raise ValueError('Invalid input')

            except:
                print >> sys.stderr, "Invalid input: %s" % line
                sys.exit(1)
            nodes.append(Node(id, label, dep_id, dep_type, self.from_left,
                              self.from_top, deplabel))
        # x coordinations
        x = self.from_left
        for index, node in enumerate(nodes):
            node.setX(x)
            if not node.label.startswith('<'):
                shift = self.fontMetrics().width(node.label) / 2.0
                if index + 1 < len(nodes):
                    shift += self.fontMetrics().width(nodes[index+1].label) / 2.0
                x += self.x_space + shift
        # y coordinations
        nodes_from_bottom = []
        y = self.from_top
        next_level = [-1]
        next_next_level = []
        while next_level:
            for id in next_level:
                for node in nodes:
                    if node.dep_id == id:
                        nodes_from_bottom.append(node)
                        node.setY(y)
                        next_next_level.append(node.id)
            next_level = next_next_level
            next_next_level = []
            y += self.y_space
        nodes_from_bottom.reverse()
        # x coordinations for phrasal tokens
        for phr_node in nodes_from_bottom:
            if not phr_node.label.startswith('<'): continue
            first = -1; last = -1
            for node in nodes:
                if node.dep_id == phr_node.id: # take into account
                    last = node.x()
                    if first < 0: first = node.x()
            if first > 0: phr_node.setX((last + first) / 2.0)
            for node in nodes: # check if not violating another token
                if phr_node.y() == node.y() and not phr_node == node:
                    dist = node.x() - phr_node.x()
                    shift = self.fontMetrics().width(node.label) / 2.0 + self.fontMetrics().width(phr_node.label) / 2.0 - abs(dist)
                    if shift > 0:
                        if dist > 0: phr_node.setX(phr_node.x() - shift)
                        else: phr_node.setX(phr_node.x() + shift)
        self.nodes = nodes

def parseTree(lines, hybrid_format):
    if not hybrid_format:
        newlines = []
        for line in lines:
            newlines.append(line.replace('\t-1\t', '\t10000\t'))
        newlines.append('\t'.join(['10000', '[root]', '-1', 'd']))
        return newlines
    else:
        return lines

def initGUI(trees):

    application = QApplication(sys.argv)
    application.setApplicationName("Tree Viewer")
#    app.setApplicationVersion("1.1") only in new PyQt
    application.setOrganizationName("Natural Language Processing Centre, Faculty of Informatics, Masaryk University, Brno, Czech Republic")
    application.setOrganizationDomain("http://nlp.fi.muni.cz/trac/set")

    TVForm = TreeViewerForm() # Main Window
#    TVForm.setWindowTitle(application.applicationName() + " " + application.applicationVersion())

    # QSlider shortcuts (cannot be handled in Qt Designer)
    hSliderLeftShortcut = QShortcut(QKeySequence("X"), TVForm)
    hSliderRightShortcut = QShortcut(QKeySequence("C"), TVForm)
    vSliderLeftShortcut = QShortcut(QKeySequence("V"), TVForm)
    vSliderRightShortcut = QShortcut(QKeySequence("B"), TVForm)
    scaleSliderLeftShortcut = QShortcut(QKeySequence("N"), TVForm)
    scaleSliderRightShortcut = QShortcut(QKeySequence("M"), TVForm)
    QObject.connect(hSliderLeftShortcut, SIGNAL("activated()"), TVForm.hSliderSub)
    QObject.connect(hSliderRightShortcut, SIGNAL("activated()"), TVForm.hSliderAdd)
    QObject.connect(vSliderLeftShortcut, SIGNAL("activated()"), TVForm.vSliderSub)
    QObject.connect(vSliderRightShortcut, SIGNAL("activated()"), TVForm.vSliderAdd)
    QObject.connect(scaleSliderLeftShortcut, SIGNAL("activated()"), TVForm.scaleSliderSub)
    QObject.connect(scaleSliderRightShortcut, SIGNAL("activated()"), TVForm.scaleSliderAdd)
    # Status bar
    TVForm.ui.statusbar.showMessage("Use Left/Right arrows or Page Up/Down for switching trees, 'F' for changing the font, 'P' for printing, 'E' for exporting, F5 for restoring default view, 'X'/'C', 'V'/'B', 'N'/'M' for changing the horizontal and vertical tree size and zooming.")
    # Export formats
    TVForm.ui.imageTypes.addItem("SVG")
    TVForm.ui.imageTypes.addItems(map(str, QImageWriter.supportedImageFormats()))
    TVForm.ui.imageTypes.setCurrentIndex(0)

    TVForm.ui.treeview.trees = trees
    TVForm.ui.treeNumber.setMaximum(len(trees))
    TVForm.ui.treeNumber.setMinimum(1)
    TVForm.ui.treeCount.setText("of " + str(len(trees)))

    TVForm.setTree(1)
    TVForm.show()

    application.exec_()

def convertBrackets(line):
    import re

    lastId = 0
    treeStack = [-1]
    result = []
    # replace PRETERM\nterm to PRETERM {term }
    line = re.sub(r"\\n([^ \t\n}]+)",r" {[\1] }",line)
    def printNtermLine(nterm, id, parent):
        if nterm[0] == "[" and nterm[-1] == "]":
            nterm = nterm[1:-1]
        else:
            nterm = "<%s>" % nterm
        result.append("%d\t%s\t%d\tp\n" % (id, nterm, parent))
        id += 1
        return id

    pos = line.find("{") + 1
    obracket = line.find("{", pos)
    cbracket = line.find("}", pos)

    while cbracket != -1:
        if obracket < cbracket and obracket != -1:
            nterm = line[pos:obracket].strip()
            pos = obracket + 1
            obracket = line.find("{", pos)
            if len(nterm) > 0:
                peek = treeStack[-1]
                treeStack.append(lastId)
                lastId = printNtermLine(nterm, lastId, peek)
        else:
            nterm = line[pos:cbracket].strip()
            pos = cbracket + 1
            cbracket = line.find("}", pos)
            if len(nterm) > 0:
                lastId = printNtermLine(nterm, lastId, treeStack[-1])
            else:
                parent = treeStack.pop()

    return result


if __name__ == "__main__":

    trees = []
    treeStart = 0
    hybrid_format = False
    lines = sys.stdin.readlines()

    if ':clear' in lines[0]: # synt output (labeled brackets)
        linesOut = []
        for line in lines[2:]: # skip :clear, :words
            linesOut.extend(convertBrackets(line))
            linesOut.append("")
        lines = linesOut

    for lineNumber, line in enumerate(lines):
        lineSplit = [ll.lower() for ll in line.split()]
        if '<top>' in lineSplit or '<TOP>' in lineSplit or '<start>' in lineSplit or '<##start##>' in lineSplit or '<sentence>' in lineSplit:
            hybrid_format = True
        if line.isspace() or len(line) == 0 or len(lines) == lineNumber + 1:
            if len(lines) == lineNumber + 1:
                lineNumber += 1
            trees.append(parseTree(lines[treeStart:lineNumber], hybrid_format))
            treeStart = lineNumber + 1
            hybrid_format = False

    initGUI(trees)

   # if outPattern:
   #     app = QCoreApplication(sys.argv) # non-GUI
   #     tv = TreeView()
   # else:

   # app.processEvents()
   # if outPattern:
   #     import os.path
   #     tree_count = len(trees)
   #     print tree_count
   #     fileName, imageType = os.path.splitext(outPattern)
   #     pat = "%s%%0%dd%s" % (fileName, tree_count, imageType)
   #     for i in range(tree_count):
   #         print "Exporting %d" % i
   #         #form.setTree(i)
   #         app.processEvents()
   #         #form.exportImage((pat % i, imageType[1:]))
   #         #form.repaint()
   #         app.processEvents()
   # else:
