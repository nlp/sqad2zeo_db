#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# reading segments from text files, class Segment
# and tree output functions

from settoken import Token, SurfaceToken, PhrToken, LinkToken
from matcher import match_token
from utils import log
import re, sys, copy

brief_re = re.compile('\s+<[lc]>')
space_re = re.compile('\s+')
g_re = re.compile('^<g[/> ]')
compl_lemma = re.compile('([^-`_]+)[-`_]')
phr_cats = ['noun', 'adj', 'pron', 'num', 'adv', 'prep', 'abbr']
phr_strip = ['part', 'conj', 'punct']


def match_any_class(token, classes, wc):
    for cl in classes:
        if match_token(token, wc[cl]): return True
    return False


def top_token(token, tokens=[]):
    t = token
    while t.dependency:
        if tokens and t.dependency not in tokens: break
        t = t.dependency
    return t


def find_surface_dep_id(tok):
    token = tok
    dep = token.dependency
    if not dep or dep.str_word() == '<sentence>':
        return -1
    if not dep:
        if not token.is_head():
            raise RuntimeError('Non-head token without dependency: %s'
                                                            % token.str_word())
        if not token.segment.pointer: return -1
        dep = token.segment.pointer.dependency; token = token.segment.pointer
    while not isinstance(dep, SurfaceToken):
        if dep.str_word() == '<sentence>':
            return -1
        if isinstance(dep, PhrToken):
            if token == dep.head: # up
                if dep.is_head():
                    if not dep.segment.pointer: return -1
                    token = dep.segment.pointer; dep = token.dependency
                else: token = dep; dep = dep.dependency
            else: token = dep; dep = dep.head # down
        elif isinstance(dep, LinkToken): # always down
            dep = dep.target.head
    if dep == tok: return -1
    return dep.id

def add_nodes(sel_nodes, phrwords_dict):
    if not phrwords_dict: return sel_nodes
    result = []
    for i, snode in enumerate(sel_nodes):
        result.append(snode)
        found = False
        for lemma in snode.token.lemma:
            for child in snode.children:
                for phrword_tuple in phrwords_dict.get(lemma, []):
                    if not phrword_tuple[0] == child.word: continue
                    intersection = [x for x in snode.children
                                          if x.word in phrword_tuple]
                    if len(intersection) == len(phrword_tuple):
                        found = True; break
                if found: result.extend(intersection); break
            if found: break
    return result

def phr2surfaceid(token):
    tok = token
    while isinstance(tok, PhrToken): tok = tok.head
    return tok.surface_index + 1


def get_marx_phrases_for_node(node, found_phrases, clause=None, short=True,
                              categories=phr_cats, phr_strip=phr_strip, wc={}):
        # clause -- print phrases only within scope of a clause
    def add_curr_phrase(result_phrases, curr_phrase):
        if not curr_phrase: return
        head_token = top_token(curr_phrase[0].token,
                                                [t.token for t in curr_phrase])
        result_phrases.append({'word': head_token.str_word(),
                               'surface_head': phr2surfaceid(head_token),
                               'tag':  head_token.str_tag(),
                               'phr': curr_phrase,
                               'ids': set([n.id + 1 for n in curr_phrase
                                               if not n.word.startswith('<')]),
                               'deps': set([n.dep_id + 1 for n in curr_phrase]),
                               })
    token = node.token
    wanted = False
    for cat in categories:
        if match_token(token, wc[cat], match_phr=True): wanted = True; break
    if token.str_word() == '<vp>': wanted = True
    marx_head = token.str_word() not in ['<coord>', '<sentence>'] and wanted \
                and not node in [nn for p in found_phrases for nn in p['phr']]
    half_head = token.str_word() == '<coord>' and wanted
    if not marx_head:
        for child in node.children:
            get_marx_phrases_for_node(child, found_phrases, clause, short,
                                      categories, phr_strip, wc)
    if token.str_word() in ['<top>', '<ends>', '<sentence>']:
        return found_phrases
    if not marx_head and not half_head: return found_phrases
    if clause and node not in clause: return found_phrases
    q = [(node.position, node)]; i = 0
    while i < len(q):
        q.extend([(ch.position, ch) for ch in q[i][1].children
                                        if not clause or ch in clause])
        i += 1
    q.sort(key=lambda x: x[0])
    if short: # split phrases on prepositions and particles
        curr_phrase = []
        result_phrases = []
        can_split = False
        for position, item in q:
            if ( item.parent and item.parent.word != '<coord>' and
                          ( match_token(item.token, wc['prep'])
                            or match_token(item.token, wc['part'])
                            or match_token(item.token, wc['punct']) )):
                if can_split and curr_phrase \
                        and not match_token(curr_phrase[-1].token, wc['prep'],
                                            match_phr=True):
                    add_curr_phrase(result_phrases, curr_phrase)
                    curr_phrase = []
            elif not match_token(item.token, wc['conj']):
                can_split = True
            if curr_phrase or (not match_any_class(item.token, phr_strip, wc)):
                curr_phrase.append(item)
        add_curr_phrase(result_phrases, curr_phrase)
    else:
        result_phrases = [{ 'word': token.str_word(),
                            'surface_head': phr2surfaceid(token),
                            'tag': token.str_tag(),
                            'phr': [n[1] for n in q],
                            'ids': set([n[1].id + 1 for n in q
                                            if not n[1].word.startswith('<')]),
                            'deps': set([n[1].dep_id + 1 for n in q])
                           }]
    found_phrases.extend(result_phrases)
    return found_phrases


def get_common_tag(tokens):
    result = tokens[0].tag[0]
    for token in tokens[1:]:
        tag = token.tag[0]
        if tag.startswith(('k1', 'k3', 'k4')):
            result = result.replace('nS', 'nP')
            if not 'g' in tag:
                continue
            g_index = tag.index('g')
            g = tag[g_index:g_index+2]
            if not 'g' in result:
                result += g
            g_index = result.index('g')
            result_g = result[g_index:g_index+2]
            if g == 'gM':
                result = result.replace(result_g, 'gM')
            elif g == 'gI' and result_g != 'gM':
                result = result.replace(result_g, 'gI')
            elif g == 'gF' and result_g == 'gN':
                result = result.replace(result_g, 'gF')
    return [result]


# classes for more transparent tree drawing and modifying
class TreeNode():

    def __init__(self, node_dict, position=-1):
        try:
            self.id = node_dict['id']
            self.word = node_dict['word']
            self.dep_id = node_dict['dep_id']
            self.dep_type = node_dict['dep_type']
            self.token = node_dict['token']
            self.deplabel = node_dict['deplabel']
        except TypeError:
            ll = node_dict.split() + ['']
            self.id, self.word, self.dep_id, self.dep_type, self.deplabel = \
                                            (ll[0], ll[1], ll[2], ll[3], ll[4])
            self.id = int(self.id)
            self.dep_id = int(self.dep_id)
            self.token = None
        if position == -1: self.position = self.id
        else: self.position = position
        self.children = []
        self.parent = None


class Tree():

    def __init__(self, lines, phr_verbs={}):
        self.nodes = [TreeNode(line) for line in lines]
        self.phr_verbs = phr_verbs
        self.first_node = None
        self.link_nodes()
        self.fix_phrasal_positions()

    def flat_sentence(self, lt=False):
        result = ''
        if lt: result_l, result_t = '', ''
        for node in self.nodes:
            if isinstance(node.token, SurfaceToken):
                result += node.token.str_word() + ' '
                if lt:
                    result_t += node.token.str_tag() + ' '
                    result_l += node.token.str_lemma() + ' '
        return result, result_l, result_t

    def get_clauses(self, wc={}, from_dep=False):
        if from_dep:
            result = []
            for node in self.nodes:
                t = node.token
                if ((node.dep_id == -1
                                 and not match_token(t, wc['part'])
                                 and not match_token(t, wc['cconj']))
#                                 and not match_token(t, wc['punct']))
                     or match_token(t, wc['sconj'])
                     or (match_token(t, wc['verb'])
                         and (match_token(node.parent.token, wc['punct'])
                              or match_token(node.parent.token, wc['cconj'])))):
                    clause = self.get_clause_for_node(node, wc, True)
                    result.append((clause, node))
                if match_token(t, wc['relpron']) and node.parent:
                    if match_token(node.parent.token, wc['verb']):
                        clause = self.get_clause_for_node(node.parent, wc, True)
                        result.append((clause, node.parent))
                    elif ( node.parent.parent
                           and match_token(node.parent.parent.token,
                                           wc['verb']) ):
                        clause = self.get_clause_for_node(node.parent.parent,
                                                          wc, True)
                        result.append((clause, node.parent.parent))
            return result
        # normal run
        return [ (self.get_clause_for_node(node, wc), node)
                 for node in self.nodes
                     if node.token.str_word() in ('<clause>', '<sentence>')
                         or (match_token(node.token, wc['verbfin'])
                             and node.token.dependency # verb in coord
                             and node.token.dependency.str_word() == '<coord>')
               ]

    def get_clause_for_node(self, start_node, wc={}, from_dep=False):
        if from_dep:
            result = []
            q = [start_node]; i = 0
            while i < len(q):
                curr_node = q[i]
                result.append((curr_node.position, curr_node))
                for node in curr_node.children:
                    t = node.token
                    if (not match_token(t, wc['sconj'])
                        # not a subordinate conjunction ...
                        and not (match_token(t, wc['verb']) and node.parent
                                 and (match_token(node.parent.token,
                                                  wc['punct'])
                                      or match_token(node.parent.token,
                                                     wc['cconj'])))
                        # ... nor a verb in coordination, ...
                        and not [n for n in node.children
                                     if match_token(n.token, wc['relpron'])]
                        # ... not a verb that carries relative pronoun ...
                        and not [n for n in
                                     [nn for nn in node.children
                                           if match_token(nn.token, wc['prep'])]
                                     if match_token(n.token, wc['relpron'])]):
                        # ... or a verb that carries a preposition that carries
                        # a relative pronoun
                        q.append(node)
                i += 1
            result.sort(key=lambda x: x[0])
            return [n[1] for n in result]
        # normal run
        result = []
        q = [start_node]; i = 0
        while i < len(q):
            curr_node = q[i]
            result.append((curr_node.position, curr_node))
            if i == 0 or curr_node.token.str_word() not in ('<clause>',
                                '<relclause>', '<interjection>', '<sentence>'):
                if not (curr_node.word == '<coord>'
                        and match_token(curr_node.token, wc['verb'])):
                    q.extend(curr_node.children)
            i += 1
        result.sort(key=lambda x: x[0])
        result = [n[1] for n in result]
        return result

    def link_nodes(self):
        for node in self.nodes:
            if node.dep_id == -1: self.first_node = node
            parent_id = node.dep_id
            for node2 in self.nodes:
                if node2.id == parent_id:
                    node.parent = node2
                    node2.children.append(node)

    def destroy_non_projectivity(self):
        for node in self.nodes:
            if not isinstance(node.token, SurfaceToken): continue
            for node2 in self.nodes:
                if not node2.parent: continue
                positions = (node2.position, node2.parent.position)
                p1, p2 = min(positions), max(positions)
                while p1 < node.position and p2 > node.position \
                             and isinstance(node2.parent.token, SurfaceToken) \
                             and not self.ancestor(node, node2.parent):
                    ii = node2.parent.children.index(node2)
                    del node2.parent.children[ii]
                    node2.parent = node2.parent.parent
                    node2.parent.children.append(node2)
                    positions = (node2.position, node2.parent.position)
                    p1, p2 = min(positions), max(positions)

    def ancestor(self, start, target):
        node = start
        while node:
            if target == node: return True
            node = node.parent
        return False

    def fix_phrasal_positions(self):
        if not self.first_node:
            log('warning: no first_node in the tree!')
            return
        q = [self.first_node]
        i = 0
        while i < len(q): q.extend(q[i].children); i += 1
        q.reverse()
        for node in q:
            if node.children and not isinstance(node.token, SurfaceToken):
                p1, p2 = node.children[0].position, node.children[-1].position
                node.position = (p1 + p2) / 2.0


    def add_preterminals(self, into_leaf=False):
        length = len(self.nodes)
        for node in self.nodes:
            if not node.children:
                if into_leaf:
                    node.word = node.token.get_preterm().strip('<>') \
                                + ':'+node.word
                else:
                    new_node = TreeNode('%d\t%s\t-2\tp'
                                          % (length, node.token.get_preterm()))
                    # warning: do not set node.position here
                    new_node.token = node.token
                    new_node.parent = node.parent
                    i = node.parent.children.index(node)
                    node.parent.children[i] = new_node
                    new_node.children = [node]
                    node.parent = new_node
                    self.nodes.append(new_node)
                    length += 1


    def canonize(self):
        nterms = set(['<ABBR>', '<ADJP>', '<ADVP>', '<VP>', '<PP>',
                      '<NP>', '<SENTENCE>', '<UNKNOWN>', '<CLAUSE>'])
        preterms = set(['<PART>', '<ADJ>', '<ADV>', '<V>', '<C>',
                        '<PREP>', '<N>', '<PRON>', '<ABBR>', '<NUM>'])
        wanted_nterms = nterms.union(preterms)
        for node in self.nodes:
            if node.word.upper() in wanted_nterms:
                node.word = node.word.upper()
        self.fix_phrasal_positions()


    def get_laa_format(self, node=None):
        if not node: node = self.first_node
        if node.children:
            inner_part = ' '.join([    self.get_laa_format(child)
                                                for child in node.children ])
            return '[%s %s ]' % (node.word, inner_part)
        else:
            return node.word


    def sort_children(self):
        for node in self.nodes:
            tmp = []
            for child in node.children: tmp.append((child.position, child))
            tmp.sort()
            node.children = [n for p, n in tmp]


    def find_marx_phrases_vert(self, short=True, justwords=False, categories=phr_cats,
                         phr_strip=phr_strip, wc={}, from_dep=False):
        def is_I89(token):
            return ( match_token(token, wc['conj'])
                     or match_token(token, wc['part'])
                     or match_token(token, wc['punct'])
                     or token.str_word().lower() in ('kdyby', 'aby'))
        clauses = self.get_clauses(wc, from_dep)
        clauses.sort(key=lambda x: min([tok.id for tok in x[0]]))
        output_phrases = []
        for clause, head_node in clauses: # clause = list of (some) nodes
            toprint = []
            for i, node in enumerate(clause): # strip unwanted nodes
                if node.token.str_word().startswith('<'): continue
                toprint.append(node)
            delete = True
            while delete and toprint:
                if is_I89(toprint[-1].token): del toprint[-1]
                else: delete = False
            if not toprint: continue

            verb_nodes = [node for node in clause
                                   if (match_token(node.token, wc['vpart'])
                                       or match_token(node.token, wc['verb']))
                                       and not node.word.startswith('<')]
            if short: # divide vp according to sense verbs
                vps_nodes = []; curr_vp = []
                for n in verb_nodes:
                    curr_vp.append(n)
                    if (match_token(n.token, wc['verb'])
                                and not match_token(n.token, wc['modalverb'])):
                        vps_nodes.append(curr_vp)
                        curr_vp = []
                if curr_vp: vps_nodes.append(curr_vp)
            else:
                vps_nodes = [verb_nodes]
            vps = []
            for vp in vps_nodes:
                if not vp: continue
                rvp = add_nodes(vp, self.phr_verbs)
                shead = phr2surfaceid(top_token(vp[0].token, vp))
                vps.append( {'word': '<vp>', 'deps': set(),
                             'tag': ','.join([t for n in vp
                                                 for t in n.token.tag]),
                             'phr': rvp, 'ids': set([n.id + 1 for n in vp]),
                             'surface_head': shead })
            phrases = get_marx_phrases_for_node(head_node, vps, clause, short,
                                                categories, phr_strip, wc)
            for phrase in phrases:
                phr_vert = {}
                phr_vert["phr_tag"] = phrase['tag']
                phr_vert["head"] = phrase['surface_head']
                phr_vert["phr_words"] = []
                for n in phrase['phr']:
                    token = {}
                    if isinstance(n.token, SurfaceToken):
                        token["word"] = n.token.str_word()
                        token["lemma"] = sorted(n.token.lemma)[0]
                        token["tag"] = n.token.tag[0]
                    if token:
                        phr_vert["phr_words"].append(token)
                output_phrases.append(phr_vert)

        # output in form of
        # {'phr': [{'lemma': 'najit', 'tag': 'k5eAaPmIp1nP;cap', 'word': 'Najdeme'}],
        #  'head': 2,
        #  'phr_tag': 'k5eAaPmIp1nP;cap'}
        return output_phrases


    def get_marx_phrases(self, short=True, justwords=False, categories=phr_cats,
                         phr_strip=phr_strip, wc={}, from_dep=False):
        def is_I89(token):
            return ( match_token(token, wc['conj'])
                     or match_token(token, wc['part'])
                     or match_token(token, wc['punct'])
                     or token.str_word().lower() in ('kdyby', 'aby'))
        clauses = self.get_clauses(wc, from_dep)
        clauses.sort(key=lambda x: min([tok.id for tok in x[0]]))
        lines = []
        lines.append('<s>%s\n<slemma>%s\n<stag>%s' % self.flat_sentence(True))
        for clause, head_node in clauses: # clause = list of (some) nodes
            toprint = []
            for i, node in enumerate(clause): # strip unwanted nodes
                if node.token.str_word().startswith('<'): continue
#                if not toprint and is_I89(node.token):
#                    continue
                toprint.append(node)
            delete = True
            while delete and toprint:
                if is_I89(toprint[-1].token): del toprint[-1]
                else: delete = False
            if not toprint: continue
            str_clause = ' '.join([node.token.str_word() for node in toprint])
            int_clause = ' '.join([str(node.token.surface_index + 1)
                                                          for node in toprint])
            lines.append('<clause>%s' % str_clause)
            if not justwords: lines.append('<clausenum>%s' % int_clause)
            # clauseconj
            conjs = [node for node in clause
                              if match_token(node.token, wc['conj'])]
            tag = conjs and conjs[0].token.tag[0] or ''
            conjstr = ' '.join([node.token.str_word() for node in conjs])
            lines.append('<clauseconj> (%s): %s' % (tag, conjstr))
            # recognize verb phrase(s)
            verb_nodes = [node for node in clause
                                   if (match_token(node.token, wc['vpart'])
                                       or match_token(node.token, wc['verb']))
                                       and not node.word.startswith('<')]
            if short: # divide vp according to sense verbs
                vps_nodes = []; curr_vp = []
                for n in verb_nodes:
                    curr_vp.append(n)
                    if (match_token(n.token, wc['verb'])
                                and not match_token(n.token, wc['modalverb'])):
                        vps_nodes.append(curr_vp)
                        curr_vp = []
                if curr_vp: vps_nodes.append(curr_vp)
            else:
                vps_nodes = [verb_nodes]
            vps = []
            for vp in vps_nodes:
                if not vp: continue
                rvp = add_nodes(vp, self.phr_verbs)
                shead = phr2surfaceid(top_token(vp[0].token, vp))
                vps.append( {'word': '<vp>', 'deps': set(),
                             'tag': ','.join([t for n in vp
                                                 for t in n.token.tag]),
                             'phr': rvp, 'ids': set([n.id + 1 for n in vp]),
                             'surface_head': shead })
            phrases = get_marx_phrases_for_node(head_node, vps, clause, short,
                                                categories, phr_strip, wc)
            for phrase in phrases:
                phrwords = ' '.join([n.token.str_word()
                                     for n in phrase['phr']
                                         if isinstance(n.token, SurfaceToken)])
                if not phrase['word'].startswith('<'):
                    lines.append('<phr> (%s): %s' % (phrase['tag'], phrwords))
                else:
                    lines.append('%s (%s): %s' % (phrase['word'],
                                                  phrase['tag'], phrwords))
                if justwords: continue
                phrlemmas = ' '.join([sorted(n.token.lemma)[0]
                                      for n in phrase['phr']
                                          if isinstance(n.token, SurfaceToken)])
                phrnums = ' '.join([str(n.token.surface_index + 1)
                                    for n in phrase['phr']
                                        if isinstance(n.token, SurfaceToken)])
                if not phrase['word'].startswith('<'):
                    lines.append('<phrlemma> (%s): %s'
                                  % (phrase['tag'], phrlemmas))
                    lines.append('<phrnum> (%s): %s' % (phrase['tag'], phrnums))
                    lines.append('<phrhead> %s ' % phrase['surface_head'])
                else:
                    lines.append('%slemma> (%s): %s' % (phrase['word'][:-1],
                                                      phrase['tag'], phrlemmas))
                    lines.append('%snum> (%s): %s' % (phrase['word'][:-1],
                                                      phrase['tag'], phrnums))
                    lines.append('%shead> %s' % (phrase['word'][:-1],
                                                 phrase['surface_head']))
                for p2 in phrases:
                    if p2 == phrase or (len(p2['ids']) <= len(phrase['ids'])
                                    and phrase['ids'].intersection(p2['ids'])):
                        continue
                    if phrase['deps'].intersection(p2['ids']) \
                            or (phrase['ids'] and p2['word'] == '<coord>'
                                and phrase['ids'].issubset(p2['ids'])):
                        lines.append('%s ->>> %s' % (sorted(phrase['ids']),
                                                            sorted(p2['ids'])))
                        break
        lines.append('</s>')
        return lines


class Segment:

    def __init__(self, tokens, id='0', head=None, pos_tags=False,
                 desamb_hacks=False, phr_verbs={}):
        # tokens can be either a list of Token objects or a list of brief lines
        self.elements = [] # subordinate elements created during the analysis
        self.id = id
        self.head = head
        self.pointer = None # pointer token (in case there is one)
        self.pos_tags = pos_tags
        self.desamb_hacks = desamb_hacks
        self.phr_verbs = phr_verbs
        if not tokens: self.tokens = []
        self.tokens = self.read_tokens(tokens)
        for token in self.tokens:
            token.segment = self


    def read_tokens(self, tokens, split_on_spaces=True):
        # tokens either as list of Token object or as brief lines
        # brief lines = word, lemma, tag order
        result = []
        first = True
        i = 0
        for token in tokens:
            if isinstance(token, Token): return tokens # Token list
            if g_re.match(token) and result:
                result[-1].glue = True
                continue
            line = brief_re.sub('\t', token).strip()
            if split_on_spaces: line = space_re.sub('\t', line)
            if not line: continue
            line_list = line.split('\t') + ['', '', '', '']
            word, lemma, tag, sem = (line_list[0], line_list[1], line_list[2],
                                     line_list[3])
            m = compl_lemma.match(lemma)
            if m: lemma = m.group(1);
            result.append(SurfaceToken(self, word, lemma, tag, sem,
                                       pos_tags=self.pos_tags, first=first,
                                       surface_index=i,
                                       desamb_hacks=self.desamb_hacks))
            first = False
            i += 1
        return result


    def get_words(self):
        return [token.str_word() for token in self.tokens]


    def str_words(self):
        return ' '.join(self.get_words())


    def create_elem(self, start, end, linkname):
        # currently not used
        new_segment = Segment( self.tokens[start:end] )
        log('  Sub-segment created: ' + new_segment.str_words())
            # replace subsegment tokens by a link
        new_link = LinkToken(self, new_segment, label=linkname)
        new_segment.pointer = new_link
        self.tokens[start:end] = [new_link]


    def add_phrasal_token(self, children_src_list, phr_name, phr_src_head,
                          dep=None, all_tokens=[], under_phrase=None,
                          deplabels=[], make_invisible=False, i1=-1, i2=-1,
                          deplabeldep='', coord_tag=False):
        if not under_phrase: under_phrase = []
        children_list = []
        phr_head = top_token(children_src_list[0])
        for child in children_src_list:
            new_child = top_token(child)
            new_child.deplabel = ' | '.join(deplabels)
            children_list.append(new_child)
            if child == phr_src_head: phr_head = new_child
        si = 0.5 * (children_src_list[0].surface_index
                                          + children_src_list[0].surface_index)
        new_token = PhrToken(self, phr_name, children=children_list,
                             head=phr_head, dependency=dep, surface_index=si,
                             tag=get_common_tag(children_list))
        new_token.deplabel = deplabeldep
        if not coord_tag: new_token.tag = phr_head.tag
        if dep: dep.children.append(new_token)
        self.tokens.append(new_token)
        for ch in children_list:
            ch.dependency = new_token
            ch.phrasal = True
        # create info about the phrasal token coverage
        if not all_tokens: all_tokens = children_list
        if i1 < 0:
            i1 = min(children_list[0].surface_index,
                     all_tokens[0].surface_index+1)
        if i2 < 0:
            i2 = max(children_list[-1].surface_index,
                     all_tokens[-1].surface_index-1)
        for token in self.tokens:
            if i1 <= token.surface_index <= i2 and token != new_token:
                token.under_phrase.append(new_token)
                if make_invisible: token.invisible = True
            #logging
        if new_token.head:
            head_info = ' ::: head = ' + new_token.head.str_word()
        else:
            head_info = ' ::: head = None'
        if dep:
            dep_info = ' ::: dep = ' + dep.str_word()
        else:
            dep_info = ''
        log('  Phrase created: ' + phr_name + ' ::: ' +
               ' '.join(w for w in [tok.str_word() for tok in children_list]) +
               head_info + dep_info)
        new_token.under_phrase = under_phrase
#        self.tokens.append(new_token)
        return new_token


    def del_phrasal_token(self, todel):
        for i, token in enumerate(self.tokens):
            if token == todel: delindex = i
            if todel in token.under_phrase:
                del token.under_phrase[token.under_phrase.index(todel)]
            if token.dependency == todel: token.dependency = None
        del self.tokens[delindex]


    def add_dependency(self, child, parent, prob=100, deplabels=[]):
        if child.dependency: return
        child.dependency = parent
        child.dep_prob = prob
        child.deplabel = ' | '.join(deplabels)
        parent.children.append(child)


    def set_token_ids(self, start_id=0, dep=False):
        id = start_id
        for token in self.tokens:
            if isinstance(token, LinkToken):
                id = token.target.set_token_ids(id, dep)
            if not dep or isinstance(token, SurfaceToken):
                token.id = id
                id += 1
        return id


    def get_tree(self, for_print=True, first=True):
        "returns the tree lines as a list"
        if first: self.set_token_ids()
        result = []
        for token in self.tokens:
                # init values
            id = token.id; word = token.str_word()
            if token.is_head():
                if token.segment.pointer: dep_id = token.segment.pointer.id
                else: dep_id = -1 # the very top token
            else:
                if token.dependency: dep_id = token.dependency.id
                else: dep_id = -2 # error
            if token.phrasal or token.is_head(): dep_type = 'p'
            else: dep_type = 'd'
            if isinstance(token, LinkToken):
                result.extend(token.target.get_tree(for_print, first=False))
            if for_print:
                result.append('\t'.join([str(id), word, str(dep_id), dep_type,
                              token.deplabel]))
            else:
                lemma = ','.join(token.lemma)
                tag = ','.join(token.tag)
                result.append({'id': id, 'word': word, 'dep_id': dep_id,
                               'dep_type': dep_type, 'token': token,
                               'deplabel': token.deplabel, 'lemma': lemma,
                               'tag': tag})
        return result


    def prepare_phr_tree(self):
        tree = Tree(self.get_tree(for_print=False))
        tree.destroy_non_projectivity()
        length = len(tree.nodes)
        # dependency -> phrasal
        q = [tree.first_node]
        i = 0
        while i < len(q): q.extend(q[i].children); i += 1
        for node in q:
            new_children = []
            dep_children = []
            for child in node.children:
                if child.dep_type == 'p' or not node.parent:
                    new_children.append(child)
                else:
                    dep_children.append(child)
            node.children = new_children
            if dep_children:
                new_node = TreeNode('%d\t%s\t-2\tp'
                                            % (length, node.token.get_nterm()))
                    # warning: do not set node.position here
                new_node.parent = node.parent
                i = node.parent.children.index(node)
                node.parent.children[i] = new_node
                new_node.children = dep_children + [node]
                node.parent = new_node
                for child in dep_children:
                    child.parent = new_node; child.dep_type = 'p'
                tree.nodes.append(new_node)
                length += 1
        # cosmetics
        tree.add_preterminals()
        tree.canonize()
        tree.sort_children()
        return tree


    def prepare_struct_tree(self):
        # phrasal tree with more levels (originally for TIL)
        tree = Tree(self.get_tree(for_print=False))
        tree.destroy_non_projectivity()
        length = len(tree.nodes)
        # dependency -> phrasal
        q = [tree.first_node]
        i = 0
        while i < len(q): q.extend(q[i].children); i += 1
        for node in q:
            new_children = []
            dep_children = []
            for child in node.children:
                if child.dep_type == 'p' or not node.parent:
                    new_children.append(child)
                    if child.deplabel: # (TIL) labels for <coord> tokens
                        node.deplabel = child.deplabel
                        child.deplabel = ''
                else:
                    dep_children.append(child)
            node.children = new_children
            nterm = node.token.get_nterm()
            flat = False #(nterm == '<VP>')
            if flat and dep_children: # flat structure
                new_node = TreeNode('%d\t%s\t-2\tp' % (length, nterm))
                new_node.token = node.token
                new_node.parent = node.parent
                i = node.parent.children.index(node)
                node.parent.children[i] = new_node
                new_node.children = dep_children + [node]
                node.parent = new_node
                for child in dep_children:
                    child.parent = new_node; child.dep_type = 'p'
                    child.deplabel = ''
                tree.nodes.append(new_node)
                length += 1
            if dep_children and not flat: # structured
                si = node.token.surface_index
                i = 0
                while i < len(dep_children) \
                                 and dep_children[i].token.surface_index < si:
                    i += 1 # find the middle one
                dep_children[i:] = reversed(dep_children[i:])
            i_dep = 0
            while not flat and i_dep < len(dep_children): # structured
                new_node = TreeNode('%d\t%s\t-2\tp' % (length, nterm))
                new_node.token = node.token
                new_node.parent = node.parent
                i = node.parent.children.index(node)
                node.parent.children[i] = new_node
                new_node.children = [dep_children[i_dep], node]
                node.parent = new_node
                dep_children[i_dep].parent = new_node
                dep_children[i_dep].dep_type = 'p'
                new_node.deplabel = dep_children[i_dep].deplabel
                dep_children[i_dep].deplabel = ''
                tree.nodes.append(new_node)
                length += 1
                i_dep += 1
        # cosmetics
        tree.add_preterminals(into_leaf=True)
        tree.canonize()
        tree.sort_children()
        return tree


    def get_phr_tree(self, struct=False, graph=False):
        self.set_token_ids()
        if struct: tree = self.prepare_struct_tree()
        else: tree = self.prepare_phr_tree()
        result = []
        for node in tree.nodes:
            if struct: # slightly different format for (TIL) struct tree
                if graph: # simpler format
                    if node == tree.first_node:
                        node_string = '%s\t%s\t-1\tp\t%s' % (node.id,
                                                      node.word, node.deplabel)
                    else:
                        node_string = '%s\t%s\t%s\t%s\t%s' % (node.id,
                                       node.word, node.parent.id,
                                       node.dep_type, node.deplabel)
                else: # everything into the tree
                    if node == tree.first_node:
                        node_string = '%s\t%s\t\t\t-1\tp\t%s' % (node.id,
                                                      node.word, node.deplabel)
                    else:
                        node_string = '%s\t%s\t%s\t%s\t%s\t%s\t%s' % (node.id,
                                       node.word, ','.join(node.token.lemma),
                                       ''.join(node.token.tag), node.parent.id,
                                       node.dep_type, node.deplabel)
            elif node == tree.first_node:
                node_string = '%s\t%s\t-1\t%s' % (node.id, node.word,
                                                  node.dep_type)
            else:
                node_string = '%s\t%s\t%s\t%s' % (node.id, node.word,
                                                 node.parent.id, node.dep_type)
            result.append((int(node.id), node_string))
            result.sort()
        return [item[1] for item in result]


    def get_dep_tree(self, root=True, first=True, for_print=True):
        if first: self.set_token_ids(dep=True)
        result = []
        for token in self.tokens:
            if isinstance(token, PhrToken): continue
            if isinstance(token, LinkToken):
                result.extend(token.target.get_dep_tree(root, first=False))
            if isinstance(token, SurfaceToken):
                sid = str(token.id); word = token.str_word()
                sdep_id = str(find_surface_dep_id(token))
                dl = token.deplabel
                if root and sdep_id == '-1': sdep_id = '10000'
                if for_print:
                    result.append('\t'.join([sid, word, sdep_id, 'd', dl]))
                else:
                    result.append({'id': sid, 'word': word, 'dep_id': sdep_id,
                                   'label': dl, 'lemma': ','.join(token.lemma),
                                   'tag': ','.join(token.tag)})
        if root and first and for_print:
            result.append('\t'.join(['10000', '[root]', '-1', 'd', '']))
        return result


    def print_collx_out_for_node(self, node, file=sys.stdout):
        parent_token = node.token
        for child in node.children:
            token = child.token
            if parent_token.lemma and token.lemma:
                for l1 in parent_token.lemma:
                    for l2 in token.lemma:
                        file.write(l1 + ' <- ' + l2 + '\t(')
                        file.write(parent_token.str_tag() + ', '
                                                             + token.str_tag())
                        file.write(')\t(' + parent_token.str_word())
                        file.write(' <- ' + token.str_word() + ')\n')
            self.print_collx_out_for_node(child, file)


    def print_phrases_for_node(self, node, file=sys.stdout, clause=None):
            # clause -- print phrases only within scope of a clause
        if not node.children: return
        token = node.token
        for child in node.children:
            self.print_phrases_for_node(child, file, clause)
#        if token.str_word() in ['<top>', '<ends>', '<sentence>']: return
        if clause and node not in clause: return
        q = [(node.position, node)]; i = 0
        while i < len(q):
            q.extend([(ch.position, ch) for ch in q[i][1].children
                                                if not clause or ch in clause])
            i += 1
        q.sort()
        if isinstance(token, SurfaceToken):
            file.write('<phr> (%s): ' % token.str_tag())
        else:
            file.write('%s (%s): ' % (token.str_word(), token.str_tag()))
        file.write(' '.join([n[1].token.str_word()
                          for n in q if isinstance(n[1].token, SurfaceToken)]))
        file.write('\n')

    def get_np_phrases_for_node(self, node):
        result = []
        # clause -- print phrases only within scope of a clause
        if not node.children:
            return result

        token = node.token
        for child in node.children:
            for x in self.get_np_phrases_for_node(child):
                result.append(x)

        q = [(node.position, node)]
        i = 0
        while i < len(q):
            q.extend([(ch.position, ch) for ch in q[i][1].children])
            i += 1
        q = sorted(q, key=lambda y: y[0])
        if isinstance(token, SurfaceToken) and token.str_tag().startswith('k1'):
            content = []
            for n in q:
                if isinstance(n[1].token, SurfaceToken):
                    content.append((n[1].token.str_word(), n[1].token.str_lemma(), n[1].token.str_tag()))
            result.append(content)

        return result

    def print_collx_out(self, file=sys.stdout):
        "prints collocations to file"
        tree = Tree(self.get_tree(for_print=False))
        self.print_collx_out_for_node(tree.first_node, file)


    def print_phrases(self, file=sys.stdout):
        "prints phrases to file"
        tree = Tree(self.get_tree(for_print=False))
        self.print_phrases_for_node(tree.first_node, file)

    def get_np_phrases(self):
        tree = Tree(self.get_tree(for_print=False))
        return self.get_np_phrases_for_node(tree.first_node)

    def print_marx_phrases(self, file=sys.stdout, short=True, justwords=False,
                           categories=phr_cats, phr_strip=phr_strip):
        "prints 'marx' phrases to file"
        tree = Tree(self.get_tree(for_print=False), phr_verbs=self.phr_verbs)
        lines = tree.get_marx_phrases(short, justwords, categories, phr_strip,
                                      wc=self.grammar.wclass)
        file.write('\n'.join(lines) + '\n')


    def get_marx_phrases_vert(self, short=True, justwords=False, categories=phr_cats, phr_strip=phr_strip,
                              word2vec=None, filter_phr=False):
        tree = Tree(self.get_tree(for_print=False))
        resulting_phrases = tree.find_marx_phrases_vert(short, justwords, categories, phr_strip,
                                                        wc=self.grammar.wclass)
        for phrase in resulting_phrases:
            if filter_phr:
                if not re.match('k1.*?', phrase['phr_tag']):
                    continue

            result = '<phr phr_tag="{}", phr_heaad="{}">\n'.format(phrase['phr_tag'], phrase['head'])
            for token in phrase['phr_words']:
                if word2vec:
                    result += '{}\t{}\t{}\t{}\n'.format(token['word'], token['lemma'], token['tag'],
                                                        word2vec[token['word']])
                else:
                    result += '{}\t{}\t{}\n'.format(token['word'], token['lemma'], token['tag'])

            result += '</phr>'
            yield result


    def print_laa_tree(self, file=sys.stdout):
        "prints phrasal tree in LAA format to file"
        tree = self.prepare_phr_tree()
        file.write(tree.get_laa_format() + '\n')


    def print_relaxed_tree(self, file=sys.stdout):
        "prints relaxed tree to file"
        file.write('\n'.join(self.get_relaxed_tree()) + '\n')


    def get_relaxed_tree(self):
        "prints relaxed tree to file"
        result = []
        tree = Tree(self.get_tree(for_print=False), phr_verbs=self.phr_verbs)
        lines = tree.get_marx_phrases(wc=self.grammar.wclass)
            # draw tree from marx lines
        deps = {}; result_lines = {}
        for line in lines:
            if line.startswith('<phr>') or line.startswith('<vp>') \
                                        or line.startswith('<coord>'):
                curr_line_words = line.split(': ', 1)[1]
            elif line.startswith('<phrnum>') or line.startswith('<vpnum>') \
                                             or line.startswith('<coordnum>'):
                curr_line_id = tuple(map(int,
                                     line.split(': ', 1)[1].strip().split()))
                result_lines[curr_line_id] = curr_line_words
            elif '->>> ' in line:
                id, dep = line.strip('[]').replace(',', '').split('] ->>> [')
                deps[tuple(map(int, id.split()))] = tuple(map(int,dep.split()))
        key_map = {-1: -1}; i = 0
        for key in sorted(result_lines): key_map[key] = i; i += 1
        for key in sorted(result_lines):
            id = key_map[key];
            words = result_lines[key]
            dep_id = key_map[deps.get(key, -1)]
            result.append('%s\t%s\t%s\td' % (id, words, dep_id))
        return result


    def print_tree(self, file=sys.stdout, tags={}):
        "prints hybrid tree to file"
        if not tags:
            file.write('\n'.join(self.get_tree()) + '\n')
        else: # output tags that were in the input vertical
            for i, line in enumerate(self.get_tree()):
                if i in tags: file.write(''.join(tags[i]))
                file.write(line + '\n')


    def print_dep_tree(self, file=sys.stdout, root=False):
        "prints dependency tree to file"
        file.write('\n'.join(self.get_dep_tree(root=root)) + '\n')


    def print_phr_tree(self, file=sys.stdout, struct=False):
        "prints phrasal tree to file"
        file.write('\n'.join(self.get_phr_tree(struct=struct)) + '\n')


    def print_sconll(self, file=sys.stdout):
        tree = self.get_dep_tree(root=False, first=True, for_print=False)
        tmpl = '%(word)s\t%(lemma)s\t%(tag)s\t%(id)s\t%(dep_id)s,%(label)s'
        file.write('\n'.join([tmpl % ll for ll in tree]) + '\n')


    def print_verb_frames(self, file=sys.stdout):
        from Queue import Queue
        tree = Tree(self.get_tree(for_print=False))
        wc = self.grammar.wclass
        for verb in tree.nodes:
            if not match_token(verb.token, wc['verb']): continue
            if verb.parent.word != '<clause>': continue
            pred = verb.token.str_lemma()
            subjs, dat_objs, acc_objs, post_preps, adverbs = [], [], [], [], []
            childrenq = Queue()
            for ch in verb.children: childrenq.put(ch)
            while not childrenq.empty():
                child = childrenq.get()
                if child.deplabel == 'subject':
                    subjs.append(child.token.str_lemma())
                elif child.deplabel == 'object': # XXX this is Czech-only
                    if match_token(child.token, {'tag':[re.compile('.*c4.*')]}):
                        acc_objs.append(child.token.str_lemma())
                    elif match_token(child.token,
                                     {'tag':[re.compile('.*c3.*')]}):
                        dat_objs.append(child.token.str_lemma())
                elif child.deplabel == 'adverb':
                    adverbs.append(child.token.str_lemma())
                elif child.deplabel == 'additional-prep' and child.children:
                    post_preps.append(child.children[0].token.str_lemma())
                elif child.deplabel == 'verb-object':
                    for ch in child.children: childrenq.put(ch)
                    pred = child.token.str_lemma()
            subjs.append('-')
            dat_objs.append('-')
            acc_objs.append('-')
            adverbs.append('-')
            post_preps += ['-', '-']
            file.write('\t'.join([pred, subjs[0], acc_objs[0], dat_objs[0],
                                  adverbs[0], post_preps[0], post_preps[1]])
                       + '\n')


    def print_commas(self, file=sys.stdout):
        tree = Tree(self.get_tree(for_print=False))
        cprev = False
        for i, token in enumerate(self.tokens):
            if not isinstance(token, SurfaceToken): continue
            if i > 0 and not cprev:
                dep_word = token.dependency.str_word()
                if dep_word == '<c>':
                    file.write(', ')
                if dep_word.startswith('<c:') and hasattr(token, 'match'):
                    items = dep_word[3:-1].split(':')
                    minfreq, minscore = 0, 0
                    idxs = []
                    for item in items:
                        if item.startswith('f'): minfreq = int(item[1:])
                        elif item.startswith('s'): minscore = float(item[1:])
                        else: idxs.append(int(item))
                    if len(idxs) == 1:
                        file.write('<comma ifincoord="%s %s" minfreq="%s" '
                                   'minscore="%s"/> '
                                   % (token.str_lemma(),
                                      token.match.rel_tokens[idxs[0]].str_lemma(),
                                      minfreq, minscore))
                    else:
                        lemmas = ' '.join([token.match.rel_tokens[i].str_lemma()
                                           for i in idxs])
                        file.write('<comma ifincoord="%s" minfreq="%s" '
                                   'minscore="%s"/> '
                                   % (lemmas, minfreq, minscore))
            file.write(token.str_word())
            if not token.glue: file.write(' ')
            if token.str_word().startswith((',', ':', ';', '.', '?', '!', '-',
                                            '(')):
                cprev = True
            else:
                cprev = False
        file.write('\n')


