#!/bin/bash

rm -rf set
mkdir set
cp -r *.py *.set data TreeViewer COPYING README set/
rm set/pdtgrammar*

tar -czvf set-0.8.tar.gz set
rm -rf set
