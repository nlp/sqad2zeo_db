#!/usr/bin/env python
# -*- coding: utf8 -*-

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# contains basic class -- Token and specialized token classes

import copy, re
import settings
import matcher


def parse_lemma(str_lemma):
    if len(str_lemma) > 1 and ',' in str_lemma:
        return set(str_lemma.split(','))
    return set([str_lemma])


def translate_pos_tag(str_tag):
    if not str_tag:
        return 'k?'
    transl_table = {
        1: { 'A': 'k2', 'C': 'k4', 'D': 'k6', 'I': 'k0', 'J': 'k8', 'N': 'k1',
             'P': 'k3', 'V': 'k5', 'R': 'k7', 'T': 'k9', 'X': 'k.', 'Z': 'kI',
           },
        2: { ',': 'xS', '1': 'xS', '4': 'yR', '5': 'xP', '6': 'xP', '8': 'xO',
             '9': 'yR', '?': 'yR', 'B': 'mI', 'C': 'mN', 'D': 'xD', 'E': 'yR',
             'F': 'q1', 'H': 'xPqC', # q1 = 1st part of prep, qC = clitical
             'J': 'yR', 'K': 'yR', 'L': 'yI', 'O': 'xO',
             'P': 'xP', 'Q': 'yR', 'S': 'xO', 'U': 'yR', 'W': 'yN', 'Y': 'yR',
             'Z': 'xI', '^': 'xC', 'a': 'yI', 'b': 'eAd1', 'c': 'kYmC',
             'd': 'k2', # ?
             'e': 'mS', 'f': 'mF', 'i': 'mR',
             'j': 'qN', 'k': 'qA', # qN = noun, qA = adjective
             'm': 'mD', 'o': 'k6eAd1', 'p': 'mA', 'q': 'mA', 's': 'mN',
             't': 'mI', 'u': 'k6yR', 'v': 'k6eAd1', 'w': 'k2', # ?
             'y': 'k1gF', 'z': 'yR',
             '!': 'k6', '.': 'k2', '3': 'k4', ';': 'k1', 'x': 'kA', '~': 'k5',
           },
        3: { 'F': 'gF', 'H': 'gNgF', 'I': 'gI', 'M': 'gM', 'N': 'gN',
             'Q': 'gNgF', 'T': 'nPgIgF', 'X': 'g.', 'Y': 'gMgI', 'Z': 'gMgNgI',
           },
        4: {  'D': 'nP', 'P': 'nP', 'S': 'nS', 'W': 'n.', 'X': 'n.' },
        5: { '1': 'c1', '2': 'c2', '3': 'c3', '4': 'c4', '5': 'c5', '6': 'c6',
             '7': 'c7', 'X': 'c.',
           },
        6: {}, # not interesting (at least so far)
        7: {},
        8: { '1': 'p1', '2': 'p2', '3': 'p3', 'X': 'p.' },
        9: { 'F': 'mBmI', 'P': 'mI', 'R': 'mR' }, # 'X': 'm.' ? m already in [2]
        10:{ '1': 'd1', '2': 'd2', '3': 'd3' },
        11:{ 'A': 'eA', 'N': 'eN' },
        12:{ 'P': 'mN' }, # 'A' is too broad
        13:{},
        14:{},
        15:{},
    }
    result = ''
    for i in range(15):
        result += transl_table[i+1].get(str_tag[i], '')
    return result


class Token:

    def __init__(self, segment, dependency=None, phrasal=False, children=[],
                        dep_prob=100, surface_index=-1):
        self.dependency = dependency # token - the superior one
        self.phrasal = phrasal # defines if the 'dependency' link is phrasal
        self.segment = segment # segment containing this token
        self.children = copy.copy(children) # list of child tokens
        self.dep_prob = dep_prob # probability of dependency
        self.word = ''
        self.lemma = set()
        self.tag = []
        self.under_phrase = [] # helps in advanced parts of parsing
        self.surface_index = surface_index
        self.sem = set()
        self.deplabel = ''
        self.invisible = False


    def get_nterm(self):
        wc = self.segment.grammar.wclass # this is not too nice but WiB
        if matcher.match_token(self, wc['noun'], match_phr=True):
            return '<NP>'
        if matcher.match_token(self, wc['prep'], match_phr=True):
            return '<PP>'
        if matcher.match_token(self, wc['verb'], match_phr=True):
            return '<VP>'
        if matcher.match_token(self, wc['abbr'], match_phr=True):
            return '<ABBR>'
        if matcher.match_token(self, wc['adj'], match_phr=True):
            return '<ADJP>'
        if matcher.match_token(self, wc['adv'], match_phr=True):
            return '<ADVP>'
        if matcher.match_token(self, wc['num'], match_phr=True):
            return '<NUM>'
        return '<X>'

    def get_preterm(self):
        wc = self.segment.grammar.wclass # this is not too nice but WiB
        if matcher.match_token(self, wc['noun'], match_phr=True):
            return '<N>'
        if matcher.match_token(self, wc['prep'], match_phr=True):
            return '<PREP>'
        if matcher.match_token(self, wc['verb'], match_phr=True) \
                or matcher.match_token(self, wc['vpart'], match_phr=True):
            return '<V>'
        if matcher.match_token(self, wc['abbr'], match_phr=True):
            return '<ABBR>'
        if matcher.match_token(self, wc['adj'], match_phr=True):
            return '<ADJ>'
        if matcher.match_token(self, wc['adv'], match_phr=True):
            return '<ADV>'
        if matcher.match_token(self, wc['num'], match_phr=True):
            return '<NUM>'
        if matcher.match_token(self, wc['part'], match_phr=True) \
                or matcher.match_token(self, wc['interj'], match_phr=True):
            return '<PART>'
        if matcher.match_token(self, wc['conj'], match_phr=True):
            return '<CONJ>'
        if matcher.match_token(self, wc['punct'], match_phr=True):
            return '<PUNCT>'
        if matcher.match_token(self, wc['pron'], match_phr=True):
            return '<PRON>'
        return '<X>'


    def is_head(self):
        return (self.segment.head == self)


    def str_tag(self):
        return ','.join(self.tag)

    def str_lemma(self):
        return ','.join(self.lemma)


class SurfaceToken(Token):

    num_re = re.compile('^[0-9][0-9,./]*$')
    punct_re = re.compile('^&.*;$|^[\*,\.%=\-_;:!\?/\'"\(\)\+\|\[\]]+$')
    abbr_re = re.compile('^[A-Z][A-Z]+$')
    glue = False

    def __init__(self, segment, word, lemma, tag, sem, dependency=None,
                 phrasal=False, children=[], dep_prob=100, surface_index=-1,
                 pos_tags=False, first=False, desamb_hacks=False):
        Token.__init__(self, segment, dependency, phrasal, children, dep_prob,
                                                                 surface_index)
        self.word = word.strip() # as string
        if lemma == '#num#' or not lemma: lemma = self.word # desamb hack
        self.lemma = parse_lemma(lemma) # as set of strings
        self.tag = (pos_tags and map(translate_pos_tag, tag.split('+')) ) \
                   or (tag == ',' and [tag]) or tag.split(',')
        self.sem = set(sem.split(',')) # set of strings
        if self.word[0].isupper():
            self.tag[0] += ';cap'
        if desamb_hacks or not self.tag or not self.tag[0]:
            # hack some known tag omissions
            if self.num_re.match(word):
                self.tag = ['k4'] # numeral
                self.tag[0] += 'c1c2c3c4c5c6c7'
                self.tag[0] += 'gMgIgFgN'
                if self.word == '1': self.tag[0] += 'nS'
                else: self.tag[0] += 'nP'
                if self.word.endswith('.'): self.tag[0] += 'xO'
                else: self.tag[0] += 'xC'
            if self.punct_re.match(word):
                self.tag = ['kI'] # punctuation
            if self.abbr_re.match(word):
                self.tag.append('kA')
            if word == "lze": self.tag.append('k5mI')
            if word == 'jako': self.tag.append('k7c1')
            if word == 'nej': self.tag.append('k2c1')

    def str_word(self):
        return self.word


class PhrToken(Token):

    def __init__(self, segment, label,  tag=[], dependency=None, phrasal=False,
                       children=[], head=None, dep_prob=100, surface_index=-1):
        Token.__init__(self, segment, dependency, phrasal, children, dep_prob,
                            surface_index)
        self.label = label # as string
        self.tag = tag
        self.head = head # token
            # inherit head attributes
# THIS IS POSSIBLE ONLY IF THE TOKEN HAS THE RIGHT POSITION IN THE SENTENCE
#        if head:
#            self.word = head.word
#            self.lemma = head.lemma
#            self.tag = head.tag


    def str_word(self):
        return self.label


class LinkToken(Token):

    def __init__(self, segment, target, dependency=None, phrasal=False,
                        children=[], label='', dep_prob=100):
        Token.__init__(self, segment, dependency, phrasal, children, dep_prob,
                                                                            -1)
        self.target = target # segment - target of the link
        self.label = label # does not have to be displayed...
        target.pointer = self


    def str_word(self):
        return self.label

