#!/usr/bin/python

# median and average from a (tab-delimited) multi-column file - column with
# numbers as a parameter

import sys

items = []
sum = 0
i   = 0
if len(sys.argv) > 1: column = int(sys.argv[1]) # column with numbers
else: column = 0

for line in sys.stdin.xreadlines():
    if not line.strip(): continue
    num = line.strip().split('\t')[column]
    items.append(float(num))
    sum += float(num)
    i += 1

items.sort()
length = len(items)
if length % 2 == 0: median = ( items[length/2]  + items[length/2-1] ) / 2
else: median = items[length/2]

print 'sum: ' + str(sum)
print 'lines: ' + str(i)
print 'average: ' + str(sum/i)
print 'median: ' + str(median)
