# Copyright (c) 2010 Vojtech Kovar
# -*- coding:utf8 -*-

# Copyright 2008 Vojtech Kovar
# inspired by Pavel Rychly's CGIPublisher
#
# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, cgi
from types import MethodType, DictType, ListType
import codecs
import imp

try:
    from Cheetah import VersionTuple
    major, minor, bugfix, _status, _statusNr = VersionTuple
except ImportError:
    from Cheetah import Version
    try: major, minor, bugfix = map(int, Version.split("."))
    except ValueError: major, minor, bugfix = 0, 0, 0
if (major, minor, bugfix) >= (2, 2, 0):
    has_cheetah_unicode_internals = True
else:
    has_cheetah_unicode_internals = False


def function_defaults (fun):
    defs = {}
    if hasattr (fun, '__init__'):
        fun = fun.__init__
    try:
        dl = fun.func_defaults or ()
    except AttributeError:
        return {}
    nl = fun.func_code.co_varnames
    for a,v in zip (nl [fun.func_code.co_argcount - len(dl):], dl):
        defs [a] = v
    return defs


def correct_types (args, defaults, del_nondef=0):
    corr_func = {type(0): int, type(0.0): float, ListType: lambda x: [x]}
    for k, v in args.items():
        if k.startswith('_') or type (defaults.get (k,None)) is MethodType:
            del args[k]
        elif defaults.has_key(k):
            default_type = type (defaults[k])
            if default_type is not ListType and type(v) is ListType:
                args[k] = v = v[-1]
            if type(v) is not default_type:
                try:
                    args[k] = corr_func[default_type](v)
                except: pass
        else:
            if del_nondef:
                del args[k]
    return args


class CheetahResponseFile:
    def __init__(self, outfile):
        if has_cheetah_unicode_internals:
            outfile = codecs.getwriter("utf-8")(outfile)
        self.outfile = outfile
    def response(self):
        return self.outfile


class CGInterface:

    _headers = {'Content-Type': 'text/html; charset=utf-8'}
    _keep_blank_values = 0
    _cookieattrs = []
    _template_dir = 'cmpltmpl/'
    _tmp_dir = '/tmp'
    exceptmethod = None
    debug = None
    uilang = ''


    def __init__ (self):
        # correct _template_dir
        if not os.path.isdir (self._template_dir):
            self._template_dir = imp.find_module('cmpltmpl')[1]

        
    def is_template (self, template):
        try:
            imp.find_module(template, [self._template_dir])
            return True
        except ImportError:
            return False


    def call_method (self, method, args, named_args):
        na = named_args.copy()
        correct_types (na, function_defaults (method), 1)
        return apply (method, args[1:], na)


    def clone_self (self):
        na = {}
        for a in dir(self) + dir(self.__class__):
            if not a.startswith('_') and not callable (getattr (self, a)):
                na[a] = getattr (self, a)
        return na


    def parse_parameters (self, cookies=None, environ=os.environ, post_fp=None):
        self.environ = environ
        named_args = {}
        form = cgi.FieldStorage(keep_blank_values=self._keep_blank_values,
                                                        environ=self.environ, fp=post_fp)
        for k in form.keys(): named_args[str(k)] = form.getvalue(k)
        na = named_args.copy()
        correct_types (na, self.clone_self())
        self.__dict__.update (na)
        return named_args


    def run_unprotected (self, path=None):
        if path is None:
            path = os.getenv('PATH_INFO','').strip().split('/')[1:]
        if len (path) is 0 or path[0] is '':
            raise RuntimeError('Invalid URL')
        else:
            if path[0].startswith ('_'):
                raise 'access denied'
        named_args = self.parse_parameters()
        methodname, tmpl, result = self.process_method (path[0], path, named_args)
        self.output_headers()
        self.output_result (methodname, tmpl, result)


    def process_method (self, methodname, pos_args, named_args):
        if self.uilang == 'cz': tmpl_suff = '_cz.tmpl'
        else: tmpl_suff = '.tmpl'
        reload = {'headers': 'wordlist_form'}
        if getattr (self, 'reload', None):
            self.reload = None
            reload_template = reload.get(methodname, methodname + '_form')
            if self.is_template (reload_template):
                return self.process_method(reload_template, pos_args, named_args)
        if not hasattr (self, methodname):
            if methodname.endswith ('_form'):
                return (methodname[:-5], methodname + tmpl_suff, {})
            else:
                raise RuntimeError('unknown method: "%s" dict:%s' % (methodname,
                                                                                    self.__dict__))
        method = getattr (self, methodname)
        try:
            return (methodname,
                    getattr (method, 'template', methodname + tmpl_suff),
                    self.call_method (method, pos_args, named_args))
        except Exception, e:
            if not self.exceptmethod and self.is_template(methodname +'_form'):
                self.exceptmethod = methodname + '_form'
            if self.debug or not self.exceptmethod:
                raise
            self.error = str(e)
            em, self.exceptmethod = self.exceptmethod, None
            return self.process_method (em, pos_args, named_args)


    def output_headers (self, outf=sys.stdout):
        if outf:
            for k,v in self._headers.items():
                outf.write('%s: %s\n' % (k, v))
            outf.write('\n')
        return self._headers

       
    def output_result (self, methodname, template, result, outf=sys.stdout):
        from Cheetah.Template import Template
        # Template
        if type(result) is DictType:
            if template.endswith('.tmpl'):
                class_name = template[:-5] # appropriate module import
                file, pathname, description = \
                    imp.find_module(class_name, [self._template_dir])
                module = imp.load_module(class_name, file, pathname, description)

                TemplateClass = getattr(module, class_name)

                result = TemplateClass(searchList=[result, self])
            else:
                result = Template(template, searchList=[result, self])
            result.respond(CheetahResponseFile(outf))
        # Other (string)
        else:
            outf.write(str(result))

