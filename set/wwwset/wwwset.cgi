#!/usr/bin/env python
# -*- Python -*-

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


import cgitb; cgitb.enable()

set_path = '/nlp/projekty/set/set' # path to the SET distribution
wwwset_path = '/nlp/projekty/set/set/wwwset' # path to the wwwSET distribution

import sys, os
if wwwset_path not in sys.path: sys.path.append (wwwset_path)
if set_path not in sys.path: sys.path.append (set_path)

from wwwset import Presentation

class SETwww(Presentation):
    desambpath = '/corpora/programy/desamb.sh' # tagger
    unitokpath = '/corpora/programy/unitok.py --language=other' # tokeniser
    _outpath = '/nlp/projekty/set/public_html/wwwset_rundata'
        # path to the data produced by SET ...
    relpath = '../wwwset_rundata' # and where it can be found using HTTP
    grammar_path = os.path.join(set_path, 'grammar.set')
    set_path = set_path
    wwwset_path = wwwset_path

if __name__ == '__main__':
    SETwww().run_unprotected()
