# Copyright (c) 2010 Vojtech Kovar
# -*- coding:utf8 -*-

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.

# the basic module of wwwSET interface

import os
from cginterface import CGInterface
from subprocess import Popen
from set import version as set_version

class Presentation (CGInterface):
    set_path = set_path = ''
    wwwset_path = ''
    _outpath = ''

    desambpath = ''
    unitokpath = ''

    plaintext = ''
    vertical = ''
    grammar_path = ''
    own_grammar = ''

    def __init__(self):
        CGInterface.__init__(self)
        self.set_version = set_version

    def first_page(self):
        return {}

    def tag(self, index):
        if not self.desambpath or not self.unitokpath:
            raise Exception("desambpath and/or unitokpath not set.")
        cmd = 'echo "%s" | %s' % (self.plaintext, self.unitokpath)
        cmd += ' | %s 2>/dev/null' % self.desambpath
        cmd += ' >%s/tagged.%05i' % (self._outpath, index)
        p = Popen(cmd, shell=True)
        sts = os.waitpid(p.pid, 0)
        if sts[1]: raise Exception('Problem in tagging: %s\n' % sts[1]
                                + 'command """%s""" failed.' % cmd)
        return open('%s/tagged.%05i' % (self._outpath, index)).read()
       

    def parse(self):
        if not self._outpath:
            raise Exception("_outpath not set.")
            # set a random session identifier
        import random
        result = {'Sentences': []}
        id = random.randint(0, 99999)
            #import everything needed and set logger
        from grammar import Grammar
        from setparser import Parser
        from segment import Segment
        from vertical_processor import VerticalProcessor
        from TreeViewer.TreeExporter import exportTrees
        from utils import log
        from utils import Logger
        logfile_id = '%s/log.%05i' % (self._outpath, id)
        Logger.file = logfile_id
        Logger.verbose = True
            # parse -- this is similar (but not same) as the process in set.py
        if self.plaintext: self.vertical = self.tag(id)
        vert_processor = VerticalProcessor(self.vertical.split('\n'))
        lines, end_tag, tags = vert_processor.get_sentence()
        if self.own_grammar:
            grammar_id = '%s/grammar.%05i' % (self._outpath, id)
            f = open(grammar_id, 'w'); f.write(self.own_grammar); f.close()
            self.grammar_path = grammar_id
        g = Grammar(self.grammar_path)
        p = Parser(g)
        i = 0
        if not lines or (len(lines) == 1 and not lines[0]):
            raise Exception('Tagged vertical is empty. Check desamb.')
        while lines:
            i += 1
            s = Segment(lines)
            if not s.tokens:
                lines, end_tag, tags = vert_processor.get_sentence()
                del s
                continue
            p.parse(s)
                # phrases
            phrasesfile_id = '%s/phrases.%05i.%03i' % (self._outpath, id, i)
            phrasesfile = open(phrasesfile_id, 'w')
            s.print_marx_phrases(phrasesfile, justwords=True)
            phrasesfile.close()
                # text tree
            treefile_id = '%s/tree.%05i.%03i' % (self._outpath, id, i)
            treefile = open(treefile_id, 'w')
            s.print_tree(treefile)
            treefile.close()
                # tree picture
            svgfile_id = '%s/tree.%05i.%03i.svg' % (self._outpath, id, i)
            svgfile_link = self.relpath + '/' + os.path.basename(svgfile_id)
            hw, hh = exportTrees([s.get_tree()], svgfile_id)
                # dependency tree
            dtreefile_id = '%s/dtree.%05i.%03i' % (self._outpath, id, i)
            dtreefile = open(dtreefile_id, 'w')
            s.print_dep_tree(dtreefile)
            dtreefile.close()
                # dependency tree picture
            dsvgfile_id = '%s/dtree.%05i.%03i.svg' % (self._outpath, id, i)
            dsvgfile_link = self.relpath + '/' + os.path.basename(dsvgfile_id)
            dw, dh = exportTrees([s.get_dep_tree(root=True)], dsvgfile_id)
                # relaxed tree
            rtreefile_id = '%s/rtree.%05i.%03i' % (self._outpath, id, i)
            rtreefile = open(rtreefile_id, 'w')
            s.print_relaxed_tree(rtreefile)
            rtreefile.close()
                # relaxed tree picture
            rsvgfile_id = '%s/rtree.%05i.%03i.svg' % (self._outpath, id, i)
            rsvgfile_link = self.relpath + '/' + os.path.basename(rsvgfile_id)
            rw, rh = exportTrees([s.get_relaxed_tree()], rsvgfile_id)
                # phrasal tree
            ptreefile_id = '%s/ptree.%05i.%03i' % (self._outpath, id, i)
            ptreefile = open(ptreefile_id, 'w')
            s.print_phr_tree(ptreefile)
            ptreefile.close()
                # phrasal tree picture
            psvgfile_id = '%s/ptree.%05i.%03i.svg' % (self._outpath, id, i)
            psvgfile_link = self.relpath + '/' + os.path.basename(psvgfile_id)
            pw, ph = exportTrees([s.get_phr_tree()], psvgfile_id)
                # all phrases
            allphrasesfile_id = '%s/allphrases.%05i.%03i' % (self._outpath,
                                                                         id, i)
            allphrasesfile = open(allphrasesfile_id, 'w')
            s.print_phrases(allphrasesfile)
            allphrasesfile.close()
                # LAA tree
            ltreefile_id = '%s/ltree.%05i.%03i' % (self._outpath, id, i)
            ltreefile = open(ltreefile_id, 'w')
            s.print_laa_tree(ltreefile)
            ltreefile.close()
                # collocations
            cfile_id = '%s/collx.%05i.%03i' % (self._outpath, id, i)
            cfile = open(cfile_id, 'w')
            s.print_collx_out(cfile)
            cfile.close()

            result['Sentences'].append({
                                  'phrases': open(phrasesfile_id).read(),
                                  'text_tree': open(treefile_id).read(),
                                  'svgfile': svgfile_link, # hybrid
                                  'hw': hw, 'hh': hh,
                                  'rtext_tree': open(rtreefile_id).read(),
                                  'rsvgfile': rsvgfile_link,
                                  'rw': rw, 'rh': rh,
                                  'dtext_tree': open(dtreefile_id).read(),
                                  'dsvgfile': dsvgfile_link,
                                  'dw': dw, 'dh': dh,
                                  'ptext_tree': open(ptreefile_id).read(),
                                  'pw': pw, 'ph': ph,
                                  'psvgfile': psvgfile_link,
                                  'allphrases': open(allphrasesfile_id).read(),
                                  'laa': open(ltreefile_id).read(),
                                  'collx': open(cfile_id).read(),
                                        })
            lines, end_tag, tags = vert_processor.get_sentence()
            del s
        result['vertical'] = self.vertical
        result['log'] = open(logfile_id).read()
        return result

