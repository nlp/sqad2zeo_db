#!/usr/bin/python

import sys

gold = open(sys.argv[1])
test = open(sys.argv[2])

no_found, no_gold, no_correct = 0, 0, 0

for line in test:
    goldline = gold.readline()
    words = line.strip().split()
    gwords = goldline.strip().split()
    if not words: continue
    i, gi = 0, 0
    while True:
        if words[i] == ',' or gwords[gi] == ',':
            if words[i] == gwords[gi]:
                no_correct += 1
            if words[i] == ',':
                no_found += 1
                i += 1
            if gwords[gi] == ',':
                no_gold += 1
                gi += 1
        else: # check that words are same
            if words[i] == gwords[gi]:
                i += 1
                gi += 1
            else:
                raise RuntimeError(words, gwords)
        if i >= len(words) or gi >= len(gwords): break

print '# found commas:', no_found
print '# gold commas:', no_gold
print '# correctly found commas:', no_correct

if no_found and no_gold and no_correct:
    prec = float(no_correct)/no_found
    rec = float(no_correct)/no_gold
    print
    print 'precision:', prec
    print 'recall:', rec
    print 'F:', 2 * prec * rec / (prec + rec)
