import sys, re

tag_re = re.compile('<.*>')

ignore = True
for line in sys.stdin:
    if line.startswith("<s ") or line.startswith("<s>"):
        ignore = False
        print
        print
    elif line.startswith("</s>"):
        ignore = True
    elif not ignore and not tag_re.match(line):
        sys.stdout.write(line.split('\t')[0].strip() + " ")
