#!/usr/bin/env python

import sys

# adds collocations to *.dict and *.collx files.

coll_path = 'data/lemma-lemma'
dict_file = coll_path + '.dict'
collx_file = coll_path + '.collx'

iwords = {}
collocations = {}

# read dictionary
dictionary = open(dict_file)
number = 0
counter = 1
for item in dictionary:
    if counter == 1:
        word = item.strip()
        counter = 0
        number += 1
    else:
        index = int(item.strip())
        iwords[index] = word
        counter = 1
dictionary.close()

colloxfile = open(collx_file)
counter = 1
for i, item in enumerate(colloxfile):
    if counter == 1:
        i1, i2 = tuple([int(num) for num in item.split()])
        sys.stdout.write(iwords[i1] + '\t' + iwords[i2] + '\t')
        counter = 0
    else:
        sys.stdout.write(item.strip() + '\n')
        counter = 1
colloxfile.close()

