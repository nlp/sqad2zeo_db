#!/usr/bin/python

# ACHTUNG loads both files into memory

import sys

if len(sys.argv) != 4:
    print "Usage: %s GOLD TEST ORIG" % sys.argv[0]
    exit(1)

gold = open(sys.argv[1]).read().decode('utf8')
test = open(sys.argv[2]).read().decode('utf8')
orig = open(sys.argv[3]).read().decode('utf8')

gi, ti, oi = 0, 0, 0

all_errs, new_errs, missed_errs, fixed_errs, found_errs = 0, 0, 0, 0, 0

while True:
    if ti >= len(test) or gi >= len (gold) or oi > len(orig):
        break
    elif gold[gi].isspace() or gold[gi] in '":.-?%' or gold[gi] == u'\uFEFF':
        gi += 1
    elif test[ti].isspace() or test[ti] in '":.-?%' or test[ti] == u'\uFEFF':
        ti += 1
    elif orig[oi].isspace() or orig[oi] in '":.-?%' or orig[oi] == u'\uFEFF':
        oi += 1
    elif test[ti].lower() == gold[gi].lower() == orig[oi].lower():
        gi += 1
        ti += 1
        oi += 1
    elif not test[ti].lower() == gold[gi].lower() == orig[oi].lower() \
                   and gold[gi] != ',' and test[ti] != ',' and orig[oi] != ',':
        print # mismatch, diff not only in commas/punctuation
        print "GOLD:", gold[gi:gi+100].encode('utf8').replace('\n', '#').replace('\r', '#')
        print
        print "TEST:", test[ti:ti+100].encode('utf8').replace('\n', '#').replace('\r', '#')
        print
        print "ORIG:", orig[oi:oi+100].encode('utf8').replace('\n', '#').replace('\r', '#')
        print
        raise RuntimeError('mismatch')
    elif gold[gi] == orig[oi] and orig[oi] != test[ti]: # new mistake
        all_errs += 1
        found_errs += 1
        new_errs += 1
        if gold[gi] == ',': gi += 1
        if test[ti] == ',': ti += 1
        if orig[oi] == ',': oi += 1
    elif orig[oi] == test[ti] and test[ti] != gold[gi]: # missed mistake
        all_errs += 1
        missed_errs += 1
        if gold[gi] == ',': gi += 1
        if test[ti] == ',': ti += 1
        if orig[oi] == ',': oi += 1
    elif gold[gi] == test[ti] and test[ti] != orig[oi]: # fixed mistake
        all_errs += 1
        found_errs += 1
        fixed_errs += 1
        if gold[gi] == ',': gi += 1
        if test[ti] == ',': ti += 1
        if orig[oi] == ',': oi += 1
#    print gold[gi:gi+20], '##', test[ti:ti+20], '##', orig[oi:oi+20]

print '# all differences:', all_errs
print '# fix attempts:', found_errs
print '# correctly fixed errors:', fixed_errs
print '# newly introduced errors:', new_errs
print '# unnoticed errors:', missed_errs
print

if found_errs and all_errs and fixed_errs:
    prec = float(fixed_errs)/found_errs
    rec = float(fixed_errs)/all_errs
    print 'precision:', prec
    print 'recall:', rec
    print 'F:', 2 * prec * rec / (prec + rec)
    print

