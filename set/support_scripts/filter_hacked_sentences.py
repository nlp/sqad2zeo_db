#!/usr/bin/python

import sys

l1, l2, l3, l4 = '', '', '', ''

for line in sys.stdin:
    if l1.startswith('<g />') and l2.startswith('.\t') \
            and l3.startswith('</s>') and l4.startswith('<s hack="1">'):
        l1, l2, l3, l4 = '', '', '', ''
    if l1: sys.stdout.write(l1)
    l1 = l2
    l2 = l3
    l3 = l4
    l4 = line
if l1: sys.stdout.write(l1)
if l2: sys.stdout.write(l2)
if l3: sys.stdout.write(l3)
if l4: sys.stdout.write(l4)
