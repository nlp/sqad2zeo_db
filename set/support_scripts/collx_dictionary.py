#!/usr/bin/env python

import get_word_list, get_collx_for_lemma
import sys

# builds collocation *.dict and *.collx files. Requires get_word_list,
# get_collx_for_lemma (these determine which collocations will be included)

if len(sys.argv) != 3:
    print 'Usage: %s dict_file_name collx_file_name' % sys.argv[0]
    exit(1)

dict_file_name = sys.argv[1]
collx_file_name = sys.argv[2]

try:
    stoplist_file = open('stoplist')
    stoplist = set([line.strip() for line in stoplist_file])
    stoplist_file.close()
except:
    stoplist = set([])

print stoplist
exit(1)
words, tmp, collx = {}, {}, {}
counter = 0

# obtain the wordlist
print 'obtaining wordlist...'
for item in get_word_list.get_list():
    if item.isalpha() and (not item in stoplist):
        counter += 1
        words [item] = counter

# obtain collocations
sys.stdout.write('obtaining collocations...')
for index, item in enumerate(words):
    if index % 50 == 0: sys.stdout.write('\nNow at %d' % index)
    sys.stdout.write('.')
    sys.stdout.flush()
    try: collocates = get_collx_for_lemma.get_collx(item.encode('utf8'))
    except: print item; pass
    for wrd in collocates:        
        if not wrd.isalpha() or wrd in stoplist: continue
        # not a known word
        if wrd not in words and wrd not in tmp:
            counter += 1
            tmp[wrd] = counter
            collx[(words[item], counter)] = 1
        # word already in dictionary
        elif wrd in words:
            # ID comparison and writing
            if words[item] < words[wrd]: collx[(words[item], words[wrd])] = 1
            else: collx[(words[wrd], words[item])] = 1
        # word in a temporary dictionary
        elif wrd in tmp:
            collx[(words[item], tmp[wrd])] = 1

# join the wordlists
for item in tmp:
    words[item] = tmp[item]

print '\nwriting information...'

# writing dictionary into file
dictionary = open(dict_file_name, 'w')
for item in words:
    dictionary.write(item.encode('utf8')+'\n')
    dictionary.write(str(words[item])+'\n')
dictionary.close()

# writing collocation information
collox_file = open(collx_file_name, 'w')
for item in collx :
    collox_file.write(str(item[0]) + ' ' + str(item[1]) +'\n')
    collox_file.write(str(collx[item])+'\n')
collox_file.close()

