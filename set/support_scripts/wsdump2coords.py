#!/usr/bin/python

import sys

for line in sys.stdin:
    w1, r, w2, f, s, lcm = line.split('\t')
    freq = int(f)
    score = round(float(s), 1)
    if freq > 1 and w1 and w2 and w1 != w2:
        print '\t'.join([w1[:-2], w2[:-2], f, str(score)])
