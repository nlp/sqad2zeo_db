#!/usr/bin/python

import sys, copy

sentence = []
clauses = []
comma_idx = []

bad = ',.a-:()?!;'
toobad = ',.a-:(?!;'

for ll in sys.stdin:
    line = ll.decode('utf8').strip()
    if line.startswith('<s>'):
        sentence = line[3:].split(' ')
        clauses = []
        comma_idx = []
    elif line.startswith('<clausenum>'):
        try:
            clauses.append(map(int, line[11:].split(' ')))
        except ValueError:
            pass
    elif line.startswith('</s>'):
        for clause in clauses:
            i, j = clause[0] - 1, clause[-1] - 1
            if i != 0 and i+1 < len(sentence) and sentence[i] not in bad \
                                              and sentence[i+1] not in bad \
                                              and sentence[i-1] not in toobad:
                comma_idx.append(i)
            if j != 0 and j+1 < len(sentence) and sentence[j] not in bad \
                                              and sentence[j+1] not in bad \
                                              and sentence[j-1] not in bad:
                comma_idx.append(j+1)
        for i, word in enumerate(sentence):
            if i in comma_idx: sys.stdout.write(', ')
            sys.stdout.write(word.encode('utf8') + ' ')
        print
        print
