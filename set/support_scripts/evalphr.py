#!/usr/bin/python

#./evalphr.py test.phr gold.phr

import sys

def load_one_sentence(file):
   result = {'s': '', 'vp': set(), 'phr': set(), 'clause': set(), 'deps': set()}
   while True:
       line = file.readline()
       if line.startswith('<s>'):
           result['s'] = line.strip().split('>')[1]
       elif line.startswith('['):
           result['deps'].add(line.strip())
       elif line.startswith('<phrnum>'):
           phr = line.strip().split('): ')[1]
           if phr: result['phr'].add(phr)
       elif line.startswith('<vpnum>'):
           vp = line.strip().split('): ')[1]
           if vp: result['vp'].add(vp)
       elif line.startswith('<clausenum>'):
           clause = line.strip().split('>')[1]
           if clause: result['clause'].add(clause)
       elif line.startswith('</s>') or not line:
           return result

def print_stats(test, gold, good, label):
    prec = test and float(good)/test or 0
    rec = float(good)/gold
    print label
    print '-----------------'
    print 'test     :', test
    print 'gold     :', gold
    print 'correct  :', good
    print 'precision:', round(prec*100, 1)
    print 'recall   :', round(rec*100, 1)
    print 'F        :', prec+rec and round(200*prec*rec/(prec+rec), 1) or 0
    print


#main

testfile = open(sys.argv[1], 'r')
goldfile = open(sys.argv[2], 'r')

goldphr, testphr, corrphr = 0, 0, 0
goldvp, testvp, corrvp = 0, 0, 0
goldclause, testclause, corrclause = 0, 0, 0
golddeps, testdeps, corrdeps = 0, 0, 0

while True:
    test = load_one_sentence(testfile)
    gold = load_one_sentence(goldfile)
#    print test['s']
#    print gold['s']
#    print
    if not test['s'] or not gold['s']:
        print_stats(testphr, goldphr, corrphr, 'PHR')
        print_stats(testvp, goldvp, corrvp, 'VP')
        print_stats(testclause, goldclause, corrclause, 'CLAUSE')
        print_stats(testdeps, golddeps, corrdeps, 'DEPS')
        print_stats(testphr+testvp, goldphr+goldvp, corrphr+corrvp, 'PHR + VP')
        print_stats(testphr+testvp+testdeps, goldphr+goldvp+golddeps,
                    corrphr+corrvp+corrdeps, 'PHR + VP + DEPS')
        print_stats(testphr+testvp+testclause+testdeps,
                    goldphr+goldvp+goldclause+golddeps,
                    corrphr+corrvp+corrclause+corrdeps, 'OVERALL')
        exit(0)
    if not test['s'] or not gold['s']:
        sys.stderr.write('gold-test sentence mismatch: \n TEST: %s\nGOLD: %s'
                         % (test['s'], gold['s']))
    goldphr += len(gold['phr'])
    testphr += len(test['phr'])
    corrphr += len(gold['phr'].intersection(test['phr']))
    goldvp += len(gold['vp'])
    testvp += len(test['vp'])
    corrvp += len(gold['vp'].intersection(test['vp']))
    goldclause += len(gold['clause'])
    testclause += len(test['clause'])
    corrclause += len(gold['clause'].intersection(test['clause']))
    golddeps += len(gold['deps'])
    testdeps += len(test['deps'])
    corrdeps += len(gold['deps'].intersection(test['deps']))






