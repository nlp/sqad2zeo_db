#!/bin/bash

# to be ran in the set directory

SRCDIR=/home/xkovar3/sa/pdt/brief
TRGDIR=/home/xkovar3/sa/pdt/set_trees
NUM=100

for I in `seq $NUM`; do
  FILE=`printf "%05d" $I`;
  ./set.py -dc $SRCDIR/$FILE 2>/dev/null >$TRGDIR/$FILE;
  echo $FILE;
done
