#!/usr/bin/python

import sys

coords_file = open(sys.argv[1])

# read coords
coords = {}
for line in coords_file:
    w1, w2, f, s = line.strip().split('\t')
    coords[(w1, w2)] = (f, s)

# read stdin and subtract from coords
for line in sys.stdin:
    ww1, r, ww2, f, s, lcm = line.split('\t')
    w1 = ww1[:-2]
    w2 = ww2[:-2] # because of lempos
    freq = int(f)
    score = float(s)
    if freq > 3 and score > 4 and (w1, w2) in coords:
        del coords[(w1, w2)]

# write rest
for w1, w2 in coords:
    f, s = coords[(w1, w2)]
    print '%s\t%s\t%s\t%s' % (w1, w2, f, s)
