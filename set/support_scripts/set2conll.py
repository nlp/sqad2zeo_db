#!/usr/bin/python
import sys
'''Parser for merging an input and output of SET into one file in the conll format => 'word', 'lempos', 'tag', 'id', 'head', 'deprel'
	all xml tags are remained'''

words = []
num = []
term = []
head = []
deprel = []
with open(sys.argv[1], 'r') as f: #reads SET output and parses the text
	for line in f:                
		if len(line) != 1:
			words = line.split()		
			num.append(words[0])
			term.append(words[1])
			
			headNum = int(words[2]) +1
			head.append(headNum)
			
			if len(words) == 4:
				deprel.append('root')
			else:
				deprel.append(words[4])

strings = []
word = []
lemma = []
tag = []
xmlTags = []
with open(sys.argv[2], 'r') as ff: #reads the SET input and parses the text
	for line in ff:      		  
		if len(line) != 1:                   	
			if line[0].startswith('<') and not (line[1].startswith('<')):
					strings.append(line)
					xmlTags.append(line)
			else:					
					words = line.split()                        
					strings.append(words[0])
					word.append(words[0])
					lemma.append(words[1])
					tag.append(words[2])

counter = 0
with open(sys.argv[3], 'w') as out:    
    #out.write('{0:12} {1:12} {2:6} {3:8} {4:6} {5:8} \n'.format('word', 'lempos', 'tag', 'id', 'head', 'deprel'))        

	for x in range(len(strings)):					
		if strings[x].startswith('<') and not strings[x].startswith('<<'):
			out.write(strings[x])
			counter += 1
		elif strings[x] == term [x-counter]:
				lempos = ''								
				if ('k1' in tag[x-counter]) or (tag[x-counter].startswith('N')):
					lempos = lemma[x-counter] + '-n'
				elif ('k2' in tag[x-counter]) or (tag[x-counter].startswith('A')):
					lempos = lemma[x-counter] + '-j'
				elif ('k3' in tag[x-counter]) or (tag[x-counter].startswith('P')):
					lempos = lemma[x-counter] + '-p'
				elif ('k4' in tag[x-counter]) or (tag[x-counter].startswith('C')):
					lempos = lemma[x-counter] + '-m'
				elif ('k5' in tag[x-counter]) or (tag[x-counter].startswith('V')):
					lempos = lemma[x-counter] + '-v'
				elif ('k6' in tag[x-counter]) or (tag[x-counter].startswith('D')):
					lempos = lemma[x-counter] + '-a'
				elif ('k7' in tag[x-counter]) or (tag[x-counter].startswith('R')):
					lempos = lemma[x-counter] + '-s'
				elif ('k8' in tag[x-counter]) or (tag[x-counter].startswith('J')):
					lempos = lemma[x-counter] + '-c'
				elif ('k9' in tag[x-counter]) or (tag[x-counter].startswith('T')):
					lempos = lemma[x-counter] + '-r'
				elif ('k0' in tag[x-counter]) or (tag[x-counter].startswith('I')):
					lempos = lemma[x-counter] + '-t'
				elif tag[x-counter].startswith('kA'):
					lempos = lemma[x-counter] + '-y'
				elif ('kI' in tag[x-counter]) or (tag[x-counter].startswith('Z')):
					lempos = lemma[x-counter] + '-i'
				elif tag[x-counter].startswith('k?'):
					lempos = lemma[x-counter] + '-?'									
				else:
					lempos = lemma[x-counter] + '-X'
				out.write('%s\t%s\t%s\t%s\t%s\t%s \n' %(word[x-counter], lempos, tag[x-counter], num[x-counter], head[x-counter], deprel[x-counter]))
		else:
				print 'Ouch, there is an ERROR with the parsing!'
				break

if len(xmlTags) != counter:
        print 'ERROR! The number of xml tags is not right!'
else:
        print 'It seems that everything worked as good as wanted, check the file. \nThe lempos endings are derivated as following:'
        print 'k1: noun          -n'
        print 'k2: adjective     -j'
        print 'k3: pronoun       -p'
        print 'k4: numeral       -m'
        print 'k5: verb          -v'
        print 'k6: adverb        -a'
        print 'k7: preposition   -s'
        print 'k8: conjuction    -c'
        print 'k9: particle      -r'
        print 'k0: interjection  -t'
        print 'kA: acronym       -y'
        print 'kI: punctuation   -i'
        print 'k?:               -?'
        print 'not defined:      -X'
