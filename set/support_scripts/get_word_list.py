#!/usr/bin/env python

# pulls a list of most frequent lemmas from the czes corpus

import urllib, urllib2, base64, simplejson, sys, random

length = 1000

usr = 'xkovar3'
passwd = 'test'

wl_query = 'http://localhost/bonito2/run.cgi/wordlist?corpname=syn2k;wlattr=lemma;wlmaxitems=%s;wlsort=f;wlpage=1;format=json' % length

def get_result(url):
    # get information from server
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (usr, passwd))[:-1]
    request.add_header("Authorization", "Basic %s" % base64string)
    file = urllib2.urlopen(request)
    data = file.read()
    file.close()
    return data

def log(string):
    sys.stderr.write(string)
    sys.stderr.write('\n')

def get_list():
    wl_json = simplejson.loads(get_result(wl_query))
    return [item['str'] for item in wl_json['Items']]

if __name__ == '__main__':
    print '\n'.join(get_list())
