#!/usr/bin/env python

# csts to marx phrases

import sys, re
from set import segment, grammar

import pprint

i = 0

#def name(i):
#    return 'trees/train-1/' + '%05d' % i


def convert_to_marx(words, id_map):
    lines, tokens = [], []
#    g = grammar.Grammar('/nlp/projekty/set/set/cz_conll.set')
    g = grammar.Grammar('/nlp/projekty/set/set/grammar.set')
    for id, word, parent_id, lemma, tag, deplabel in words:
        line = { 'id': id_map[id],
                    'word': word,
                    'dep_id': id_map.get(parent_id, -1),
                    'dep_type': 'd',
                    'token': None,
                    'tag': tag, 
                    'deplabel': deplabel,
                 }
#        line = '\t'.join([id_map[id], word, id_map.get(parent_id, '-1'), 'd'])
        lines.append(line)
        tokens.append('\t'.join([word, lemma, tag]))
#    pprint.PrettyPrinter(indent=3).pprint(lines)
    seg = segment.Segment(tokens, pos_tags=True)
    for i, line in enumerate(lines):
        line['token'] = seg.tokens[i]
        if line['dep_id'] >= 0:
            seg.tokens[i].dependency = seg.tokens[line['dep_id']]
        else:
            seg.tokens[i].dependency = None
            seg.head = seg.tokens[i]
    tree = segment.Tree(lines)
#    seg.print_tree()
    for line in tree.get_marx_phrases(short=True, wc=g.wclass, from_dep=True):
        print line


words, id_map = [], {}
for vertline in sys.stdin:
    line = vertline.strip()
#    if i > 20: break
    if not line:
        if words: convert_to_marx(words, id_map)
        words = []
        id_map = {}
        i += 1
    else:
        fields = line.split('\t')
        word_i = int(fields[0]) - 1
        word = fields[1]
        lemma = fields[2]
        tag = fields[10]
#        if len(tag) == 1: tag = tag+tag
        try: id = fields[0]
        except: print line; exit(1)
        id_map[id] = word_i
        try: parent_id = fields[6]
        except: print line; exit(1)
        deplabel = fields[7]
        words.append((id, word, parent_id, lemma, tag, deplabel))

if words: convert_to_marx(words, id_map)

