#!/usr/bin/env python

# pulls a list of collocations for a lemma from the czes corpus

import urllib, urllib2, base64, simplejson, sys, random

length = 500

usr = 'xkovar3'
passwd = 'test'

collx_query_tmpl = 'http://localhost/bonito2/run.cgi/collx?q=alemma,"%s"&corpname=syn2k&cattr=lemma&cfromw=-3&ctow=3&cminfreq=10&cminbgr=10&cmaxitems=%s&cbgrfns=c&csortfn=l;format=json'

def get_result(url):
   # get information from server
   request = urllib2.Request(url)
   base64string = base64.encodestring('%s:%s' % (usr, passwd))[:-1]
   request.add_header("Authorization", "Basic %s" % base64string)
   file = urllib2.urlopen(request)
   data = file.read()
   file.close()
   return data

def log(string):
   sys.stderr.write(string)
   sys.stderr.write('\n')

def get_collx(lemma):
    collx_query = collx_query_tmpl % (lemma, length)
    collx_json = simplejson.loads(get_result(collx_query))
    return [item['str'] for item in collx_json['Items']]


if __name__ == '__main__':
   print '\n'.join(get_collx(sys.argv[1]))

