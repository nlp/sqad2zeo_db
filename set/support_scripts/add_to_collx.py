#!/usr/bin/env python

import sys

# adds collocations to *.dict and *.collx files.

coll_path = 'semclass-semclass'
dict_file = coll_path + '.dict'
collx_file = coll_path + '.collx'

if len(sys.argv) != 4:
    print 'Usage: %s lemma1 lemma2 weight' % sys.argv[0]
    exit(1)

lemma1 = sys.argv[1]
lemma2 = sys.argv[2]
weight = int(sys.argv[3])

words = {}
collocations = {}

# read dictionary
dictionary = open(dict_file)
number = 0
counter = 1
for item in dictionary:
    if counter == 1:
        temp_1 = item.strip()
        counter = 0
        number += 1
    else:
        temp_2 = int(item.strip())
        words[temp_1] = temp_2
        counter = 1
dictionary.close()
# read index
colloxfile = open(collx_file)
counter = 1
for i, item in enumerate(colloxfile):
    if counter == 1:
        temp_3 = tuple([int(num) for num in item.split()])
        counter = 0
    else:
        temp_2 = int(item.strip())
        collocations[temp_3] = temp_2
        counter = 1
colloxfile.close()
# add lemmas if needed and find indeces
print number
if lemma1 in words:
    index1 = words[lemma1]
else:
    number += 1
    words[lemma1] = number
    index1 = number
    print number, index1
if lemma2 in words:
    index2 = words[lemma2]
else:
    number += 1
    words[lemma2] = number
    index2 = number
    print number, index2
if index1 < index2: tt = (index1, index2)
else: tt = (index2, index1)
collocations[tt] = weight

#print words, collocations

#exit(1)
# writing dictionary into file
dictionary = open(dict_file, 'w')
for item in words:
    dictionary.write(item + '\n')
    dictionary.write(str(words[item])+'\n')
dictionary.close()

# writing collocation information
collox_file = open(collx_file, 'w')
for item in collocations :
    collox_file.write(str(item[0]) + ' ' + str(item[1]) +'\n')
    collox_file.write(str(collocations[item])+'\n')
collox_file.close()

