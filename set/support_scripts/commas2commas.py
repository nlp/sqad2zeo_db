#!/usr/bin/python

# converts preliminary --commas output into text with commas, according to
# external info, e.g. words in coordination
#
# usage: commas2comas.py <lemma_coord_filename>

import sys, re
from itertools import combinations

if len(sys.argv) < 2: coord_file = '/nlp/projekty/set/set/data/coord_lemmas'
else: coord_file = sys.argv[1]

db = {}

for line in open(coord_file):
    w1, w2, f, s = line.strip().split('\t')
    freq = int(f)
    score = float(s)
    if (w1, w2) in db or (w2, w1) in db:
        continue
    if freq > 0 and score > 0: #### change thresholds here
        db[(w1, w2)] = (freq, score)

r = re.compile('<comma ifincoord="([^"]*)" minfreq="([^"]*)" '
               'minscore="([^"]*)"[^/>]*/>')

for ll in sys.stdin:
    line = ll
    # process line
    while True:
        m = r.search(line)
        if not m:
            break
        toprocess = m.group()
        words = m.group(1).split()
        minfreq = int(m.group(2))
        minscore = float(m.group(3))
        indb, passed = True, True
        for w1, w2 in combinations(words, 2):
            if w1 == w2:
                continue
            if (w1, w2) in db:
                x = (w1, w2)
            elif (w2, w1) in db:
                x = (w2, w1)
            else:
                indb = False
                break
            if db[x][0] < minfreq or db[x][1] < minscore:
                passed = False
                break
        if indb and passed:
            line = line.replace(toprocess, ',', 1)
        else:
            line = line.replace(toprocess, '', 1)
    sys.stdout.write(line)
