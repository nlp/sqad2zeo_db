#!/usr/bin/python

import sys

emptyprint = True
dontprint = True
first = False
for line in sys.stdin:
    if line.startswith('</s>'):
        dontprint = True
    if line.startswith(('<s>', '<s ')):
        dontprint = False
        first = True
        if not emptyprint:
            sys.stdout.write('\n\n')
            emptyprint = True
    if line.startswith('<') or dontprint: continue
    if not first: sys.stdout.write(' ')
    
    sys.stdout.write(line.strip().split('\t')[0])
    emptyprint = False
    first = False
