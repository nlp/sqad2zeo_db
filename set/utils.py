#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# various utilities

import sys, os

class Logger:
    file = None
    verbose = False

    @staticmethod
    def log(string):
        if not Logger.file: return
        if isinstance(Logger.file, basestring):
            f = open(Logger.file, 'a')
            f.write(string + '\n')
            f.close()
        else:
            Logger.file.write(string + '\n')

def log(string):
    if Logger.verbose: Logger.log(string)

def usage(prog_name):
    path = os.path.dirname(prog_name)
    if path: path += '/'
    sys.stderr.write(open(path + 'README').read() + '\n')
