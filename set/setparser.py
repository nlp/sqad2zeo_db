#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# parser itself -- class Parser

import grammar, copy, re
from matcher import Match, Matcher, match_token
from grammar import Grammar
from segment import Segment, top_token
from settoken import SurfaceToken, PhrToken, LinkToken
from collocations import Collocations
from utils import log

from settings import COLL_BONUS, VALENCY_BONUS, SMALL_PENALTY, CROSS_PENALTY


BIG_INT = 1000


def is_dep_path(start, target):
        # helps to build an acyclic graph of dependencies
    if start == target: return True
    if start.dependency == target: return True
    curr_token = start
    i = 0
    while curr_token.dependency:
        i += 1
#        if i < 10: log(curr_token.str_word())
        curr_token = curr_token.dependency
        if curr_token == target: return True
        if i > 1000: raise RuntimeError('Cyklime...')
    return False


def is_dep_cross(segment, src_token, dep, cross):
    token = src_token
    while isinstance(token, PhrToken): token = token.head
    if not isinstance(dep, SurfaceToken) or not isinstance(cross, SurfaceToken):
        return False
    node_i = segment.tokens.index(token)
    dep_i = segment.tokens.index(dep)
    cross_i = segment.tokens.index(cross)
    if node_i < cross_i and dep_i > cross_i: return True
    if node_i > cross_i and dep_i < cross_i: return True
    return False


class Parser:

    lw_collx = None
    ll_collx = None
    lt_collx = None
    ls_collx = None
    ss_collx = None
        # for ranking functions
    join_tmpl  = { 'word not': set(['\xc4\x8di']),
                   'tag': [re.compile('k3.*y[RQ]|k8.*xS')],
                 }
    conj_tmpl  = { 'tag': [re.compile('k8')] }
    verb_tmpl  = { 'tag': [re.compile('k5.*m[IBRAP]')] }
    noun1_tmpl = { 'tag': [re.compile('k1.*c1')] }
    adj1_tmpl  = { 'tag': [re.compile('k2.*c1')] }
    comma_tmpl  = { 'word': [',', '-'] }


    def __init__(self, grammar, lw_collx_root='', ll_collx_root='',
                 lt_collx_root='', ls_collx_root='', ss_collx_root='',
                 flatten_coords=True):
        if not isinstance(grammar, Grammar):
            raise TypeError(
                         'ParserError: Parser init argument must be a Grammar')
        if lw_collx_root:
            lw_dict = lw_collx_root + '.dict'
            lw_file = lw_collx_root + '.collx'
            self.lw_collx = Collocations(lw_dict, lw_file)
        if ll_collx_root:
            ll_dict = ll_collx_root + '.dict'
            ll_file = ll_collx_root + '.collx'
            self.ll_collx = Collocations(ll_dict, ll_file)
        if lt_collx_root:
            lt_dict = lt_collx_root + '.dict'
            lt_file = lt_collx_root + '.collx'
            self.lt_collx = Collocations(lt_dict, lt_file)
        if ls_collx_root:
            ls_dict = ls_collx_root + '.dict'
            ls_file = ls_collx_root + '.collx'
            self.ls_collx = Collocations(ls_dict, ls_file)
        if ss_collx_root:
            ss_dict = ss_collx_root + '.dict'
            ss_file = ss_collx_root + '.collx'
            self.ss_collx = Collocations(ss_dict, ss_file)
        self.use_collx = self.lw_collx or self.ll_collx or self.lt_collx \
                         or self.ls_collx or self.ss_collx
        self.grammar = grammar
        self.flatten_coords = flatten_coords


    def get_collx_bonus(self, match, category=''):
        for i, t1 in enumerate(match.tokens):
            for t2 in match.tokens[i+1:]:
                if category == 'lw':
                    score = max( self.lw_collx.find_collocation (
                                                    t1.lemma, set([t2.word])),
                                 self.lw_collx.find_collocation (
                                                    set([t1.word]), t2.lemma) )
                elif category == 'ls':
                    score = max( self.ls_collx.find_collocation(t1.sem,
                                                                t2.lemma),
                                 self.ls_collx.find_collocation(t1.lemma,
                                                                t2.sem) )
                elif category == 'll':
                    score = self.ll_collx.find_collocation(t1.lemma, t2.lemma)
#                    print t1.lemma, t2.lemma, score
                elif category == 'ss':
                    score = self.ss_collx.find_collocation(t1.sem, t2.sem)
                elif category == 'lt':
                    pass # TODO (?)
                if score:
                    log('Collocations found (%s): %s, %s (%i)'
                                         % (category, t1.word, t2.word, score))
                    return COLL_BONUS * score
        return 1


    def parse(self, segment, maxlen=20):
            # method performing complex segment parsing -- adds relationships
            # to the segment
        log('Parsing segment: ' + segment.str_words())
        matches = self.find_matches(segment, maxlen)
        sorted_matches = self.sort_matches(segment, matches)
        self.make_matches(segment, sorted_matches, maxlen)
        self.finish_tree(segment)
        segment.grammar = self.grammar


    def find_matches(self, segment, maxlen=20):
        matches = []
        wanted_rules = self.grammar.coordinations + self.grammar.dependencies \
                                + self.grammar.relative_clauses
        for rule in wanted_rules:
            matcher = Matcher(rule, segment, maxlen=maxlen)
            matches.extend(matcher.get_matches())
        return matches


    def sort_matches(self, segment, matches):
        ranked_matches = []
        log('Ranking matches')
        for index, m in enumerate(matches):
            rank = 1.0 * m.prob / m.length # could be improved later
            if self.use_collx:
                rank *= self.get_collx_bonus(m, category='ls')
                rank *= self.get_collx_bonus(m, category='ss')
            ranked_matches.append((rank, - index, m))
            log('    %s\t%.1f' % (m.view(), rank))
        ranked_matches.sort(reverse=1)
        return ranked_matches


    def make_matches(self, segment, sorted_matches, maxlen=20):
        # TODO divide the long function
        while sorted_matches:
            new_sorted_matches = []
            for ii, (rank, index, m) in enumerate(sorted_matches): # m = match
                if not m.marked_tokens:
                    log('WARNING: no marked tokens: ' + m.view()); continue
                if m.rule in self.grammar.coordinations:
                    if m.imp_tokens and m.imp_tokens[0].dependency \
                            and m.imp_tokens[-1].dependency \
                            and not m.imp_tokens[0].phrasal \
                            and not m.imp_tokens[-1].phrasal:
                        continue # overriden by other bounds
                    cross_phrase = False; t1 = m.tokens[0]
                    for t in m.tokens[1:]: # check cross-phrase-ness
                        if t.under_phrase != t1.under_phrase:
                            if t1 == m.tokens[0] and t1.phrasal \
                                          and t1.dependency.children[-1] == t1:
                                t1 = t; continue
                            if t == m.tokens[-1] and t.phrasal \
                                             and t.dependency.children[0] == t:
                                continue
                            cross_phrase = True; break
                    conflict = False
                    for t in m.tokens:
                        if t.dependency and t.dependency in m.tokens:
                            conflict = True; break
                    if cross_phrase or conflict:
#                        log('  Match ommited: %s (rank %s)' % (m.view(), rank))
#                        log('         %s' % m.view_all())
#                        log('     Rule: ' + m.rule.view())
                        continue
                    conflict_tokens = [t for t in m.marked_tokens if t.phrasal]
                    if conflict_tokens and len(m.marked_tokens) == 1: continue
                    conflict = False; new_on_left = False; new_on_right = False
                    for c_token in conflict_tokens: # check chains
                        fcp_token = c_token.dependency.children[0]
                                        # first token in the conflict phrase
                        lcp_token = c_token.dependency.children[-1]
                                        # last token in the conflict phrase
                        if c_token == m.marked_tokens[-1] \
                                and fcp_token == c_token:
                            new_on_left = True
                        elif c_token == m.marked_tokens[0] \
                                and lcp_token == c_token:
                            new_on_right = True
                        else:
                            conflict = True
                    if conflict:
#                        log('  Match ommited: %s (rank %s)' % (m.view(), rank))
#                        log('         %s' % m.view_all())
#                        log('     Rule: ' + m.rule.view())
                        continue
                    dep_right, dep_left = None, None
                    if new_on_left or new_on_right:
                        if new_on_left: # update the match and del the old coord
                            if self.flatten_coords:
                                c_token = m.marked_tokens[-1]
                                m.marked_tokens.extend(
                                               c_token.dependency.children[1:])
                                m.head = c_token.dependency.head
                                dep_left = c_token.dependency.dependency
                                segment.del_phrasal_token(c_token.dependency)
                            else:
                                m.marked_tokens[-1] = c_token.dependency
                        if new_on_right:
                            if self.flatten_coords:
                                c_token = m.marked_tokens[0]
                                m.marked_tokens = \
                                             c_token.dependency.children[:-1] \
                                                            + m.marked_tokens
                                dep_right = c_token.dependency.dependency
                                segment.del_phrasal_token(c_token.dependency)
                            else:
                                m.marked_tokens[0] = c_token.dependency
                    else: # check for appropriate dependency if not chain
                        deps = [t.dependency for t in m.marked_tokens
                                                 if t.dependency]
                        dep_left, dep_right = (deps + [None, None])[:2]
                    log('  Match selected: %s (rank %s)' % (m.view(), rank))
#                    log('         %s' % m.view_all())
                    log('     Rule: ' + m.rule.view())
                    dep = dep_right or dep_left
                    if not dep and new_on_left and not new_on_right:
                        dep = m.marked_tokens[0].dependency
                    elif not dep and new_on_right and not new_on_left:
                        dep = m.marked_tokens[-1].dependency
                    if dep: # check for cycles
                        for t in m.marked_tokens:
                            if is_dep_path(dep, t): dep = None; break
                        if dep: m.deplabeldep = dep.children[0].deplabel
                    for t in m.marked_tokens: t.dependency = None
                    for t in m.marked_tokens: t.match = m
                    segment.add_phrasal_token(m.marked_tokens, m.label,
                           m.head, dep, under_phrase=copy.copy(t1.under_phrase),
                           deplabels=m.deplabels, deplabeldep=m.deplabeldep,
                           coord_tag=True)
                elif m.rule in self.grammar.relative_clauses:
                    mt_i1, mt_i2 = 0, len(m.marked_tokens)
                    conflict = False
                    for i, t in enumerate(m.marked_tokens):
                        if t.dependency:
                            if i == 0 and t == t.dependency.children[-1]:
                                mt_i1 = 1
                            elif i == mt_i2 and t == t.dependency.children[0]:
                                mt_i2 = -1
                            else:
                                conflict = True
                    m.marked_tokens = m.marked_tokens[mt_i1:mt_i2]
                    if conflict or not m.marked_tokens \
                                or (len(m.marked_tokens) == 1
                                    and m.marked_tokens[0].dependency):
                        continue
                    if m.dep: # check for cycles
                        for t in m.marked_tokens:
                            if is_dep_path(m.dep, t): m.dep = None; break
                    log('  Match selected: %s (rank %s)' % (m.view(), rank))
                    log('    Rule: ' + m.rule.view())
                    for t in m.marked_tokens: t.match = m
                    segment.add_phrasal_token(m.marked_tokens, m.label, m.head,
                                 all_tokens=m.all_tokens, deplabels=m.deplabels,
                                 make_invisible=True, dep=m.dep,
                                 i1=m.imp_first, i2=m.imp_last,
                                 deplabeldep=m.deplabeldep)
                    log('\n=============\n')
                    newms = [(0, 0, m)
                             for m in self.find_matches(segment, maxlen)]
                    new_sorted_matches = self.sort_matches(segment,
                              [sm[2] for sm in sorted_matches[ii+1:] + newms])
                    break
                else: # dependencies
                    if not m.marked_tokens or not m.dep: continue
                    token = m.marked_tokens[0]
                    dep = m.dep
                    # do not allow more dependencies on one preposition
                    wc = self.grammar.wclass
                    if 'prep' in wc and match_token(dep, wc['prep']) \
                                    and dep.children:
                        continue
                    while token.phrasal: token = token.dependency
                    if not token.dependency and not is_dep_path(dep, token):
                        log('  Match selected: %s (rank %s)' % (m.view(), rank))
                        log('     Rule: ' + m.rule.view())
                        for t in m.marked_tokens: t.match = m
                        segment.add_dependency(token, dep, m.prob, m.deplabels)
                        if token.sem and self.use_collx:
#                            print token.str_word(), '-->', dep.str_word()
#                            print [role for role in token.sem]
                            dep_cats = [tt[tt.find('k')+1] for tt in dep.tag
                                                           if tt.find('k') >= 0]
                            if not '5' in dep_cats: dep.sem.update(token.sem)
#                            print [role for role in dep.sem]
                            new_sorted_matches = self.sort_matches(segment,
                                       [sm[2] for sm in sorted_matches[ii+1:]])
                            break
            sorted_matches = new_sorted_matches


    def finish_tree(self, segment):
        # complete dependencies (somehow -- add top phrasal token)
        log('completing the tree ...')
        orphan_tokens = []
        for index, token in enumerate(segment.tokens):
            if not token.dependency: # orphan candidate
                added = False
                if token.under_phrase and token.str_word() != '<clause>':
                    for phr in token.under_phrase:
#                        if not phr in segment.tokens: print '++++', phr
                        if not is_dep_path(phr, token):
                            segment.add_dependency(token, phr, 'x')
                            token.phrasal = True # XXX kind of hack
                            log('  Added edge: %s -> %s' % (token.str_word(),
                                                            phr.str_word()))
                            added = True; break
                if not added:
                    orphan_tokens.append(token)
        clauses = [t for t in orphan_tokens if t.str_word() == '<clause>']
        while len(clauses) > 2 and not self.flatten_coords:
            clauses.sort(key=lambda t: t.surface_index)
            c1, c2 = clauses[:2]
            orphs = []
            for orph in orphan_tokens:
                osi = orph.surface_index
                if osi > c1.surface_index and osi < c2.surface_index:
                    orphs.append(orph)
            new_token = segment.add_phrasal_token([c1] + orphs + [c2],
                          '<sentence>', c1, deplabels=[self.grammar.toplabel])
            del orphan_tokens[orphan_tokens.index(c2)]
            orphan_tokens[orphan_tokens.index(c1)] = new_token
            clauses = [t for t in orphan_tokens if t.str_word() == '<sentence>'
                                                 or t.str_word() == '<clause>']
        segment.head = segment.add_phrasal_token(orphan_tokens, '<sentence>',
                                             self.select_head(orphan_tokens),
                                             deplabels=[self.grammar.toplabel])


    def select_head(self, tokens):
        if not tokens: return
        if len(tokens) == 1: return tokens[0]
        ranked_tokens = [(self.head_rank(token), token) for token in tokens]
        ranked_tokens.sort(key=lambda x: x[0], reverse=1)
        return ranked_tokens[0][1]


    def head_rank(self, token):
        result = 0
        wc = self.grammar.wclass
        if not token.dependency: result += 1000
        try:
            if match_token(token, wc['verb']): result +=  100
            if match_token(token, wc['noun']): result +=   50
            if match_token(token, wc['adj']) : result +=   20
        except KeyError:
            log('warning: "verb", "noun" and "adj" word classes are not set' \
                'in the grammar.')
        if token.phrasal: result -= 1000 # part of a phrase
        return result - token.dep_prob










