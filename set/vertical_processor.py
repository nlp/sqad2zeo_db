#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# reading vertical files, class VerticalProcessor

import re, sys
import settings

tag_re = re.compile('^<.*>')
s_re = re.compile('^</?s[> ]')
doc_re = re.compile('^</?doc[> ]')
s_re = re.compile('^</?s[> ]')
head_re = re.compile('^</?head[> ]')

class VerticalProcessor():

    def __init__(self, file, incl_heads=False):
        self.i = 0
        self.file = file
        self.incl_heads = incl_heads
        if type([]) == type(file): self.strings = True
        else: self.strings = False

    def get_sentence(self, out_file=sys.stdout):
        result = []
        tags = {}
        ii = 0
        if self.strings:
            if self.i < len(self.file): line = self.file[self.i]
            else: return [], '', {}
        else:
            line = self.file.readline()
        while line or (self.strings and self.i < len(self.file)):
            if len(result) > 1000000:
                raise RuntimeError('Error: Sentence too long.'
                                   'Stopping at position %d' % self.i)
            self.i += 1
            if (result and s_re.match(line)) or (self.incl_heads and result
                                                 and head_re.match(line)):
                return result, line, tags

            if tag_re.match(line):
                if not settings.IGNORETAGS:
                    if not ii in tags: tags[ii] = []
                    tags[ii].append(line)
                elif not settings.IGNOREDOCTAGS and doc_re.match(line): 
                    out_file.write(line)
            else:
                result.append(line)
                ii += 1

            if self.strings:
                if self.i < len(self.file): line = self.file[self.i]
                else: return result, '', tags
            else:
                line = self.file.readline()
        return result, '', tags

