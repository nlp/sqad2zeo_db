#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# module for drawing trees

from Tkinter import *
#from pyx import *

x_char_space = 7
y_space = 30
from_top = 30
from_left = 40

def display_tree(lines, w_width=1200, w_height=250):
    node_list = compute_tree(lines)
    draw_tree(node_list, w_width, w_height)
#    write_tree(node_list, w_width, w_height, 'tree.eps')


def draw_tree(node_list, w_width, w_height):
    w = Tk()
    w.wm_title('tree view')
        # init canvas with scrolling
    frame = Frame(w, bd=2, relief=SUNKEN)
    frame.grid_rowconfigure(0, weight=1)
    frame.grid_columnconfigure(0, weight=1)
    xscrollbar = Scrollbar(frame, orient=HORIZONTAL)
    xscrollbar.grid(row=1, column=0, sticky=E+W)
    yscrollbar = Scrollbar(frame)
    yscrollbar.grid(row=0, column=1, sticky=N+S)
    c = Canvas(frame, bd=0, width=w_width, height=w_height,
                  xscrollcommand=xscrollbar.set, yscrollcommand=yscrollbar.set)
    c.grid(row=0, column=0, sticky=N+S+E+W)
    xscrollbar.config(command=c.xview)
    yscrollbar.config(command=c.yview)
    frame.pack()
        # draw the tree on canvas
    curr_x = from_left
    for node in node_list:
        if node.label.startswith('<'):
            color = "blue"
        else:
            color = "black"
            c.create_text(node.x, 10, text=node.label, font=("sans", 10))
        c.create_text(node.x, node.y, text=node.label,
                                         font=("sans", 10, "bold"), fill=color)
        if node.dep_id > -1:
            if node.dep_id < 10000: dep = node_list[node.dep_id]
            else: dep = node_list[-1] # nasty hack
            if node.dep_type == 'p': color = "blue"
            else: color = "black"
            c.create_line(node.x, node.y - 5, dep.x, dep.y + 8, fill=color)
    c.config(scrollregion=c.bbox(ALL))
    mainloop()


def write_tree(node_list, w_width, w_height, filename):
    #meritko k prepoctu souradnic, jen
    scale = 25.0
    #velikost pisma
    fontsize = 4
    #snize pozice slov
    fontoff = 10
    #sirka car
    linewidth = 0.05

    text.set(mode="latex")
    text.preamble(r"\usepackage[utf8x]{inputenc}")
    text.preamble(r"\usepackage{color}")
    c = canvas.canvas()
    for node in node_list:
        if node.label.startswith('<'):
            clr = "Gray"
            node.label = '$<$' + node.label[1:-1] + '$>$'
        else:
            clr = "Black"
        font = r"\sffamily \bfseries \textcolor[named]{" + clr + "}{" \
                                                            + node.label + "}", 
        c.text (node.x / scale, -(node.y + 4) / scale, font
                                     [text.halign.center, text.size(fontsize)])
        if node.dep_id > -1:
            if node.dep_id < 10000: dep = node_list[node.dep_id]
            else: dep = node_list[-1] # nasty hack
            if node.dep_type == 'p': 
                clr = color.rgb.grey(color.rgb.green)
            else: 
                clr = color.rgb.black
            p = path.line(node.x / scale, -(node.y - fontoff) / scale, dep.x \
                                                 / scale, -(dep.y + 8) / scale)
            c.stroke(p, [clr, style.linewidth(linewidth)])

    c.writeEPSfile(filename)

def compute_tree(lines):
    # TODO divide the code
    nodes = []
    for line in lines:
        id, label, dep_id, dep_type = line.split('\t')
        nodes.append(Node(id, label, dep_id, dep_type))
    # x coordinations
    x = from_left
    for index, node in enumerate(nodes):
        node.x = x
        if not node.label.startswith('<'):
            shift = len(node.label.decode('utf8')) / 2.0
            if index + 1 < len(nodes):
                shift += len(nodes[index+1].label.decode('utf8')) / 2.0
            x += (shift + 1) * x_char_space
        # y coordinations
    nodes_from_bottom = []
    y = from_top
    next_level = [-1]
    next_next_level = []
    while next_level:
        for id in next_level:
            for node in nodes:
                if node.dep_id == id:
                    nodes_from_bottom.append(node)
                    node.y = y
                    next_next_level.append(node.id)
        next_level = next_next_level
        next_next_level = []
        y += y_space
    nodes_from_bottom.reverse()
        # x coordinations for phrasal tokens
    for phr_node in nodes_from_bottom:
        if not phr_node.label.startswith('<'): continue
        first = -1; last = -1
        for node in nodes:
            if node.dep_id == phr_node.id: # take into account
                last = node.x
                if first < 0: first = node.x
        if first > 0: phr_node.x = (last + first) / 2.0
        for node in nodes: # check if not violating another token
            if phr_node.y == node.y and not phr_node == node:
                dist = node.x - phr_node.x
                shift = len(node.label.decode('utf8')) / 2.0 \
                            + len(phr_node.label.decode('utf8')) / 2.0 \
                            - abs(dist)
                if shift > 0:
                    if dist > 0: phr_node.x -= shift
                    else: phr_node.x += shift
    return nodes


class Node: # represents a tree node in the graphical view

    def __init__(self, id, label, dep_id, dep_type):
        self.id = int(id)
        self.label = label
        self.dep_id = int(dep_id)
        self.dep_type = dep_type
        self.x = from_left
        self.y = from_top


if __name__ == '__main__':
    import sys
    hybrid_format = False
    lines = sys.stdin.readlines()
    for line in lines:
        if '<top>' in line.split(): hybrid_format = True; break
    if not hybrid_format:
        newlines = []
        for line in lines: newlines.append(line.replace('\t-1\t', '\t10000\t'))
        newlines.append('\t'.join(['10000', '[root]', '-1', 'd']))
        display_tree(newlines)
    else:
        display_tree(lines)

