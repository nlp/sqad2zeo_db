#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# the top-level code of the SET parser

import os, sys, getopt, signal, gc
import locale

import settings
from grammar import Grammar
from setparser import Parser
from segment import Segment
from vertical_processor import VerticalProcessor
from utils import log, usage

locale.setlocale(locale.LC_ALL, 'C')

version = '0.8'

if __name__ == '__main__':

    from utils import Logger
    Logger.file = sys.stderr

    graph_out = False
    dep_out = False
    phr_out = False
    struct_out = False
    relaxed_out = False
    laa_out = False
    collx_out = False
    pos_tags = False # XXX obsolete
    desamb_hacks = False
    use_collx = False
    phrases = False
    print_tree = False
    marx = False # export phrases for Marek Grac's ontology building
    marx_long = False
    vasek = False
    sconll = False
    commas = False
    verbframes = False
    maxlen = 0 # unlimited
    max_match_len = 20 # for efficiency
    phr_verbs_path = ''

    set_path = os.path.dirname(sys.argv[0])
    grammar = os.path.join(set_path, 'grammar.set')
    lw_collx = os.path.join(set_path, 'data', 'lemma-word')
    ll_collx = os.path.join(set_path, 'data', 'lemma-lemma')
    lt_collx = os.path.join(set_path, 'data', 'lemma-tag')
    ls_collx = os.path.join(set_path, 'data', 'lemma-semclass')
    ss_collx = os.path.join(set_path, 'data', 'semclass-semclass')
    vasek_strip = ['part', 'conj', 'prep']

    # parse the command line
    try:
        options, args = getopt.getopt(sys.argv[1:], 'gdpctrvbs',
                       ['version', 'laa', 'postags', 'cout', 'phrases', 'marx',
                        'marx-long', 'vasek', 'maxlen=', 'grammar=', 'desamb',
                        'preserve-xml-tags', 'preserve-doc-tags', 'phrverbs=',
                        'sconll', 'commas', 'verbframes', 'zuzka',
                        'short-phrases', 'long-phrases', 'np', 'max-match-len='])
    except:
        usage(sys.argv[0]); exit(1)

    for option, value in options:
        if option == '-g': graph_out = True
        elif option == '-t': print_tree = True
        elif option == '-d': dep_out = True
        elif option == '-r': relaxed_out = True
        elif option == '-b': relaxed_out = True
        elif option == '-p': phr_out = True
        elif option == '-s': struct_out = True
        elif option == '-c': use_collx = True
        elif option == '-v': Logger.verbose = True
        elif option == '--preserve-xml-tags': settings.IGNORETAGS = False
        elif option == '--preserve-doc-tags': settings.IGNOREDOCTAGS = False
        elif option == '--postags': pos_tags = True
        elif option == '--cout': collx_out = True
        elif option == '--phrases': phrases = True
        elif option == '--laa': laa_out = True
        elif option == '--marx': marx = True
        elif option == '--short-phrases': marx = True
        elif option == '--vasek': vasek = True
        elif option == '--zuzka': vasek = True
        elif option == '--np': vasek = True
        elif option == '--marx-long': marx_long = True
        elif option == '--long-phrases': marx_long = True
        elif option == '--desamb': desamb_hacks = True
        elif option == '--maxlen': maxlen = int(value)
        elif option == '--max-match-len': max_match_len = int(value)
        elif option == '--grammar': grammar = value
        elif option == '--phrverbs': phr_verbs_path = value
        elif option == '--sconll': sconll = True
        elif option == '--commas': commas = True
        elif option == '--verbframes':
            verbframes = True
            print 'PRED\tSUBJ\tACC_OBJ\tDAT_OBJ\tADV_MODIF\tPP1\tPP2'
        elif option == '--version': print 'SET version %s' % version

    if (not print_tree) and (dep_out or phr_out or collx_out or phrases
                             or relaxed_out or laa_out or marx or sconll
                             or commas or verbframes or struct_out):
        print_tree = False
    else:
        print_tree = True

    if dep_out and (laa_out or phr_out):
        log('Incompatible options: -d not compatible with -p and --laa');
        exit(3)
    if collx_out and (dep_out or laa_out or phr_out or struct_out):
        log('Incompatible options: --cout not compatible with -d, -p and --laa')
        exit(3)

    # open the input file
    if not args:
        file = sys.stdin
    else:
        filename = args[0]
        try:
            file = open(filename)
        except Exception, detail:
            log('Cannot open the file %s : %s' % (filename, str(detail)) )
            exit(2)
    # load phr_verbs if set
    phr_verbs = {}
    if phr_verbs_path:
        for line in open(phr_verbs_path):
            llist = line.split()
            if llist[0] not in phr_verbs: phr_verbs[llist[0]] = []
            phr_verbs[llist[0]].append(tuple(llist[1:]))
    # get separate sentences from vertical processor
    vert_processor = VerticalProcessor(file)
    lines, end_tag, tags = vert_processor.get_sentence()
    # init structures
    g = Grammar(grammar)
    if use_collx:
        p = Parser(g, lw_collx_root=lw_collx, ll_collx_root=ll_collx,
                   lt_collx_root=lt_collx, ls_collx_root=ls_collx,
                   ss_collx_root=ss_collx, flatten_coords=not struct_out)
    else:
        p = Parser(g, flatten_coords=not struct_out)

    def sigusr1_handler(*args):
        open('/tmp/set-sigusr1-last-sentence', 'w').write(''.join(lines))
        sys.exit(1)

    i = 0
    while lines:
        i += 1
        signal.signal(signal.SIGUSR1, sigusr1_handler)
        # load file with sentence ...
#        print '###', i, len(lines)
#        sys.stdout.flush()
        if maxlen and len(lines) > maxlen:
            lines, end_tag, tags = vert_processor.get_sentence()
            continue
        s = Segment(lines, pos_tags=pos_tags, desamb_hacks=desamb_hacks,
                    phr_verbs=phr_verbs)
        p.parse(s, maxlen=max_match_len)

        if i > 1 and not verbframes: print # XXX
        if dep_out: s.print_dep_tree(root=False)
        if laa_out: s.print_laa_tree()
        if phr_out: s.print_phr_tree()
        if struct_out: s.print_phr_tree(struct=True)
        if relaxed_out: s.print_relaxed_tree()
        if collx_out: s.print_collx_out()
        if print_tree: s.print_tree(tags=tags)
        if phrases: s.print_phrases()
        if marx: s.print_marx_phrases()
        if sconll: s.print_sconll()
        if commas: s.print_commas()
        if verbframes: s.print_verb_frames()
        if vasek: s.print_marx_phrases(phr_strip=vasek_strip)
        if marx_long: s.print_marx_phrases(short=False)

        if graph_out:
            #from tree_view import display_tree
            from TreeViewer.TreeViewer import initGUI
            if dep_out: initGUI([s.get_dep_tree(root=True)])
            elif phr_out or laa_out: initGUI([s.get_phr_tree()])
            elif struct_out: initGUI([s.get_phr_tree(struct=True, graph=True)])
            elif relaxed_out: initGUI([s.get_relaxed_tree()])
            else: initGUI([s.get_tree()])

        if end_tag and not settings.IGNORETAGS: sys.stdout.write(end_tag)
#        print '### loading next ...'
#        sys.stdout.flush()
        lines, end_tag, tags = vert_processor.get_sentence()
        gc.collect()

#    log('no more lines: ' + str(lines))

    file.close()
