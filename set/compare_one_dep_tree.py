#!/usr/bin/env python

# computes precision/recall (or differences) of the two dependency tree files


def get_dep_score(list1, list2):
    all = 0
    good = 0
    for index, line1 in enumerate(list1):
        line2 = list2[index]
        if line1 and line2:
            dep1 = line1.split()[2]
            dep2 = line2.split()[2]
            all += 1
            if dep1 == dep2: good += 1
    return 100.0 * good / all



if __name__ == '__main__':
    import sys

    name1 = sys.argv[1]
    name2 = sys.argv[2]

    f1 = open(name1)
    f2 = open(name2)
    list1 = f1.readlines()
    list2 = f2.readlines()
    f1.close()
    f2.close()

    print get_dep_score(list1, list2)
