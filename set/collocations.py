#!/usr/bin/env python

# Copyright 2009 Vojtech Kovar, Petr Vronsky

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.


# module for finding collocations

class Collocations:

#    words = {}
#    collocations = {}
    list1 = []

    def __init__ (self, dictionary_file, collocation_file):
        self.words = {}
        self.collocations = {}
        counter = 1
        dictionary = open(dictionary_file)
        # loading the dictionary (Word: ID)
        number = 0
        for item in dictionary:
            if counter == 1:
                temp_1 = item.strip()
                counter = 0
                number += 1
            else:
                temp_2 = int(item.strip())
                self.words[temp_1] = temp_2
                counter = 1
        dictionary.close()

        colloxfile = open(collocation_file)
        counter = 1
#        self.list1 = [set() for i in range(number)]
#        print '------'
        # obtain dictionary {(ID, ID): number} from the collocation file
        for i, item in enumerate(colloxfile):
            if counter == 1:
#                neco = item.split(' ')
#                index, value = int(neco[0]), int(neco[1])
#                self.list1[index].add(value)
                temp_3 = tuple([int(num) for num in item.split()])
                counter = 0
            else:
                # value (1 or 0), in future could contain probability of collocation
                temp_2 = int(item.strip())
                self.collocations[temp_3] = temp_2
                counter = 1
        colloxfile.close()


    def print_list1(self, filename):
#        print list1
#        return
        f = open(filename, 'w')
        f.write('list1 = ')
        f.write(repr(self.list1))
        f.close()


    def find_collocation(self, lemma_set1, lemma_set2):
        result = -9999
        for lemma1 in lemma_set1:
            for lemma2 in lemma_set2:
                id_1, id_2 = 0, 0 # id for lemma1 and lemma2
                if lemma1 in self.words and lemma2 in self.words: # known words
                    id_1 = self.words[lemma1]
                    id_2 = self.words[lemma2]
                else:
                    continue
                if id_1 <= id_2: search_key = (id_1, id_2)
                else: search_key = (id_2, id_1)
                found_value = self.collocations.get(search_key, 0)
                if found_value > result: result = found_value
#        print lemma1, lemma2, result
        if result == -9999: return 0
        return result


if __name__ == '__main__':
    import sys
    c = Collocations('data/collocations.dict', 'data/collocations.collx')
    sys.stdout.write ('list1 = ')
    c.print_list1(c.list1)

