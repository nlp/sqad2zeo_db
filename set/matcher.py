#!/usr/bin/env python

# Copyright 2008 Vojtech Kovar

# This file is part of SET.
#
# SET is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SET is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SET.  If not, see <http://www.gnu.org/licenses/>.



# matching rules -- classes Match and Matcher

import grammar, copy
from settoken import Token, SurfaceToken, PhrToken
from utils import log

bound_words = set([',', 'a', '.'])


def match_token(token, tmpl_token, segment=None, token_i=-1, match_phr=False):
        #if match_phr is set in a bad place, it may cause cycling
    if not (token and tmpl_token): return False
    if tmpl_token.get('spec') == 'rbound' and segment != None:
        if token_i >= 0: i = token_i
        else: i = segment.tokens.index(token)
        if token.str_word() in bound_words:
            return 1
        # TODO this expects phrasal tokens to be in the end of the segment
        if isinstance(token, PhrToken):
            return 3
        if len(segment.tokens) == i \
                or isinstance(segment.tokens[i], PhrToken):
            return 2
        else:
            return 0
    if isinstance(token, PhrToken) and not match_phr: return 0
    for key, value in tmpl_token.items():
        if key == 'word' and token.word not in value: return 0
        if key == 'word not' and token.word in value: return 0
        if key == 'lemma' and not token.lemma.intersection(value): return 0
        if key == 'lemma not' and token.lemma.intersection(value): return 0
        if key == 'tag':
            success = False
            for tag_re in value:
                for tt in token.tag:
                    if tag_re.match(tt): success = True; break
            if not success: return 0
        if key == 'tag not':
            success = False
            for tag_re in value:
                for tt in token.tag:
                    if tag_re.match(tt): success = True; break
            if success: return 0
    return 1


class Match: # represents tokens matching a rule

    def __init__(self, segment, tokens, index_first=-1, index_last=-1,
                    marked_tokens=[], prob=100, rule=None, head=None, dep=None,
                    label='', imp_tokens=[], bound=False, rbound=False):
        self.segment = segment
        self.tokens = tokens
        self.prob = prob
        self.rule = rule
        self.head = head # head token
        self.dep = dep
        self.label = label
        self.marked_tokens = marked_tokens
        self.bound = bound
        self.rbound = rbound
        self.deplabels = self.rule.actions.get('LABEL', [''])
        self.deplabel = ', '.join(self.deplabels)
        self.deplabeldeps = self.rule.actions.get('LABELDEP', [''])
        self.deplabeldep = ' | '.join(self.deplabeldeps)
        if self.rule and self.rule.template[-1].get('spec', '') == 'rbound' \
                   and self.tokens and not self.tokens[-1].word in bound_words:
            self.rbound = True
        if imp_tokens: self.imp_tokens = imp_tokens
        else: self.imp_tokens = marked_tokens
        if index_first >= 0: self.index_first = index_first
        else: self.index_first = segment.tokens.index(tokens[0])
        if index_last >= 0: self.index_last = index_last
        else: self.index_last = segment.tokens.index(tokens[-1])
        self.length = self.index_last - self.index_first or 1
        if self.imp_tokens:
            if self.bound and '0' in self.rule.actions.get('MARK', []):
                self.imp_first = self.index_first
            else:
                self.imp_first = segment.tokens.index(self.imp_tokens[0])
            if self.rbound and str(len(self.rule.template) - 1) \
                                     in self.rule.actions.get('MARK', []):
                self.imp_last = self.index_last
            else:
                self.imp_last = segment.tokens.index(self.imp_tokens[-1])
            self.imp_length = self.imp_last - self.imp_first
        else:
            self.imp_length = 0
            self.imp_first = 0
            self.imp_last = 0
        self.all_tokens = self.segment.tokens[
                                          self.index_first : self.index_last+1]
        self.all_imp_tokens = self.segment.tokens[
                                                self.imp_first : self.imp_last]
        self.gap = self.compute_gap()


    def get_index_first(self):
        self.index_first = self.segment.tokens.index(self.all_tokens[0])
        return self.index_first


    def get_index_last(self):
        self.index_last = self.segment.tokens.index(self.all_tokens[-1])
        return self.index_last


    def compute_gap(self):
        if self.marked_tokens:
            if self.dep: dep_i = self.segment.tokens.index(self.dep)
            first_mark = self.segment.tokens.index(self.marked_tokens[0])
            last_mark = self.segment.tokens.index(self.marked_tokens[-1])
            if self.dep and dep_i < first_mark: return first_mark - dep_i
            elif self.dep and dep_i > last_mark: return dep_i - last_mark
            else: return first_mark - self.index_first
        else:
            return 0


    def get_all_words(self):
        index_first = self.get_index_first()
        index_last = self.get_index_last()
        return self.segment.get_words()[index_first:index_last+1]


    def get_all_tokens(self):
        return self.all_tokens


    def view(self):
        app = self.deplabel and '  (%s)' % self.deplabel or ''
        return ' '.join([ tok.str_word() for tok in self.tokens ]) + app


    def view_all(self):
        app = self.deplabel and '  (%s)' % self.deplabel or ''
        return ' '.join([ tok.str_word() for tok in self.all_tokens ]) + app


    def is_imp_closed(self):
        # true if deps of all imp. tokens do not go out of the imp. tokens
        for token in self.all_imp_tokens:
            t = token
            while t:
                if not t in self.all_imp_tokens and not isinstance(t, PhrToken):
                    return False
                t = t.dependency
        return True


class Matcher: # matching complex rules

    def __init__(self, rule, segment, skip_matched=False, maxlen=20):
        self.rule = rule
        self.segment = segment
        self.matches = []
        self.skip_matched = skip_matched
        self.maxlen = maxlen
            # determines if we want to skip previously matched tokens
            # (e.g. as relclauses...)


    def get_matches(self):
        self.run_match()
        return self.matches


    def run_match(self):
        # TODO divide the code, it is too long
        self.matches = []
        tmpl = self.rule.template
        tokens = self.segment.tokens
        for i in range(len(tokens)): # trying to match from 'i'-th token
            tmpl_i = 0; token_i = i;
            rend = -1; lend = -1; bound=False; rbound=False
            found_tokens = []
            checkpoints = [] # because of matching '...'
            checkpoints_i = -1

            while checkpoints_i < len(checkpoints):
                if checkpoints_i >= 0:
                    tmpl_i, token_i, found_tokens, lend, rend, bound, rbound =\
                                                     checkpoints[checkpoints_i]
                checkpoints_i += 1

                while True:
                    finish = False
                    if token_i - i > self.maxlen: # ignore too long matches
                        break
                    if token_i >= len(tokens) and not tmpl_i >= len(tmpl):
                        finish = True
                        for tmpl_token in tmpl[tmpl_i:]:
                            if not tmpl_token.get('spec'): finish = False
                        if finish:
                            rend = len(tokens) - 1
                    if tmpl_i >= len(tmpl) or finish: # match found
                        match = self.make_actions(dict(found_tokens), lend,
                                                           rend, bound, rbound)
                        if match:
                            self.matches.append(match)
                            log('  Match found: '+ match.view())
                            log('    Rule: ' + self.rule.view())
                        break
                    if token_i >= len(tokens): break
                    token = tokens[token_i]
                    tmpl_token = tmpl[tmpl_i]
                    if (self.skip_matched and token.dependency) \
                                                      or token.invisible:
                        token_i += 1
                        continue
                        # handle special tmpl_tokens
                    spec = tmpl_token.get('spec')
                    if spec == 'bound':
                        m = self.bound_match(token_i)
                        if not m:
                            break
                        elif isinstance(m, Token):
                            found_tokens.append((tmpl_i, m))
                        else:
                            lend = token_i; bound=True
                        tmpl_i += 1
                    elif spec == 'start':
                        if token_i != 0: break
                        lend = token_i; bound = True; tmpl_i += 1
                    elif spec == '...':
                        m_result = match_token(token, tmpl[tmpl_i + 1],
                                                         self.segment, token_i)
                        if m_result:
                            if m_result < 2:
                                if match_token(token, tmpl_token):
                                    checkpoints.append( (tmpl_i, token_i + 1,
                                                    copy.copy(found_tokens),
                                                    lend, rend, bound, rbound))
                                found_tokens.append((tmpl_i + 1, token))
                            elif m_result == 2:
                                rend = token_i; rbound = True
                            elif m_result == 3:
                                rend = token_i - 1; rbound = True;
                            tmpl_i += 2; token_i += 1
                        else:
                            if match_token(token, tmpl_token): token_i += 1
                            else: break
                    else: # ordinary tmpl_token or rbound
                        m_result = match_token(token, tmpl_token, self.segment,
                                                                       token_i)
                        if m_result:
                            if m_result == 1:
                                found_tokens.append((tmpl_i, token))
                            elif m_result == 2:
                                rend = token_i; rbound = True
                            elif m_result == 3:
                                rend = token_i - 1; rbound = True
                            tmpl_i += 1; token_i += 1
                        else:
                            break


    def bound_match(self, i):
        if i == 0: return True
        cand_token = self.segment.tokens[i-1]
        if cand_token.str_word() in bound_words: return cand_token
        return False


    def match_fields(self, field, tok_value, tmpl_value):
        if field == 'tag':
            for tag in tok_value:
                if tmpl_value.match(tag): return True
        elif field == 'lemma':
            if tmpl_value in tok_value: return True
        elif field == 'word':
            if tok_value == tmpl_value: return True
        return False


    def make_actions(self, token_index, lend, rend, bound, rbound):
        # returns Match objects list (in case of success) or None
        prob = self.get_prob()
        if not self.check_agree(token_index): return None
        # check sisters, if any, ...
        if not self.rule.sisters: sisters_break = False
        for i, j, field1, field2, probs in self.rule.sisters:
            sisters_break = True
            tok1 = getattr(token_index[i], field1)
            tok2 = getattr(token_index[j], field2)
            tmpl1 = self.rule.template[i].get(field1, [])
            tmpl2 = self.rule.template[j].get(field2, [])
            for index, value in enumerate(tmpl1):
                if self.match_fields(field1, tok1, value):
                    if self.match_fields(field2, tok2, tmpl2[index]):
                        prob = prob * probs[index] / 100
                        sisters_break = False
                        break
        if sisters_break: return None
            # ... fix bound and rbound (check if they are marked), ...
        if not '0' in self.rule.actions.get('MARK', []): new_bound = False
        else: new_bound = bound
        if not str(len(self.rule.template) - 1) \
                                          in self.rule.actions.get('MARK', []):
            new_rbound = False
        else:
            new_rbound = rbound
            # ... and complete the actions
        try:
            marked_tokens, label = self.get_marks(token_index)
            head = self.get_head(token_index, marked_tokens)
            dep = self.get_dep(token_index)
            imp_tokens = self.get_imp_tokens(token_index)
        except KeyError as parameter:
            raise RuntimeError('GrammarError: Index %s not found in: %s'
                                               % (parameter, self.rule.view()))
        match = Match( self.segment, list(token_index.values()), lend, rend,
                       marked_tokens=marked_tokens, prob=prob, rule=self.rule,
                       head=head, dep=dep, label=label, imp_tokens=imp_tokens,
                       bound=new_bound, rbound=new_rbound )
        match.rel_tokens = token_index
        return match


    def get_prob(self):
        args = self.rule.actions.get('PROB')
        if args: return int(args[0])
        else: return grammar.DEFAULT_PROB


    def get_dep(self, token_index):
        args = self.rule.actions.get('DEP')
        if args: return token_index[int(args[0])]
        else: return None


    def get_head(self, token_index, marked_tokens):
        args = self.rule.actions.get('HEAD')
        if not args:
            return None
        return token_index[int(args[0])]


    def get_imp_tokens(self, token_index):
        imp_tokens = []
        args = self.rule.actions.get('IMPORTANT', [])
        for arg in args:
            try: int_arg = int(arg)
            except: continue
            token = token_index.get(int_arg) # does not have to be here (bound)
            if token: imp_tokens.append(token)
        return imp_tokens


    def get_marks(self, token_index):
        marked_tokens = []; label = ''
        args = self.rule.actions.get('MARK', [])
        for arg in args:
            try:
                int_arg = int(arg)
                token = token_index.get(int_arg)
                                            # does not have to be here (bound)
                if token: marked_tokens.append(token)
            except ValueError:
                label = arg
        return marked_tokens, label


    def check_agree(self, token_index):
        args = self.rule.actions.get('AGREE', [])
        index = 0
        while index < len(args):
            arg1, arg2, categories = args[index : index+3]
            tok1 = token_index[int(arg1)]
            tok2 = token_index[int(arg2)]
            for char in categories:
                try:
                    i = int(char);
                    value1 = set([tt[i] for tt in tok1.tag])
                    value2 = set([tt[i] for tt in tok2.tag])
                except ValueError:
                    value1, value2 = set(), set()
                    for tt in tok1.tag:
                        for tpart in tt.split(char)[1:]:
                            value1.add(tpart[0])
                    for tt in tok2.tag:
                        for tpart in tt.split(char)[1:]:
                            value2.add(tpart[0])
                if not value1 or not value2: continue # !!! problematic
                if value1.intersection('.X'): continue
                if value2.intersection('.X'): continue
                if not value1.intersection(value2):
                    return False
            index += 3
        return True


