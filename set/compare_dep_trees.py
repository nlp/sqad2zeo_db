#!/usr/bin/env python

# compares two lists of trees and creates a test file

import os, sys
from compare_one_dep_tree import get_dep_score

# settings ###################################################################

#dir1 = '/home/xkovar3/sa/data/pdt2/trees/train-1'
#dir2 = '/home/xkovar3/sa/data/pdt2/set_trees/train-1'

dir1 = '/home/xkovar3/sa/data/pdt2/trees/etest'
dir2 = '/home/xkovar3/sa/data/pdt2/set_trees/etest'

#number = 100
number = 10148

not_sentences = False # if True, not sentences are counted, only dependencies
#not_sentences = True

# end settings ###############################################################

def pdt_name(i):
   return os.path.join(dir1, '%05d' % i)

def set_name(i):
   return os.path.join(dir2, '%05d' % i)



if __name__ == '__main__':
    i = 0
    global1 = []
    global2 = []
    while i < number:
        i += 1
        name1 = pdt_name(i); name2 = set_name(i)
        f1 = open(name1); f2 = open(name2)
        list1 = []; list2 = []
        for line in f1:
            if len(line.split('\t')) == 4: list1.append(line)
        for line in f2:
            if len(line.split('\t')) == 4: list2.append(line)
        f1.close(); f2.close()
        if len(list1) == len(list2):
            if not_sentences: global1.extend(list1); global2.extend(list2)
            else: print '%05d' % i + '\t' + str(get_dep_score(list1, list2))
        else: sys.stderr.write('warning: skipped sentence No. %d\n' % i)

if not_sentences: print get_dep_score(global1, global2)

