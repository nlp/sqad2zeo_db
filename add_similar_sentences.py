#!/usr/bin/env python3
# coding: utf-8
# Created by Marek Medveď - xmedved1@fi.muni.cz
from sqad_db import SqadDb
from sqad_db import id2word
from query_database import get_content
import sys
import numpy as np
from scipy import spatial
import pickle
from os import path
import os
import re
import math
import persistent.list
import transaction
import traceback
dir_path = os.path.dirname(os.path.realpath(__file__))


def precompute_tfidf(kb, vocabulary):
    """
    Copute term frequency per document and document frequency per word
    :param kb: ZODB object
    :param vocabulary: persistent list
    :return: number_of_documents, idf_per_doc, tf_per_doc
    """
    number_of_documents = 0
    idf_per_doc = {}
    tf_per_doc = {}

    for url, text in kb.url2doc.items():
        number_of_documents += 1
        number_of_words_per_document = 0
        for sentence in text['text']:
            for word_id in sentence['sent']:
                number_of_words_per_document += 1
                word_lemma = id2word(vocabulary, word_id)['lemma']

                if idf_per_doc.get(url):
                    idf_per_doc[url].add(word_lemma)
                else:
                    idf_per_doc[url] = {word_lemma}

                if tf_per_doc.get(url):
                    if tf_per_doc[url].get(word_lemma):
                        tf_per_doc[url][word_lemma] += 1
                    else:
                        tf_per_doc[url][word_lemma] = 1
                else:
                    tf_per_doc[url] = {word_lemma: 1}
        for word_lemma in tf_per_doc[url].keys():
            tf_per_doc[url][word_lemma] = float(tf_per_doc[url][word_lemma]) / number_of_words_per_document

    return number_of_documents, idf_per_doc, tf_per_doc


def compute_tfidf(kb, vocabulary, verbose=False):
    """
    COpute final TF-IDF according to https://cs.wikipedia.org/wiki/Tf-idf
    :param kb: ZODB object
    :param vocabulary: persistent list
    :param verbose: bool
    :return: dict, final tf-idf
    """
    tf_idf = {}
    number_of_documents, idf_per_doc, tf_per_doc = precompute_tfidf(kb, vocabulary)

    idf = {}
    for url, value in idf_per_doc.items():
        for w in value:
            if idf.get(w):
                idf[w] += 1
            else:
                idf[w] = 1

    for url, value in tf_per_doc.items():
        for word_lemma, word_tf in value.items():
            if tf_idf.get(url):
                if tf_idf.get(url).get(word_lemma):
                    print('Error: multiple tf-idf')
                    sys.exit()
                else:
                    tf_idf[url][word_lemma] = word_tf * math.log(float(number_of_documents) / idf[word_lemma])
            else:
                tf_idf[url] = {word_lemma: word_tf * math.log(float(number_of_documents) / idf[word_lemma])}

    if verbose:
        # Check TF-IDF score manually
        for url, value in tf_per_doc.items():
            print(f'{url}')
            for w, c in value.items():
                print(f'{w}\t: tf:{c}, idf: {idf[w]}, tf-idf: {tf_idf[url][w]}')
        sys.exit()

    return tf_idf


def compute_tfidf_per_doc(kb, vocabulary):
    tf_idf_per_doc = {}

    for url, text in kb.url2doc.items():
        term_frequency = {}
        idf = {}
        word_per_sentece = {}
        number_of_words_per_document = 0
        number_of_senteces = 0
        for idx, sentence in enumerate(text['text']):
            number_of_senteces += 1
            for word_id in sentence['sent']:
                number_of_words_per_document += 1
                word_lemma = id2word(vocabulary, word_id)['lemma']

                if word_per_sentece.get(idx):
                    word_per_sentece[idx].add(word_lemma)
                else:
                    word_per_sentece[idx] = {word_lemma}

                if term_frequency.get(word_lemma):
                    term_frequency[word_lemma] += 1
                else:
                    term_frequency[word_lemma] = 1

        for idx, value in word_per_sentece.items():
            for w in value:
                if idf.get(w):
                    idf[w] += 1
                else:
                    idf[w] = 1

        for word_lemma, word_tf in term_frequency.items():
            if tf_idf_per_doc.get(url):
                if tf_idf_per_doc.get(url).get(word_lemma):
                    print('Error: multiple tf-idf')
                    sys.exit()
                else:
                    tf_idf_per_doc[url][word_lemma] = (float(word_tf) / number_of_words_per_document) * math.log(float(number_of_senteces) / idf[word_lemma])
            else:
                tf_idf_per_doc[url] = {word_lemma: (float(word_tf) / number_of_words_per_document) * math.log(float(number_of_senteces) / idf[word_lemma])}

    return tf_idf_per_doc


def get_n_grams(token, length):
    """
    Separate each word to n-gram characters
    :param token: str
    :param length: int, length of n-gram
    :return:
    """
    all_bigrams = set()
    word = f'-{token}-'
    for i in range(len(word)):
        if i+length < len(word):
            all_bigrams.add(word[i:i+length])
    return all_bigrams


def find_similar_senteces(vocabulary, kb, tf_idf_full_sqad, tf_idf_per_doc, record, length, export_tex):
    """
    Searching for sentences containing the exact answer
    :param db: ZODB database
    :param tf_idf_full_sqad:
    :param tf_idf_per_doc:
    :param length: int, length of Jaccard n-gram
    :param record: record object
    :param export_tex: bool, write tex tables
    :return: list of indexes with sentences within the text containing exact answer
    """
    similar_senteces = []

    try:
        for answer_selection_sent in get_content(record.answer_selection, vocabulary, old=False):
            # Answer selection vector enhanced by TF-IDF
            as_tf_idf_full_sqad_vec = []
            answer_selection_fasttext_vec = []
            answer_selection_content_full_sqad = []
            answer_selection_content_per_doc = []
            answer_selection_content_fastext = []
            as_tf_idf_per_doc_vec = []

            answer_selection_jaccard = set()

            answer_selection_content_full_sqad_sent = []
            answer_selection_content_per_doc_sent = []
            answer_selection_content_jaccard = []

            for token in answer_selection_sent['sent']:
                answer_selection_word_tf_idf = tf_idf_full_sqad[record.text][token['lemma']]
                as_tf_idf_full_sqad_vec.append([y * answer_selection_word_tf_idf for y in token['v300']])
                as_per_doc_tf_idf = tf_idf_per_doc[record.text][token['lemma']]
                as_tf_idf_per_doc_vec.append([y * as_per_doc_tf_idf for y in token['v300']])
                answer_selection_fasttext_vec.append(token['v300'])
                answer_selection_jaccard = answer_selection_jaccard.union(get_n_grams(token['lemma'], length))

                if export_tex:
                    answer_selection_content_full_sqad.append((token["word"], answer_selection_word_tf_idf))
                    answer_selection_content_per_doc.append((token["word"], as_per_doc_tf_idf))
                    answer_selection_content_fastext.append(f'{token["word"]}')
                    answer_selection_content_jaccard.append(f'{token["word"]}')

            if export_tex:
                # Focus word in full SQAD
                focus_full_sqad = max(answer_selection_content_full_sqad, key=lambda item: item[1])[0]
                for word, score in answer_selection_content_full_sqad:
                    if word == focus_full_sqad:
                        answer_selection_content_full_sqad_sent.append(f'{{\color{{red}}{word}}}$_{{\color{{gray}}{score:.3f}}}$')
                    else:
                        answer_selection_content_full_sqad_sent.append(f'{word}$_{{\color{{gray}}{score:.3f}}}$')

                # Focus word per doc
                focus_per_doc = max(answer_selection_content_per_doc, key=lambda item: item[1])[0]
                for word, score in answer_selection_content_per_doc:
                    if word == focus_per_doc:
                        answer_selection_content_per_doc_sent.append(f'{{\color{{red}}{word}}}$_{{\color{{gray}}{score:.3f}}}$')
                    else:
                        answer_selection_content_per_doc_sent.append(f'{word}$_{{\color{{gray}}{score:.3f}}}$')

            # Mean of all vectors
            answer_selection_tf_idf_full_sqad_vector = np.mean(as_tf_idf_full_sqad_vec, axis=0)
            answer_selection_tf_idf_per_doc_vector = np.mean(as_tf_idf_per_doc_vec, axis=0)
            answer_selection_fasttext_vec = np.mean(answer_selection_fasttext_vec, axis=0)

            # Computing similar sentences within document
            for idx, sent_and_phrs in enumerate(get_content(kb.url2doc.get(record.text)['text'], vocabulary, old=False)):
                if idx != record.text_answer_position:
                    vec_tf_idf = []
                    vec_fasttext = []
                    vec_tf_idf_per_doc = []
                    similar_sentence_content_full_sqad = []
                    similar_sentence_content_per_doc = []
                    similar_sentence_content_fastext = []
                    similar_sentence_content_full_sqad_sent = []
                    similar_sentence_content_per_doc_sent = []

                    similar_sentence_jaccard = set()

                    for token in sent_and_phrs['sent']:
                        w_tf_idf = tf_idf_full_sqad[record.text][token['lemma']]
                        vec_tf_idf.append([y * w_tf_idf for y in token['v300']])

                        w_per_doc_tf_idf = tf_idf_per_doc[record.text][token['lemma']]
                        vec_tf_idf_per_doc.append([y * w_per_doc_tf_idf for y in token['v300']])

                        vec_fasttext.append(token['v300'])
                        similar_sentence_jaccard = similar_sentence_jaccard.union(get_n_grams(token['lemma'], length))

                        if export_tex:
                            similar_sentence_content_per_doc.append((token["word"], w_per_doc_tf_idf))
                            similar_sentence_content_full_sqad.append((token["word"], w_tf_idf))
                            similar_sentence_content_fastext.append(f'{token["word"]}')

                    if export_tex:
                        focus_ss_full_sqad = max(similar_sentence_content_full_sqad, key=lambda item: item[1])[0]
                        for word, score in similar_sentence_content_full_sqad:
                            if word == focus_ss_full_sqad:
                                similar_sentence_content_full_sqad_sent.append(f'{{\color{{red}}{word}}}$_{{\color{{gray}}{score:.3f}}}$')
                            else:
                                similar_sentence_content_full_sqad_sent.append(f'{word}$_{{\color{{gray}}{score:.3f}}}$')

                        focus_ss_per_doc = max(similar_sentence_content_per_doc, key=lambda item: item[1])[0]
                        for word, score in similar_sentence_content_per_doc:
                            if word == focus_ss_per_doc:
                                similar_sentence_content_per_doc_sent.append(f'{{\color{{red}}{word}}}$_{{\color{{gray}}{score:.3f}}}$')
                            else:
                                similar_sentence_content_per_doc_sent.append(f'{word}$_{{\color{{gray}}{score:.3f}}}$')

                    similar_sentence_tf_idf_vector = np.mean(vec_tf_idf, axis=0)
                    similar_sentence_tf_idf_per_doc_vector = np.mean(vec_tf_idf_per_doc, axis=0)
                    similar_sentence_fasttext_vector = np.mean(vec_fasttext, axis=0)

                    cos_similarity_tf_idf_full_sqad = 1 - spatial.distance.cosine(answer_selection_tf_idf_full_sqad_vector, similar_sentence_tf_idf_vector)
                    cos_similarity_tf_idf_per_doc = 1 - spatial.distance.cosine(answer_selection_tf_idf_per_doc_vector, similar_sentence_tf_idf_per_doc_vector)
                    cos_similarity_fasttext = 1 - spatial.distance.cosine(answer_selection_fasttext_vec, similar_sentence_fasttext_vector)

                    try:
                        jaccard_distance = len(similar_sentence_jaccard.intersection(answer_selection_jaccard)) / len(similar_sentence_jaccard.union(answer_selection_jaccard))
                    except ZeroDivisionError:
                        jaccard_distance = 0
                    # Filter exact answers
                    similar_senteces.append({'idx': idx,
                                             'cos_similarity_tf_idf_full_sqad': cos_similarity_tf_idf_full_sqad,
                                             'cos_similarity_tf_idf_per_doc': cos_similarity_tf_idf_per_doc,
                                             'cos_similarity_fasttext': cos_similarity_fasttext,
                                             'jaccard_distance': jaccard_distance,
                                             'answer_selection_content_full_sqad': re.sub('%', r'\%', ' '.join(answer_selection_content_full_sqad_sent)),
                                             'answer_selection_content_per_doc': re.sub('%', r'\%', ' '.join(answer_selection_content_per_doc_sent)),
                                             'answer_selection_content_fastext': re.sub('%', r'\%', ' '.join(answer_selection_content_fastext)),
                                             'answer_selection_content_jaccard': re.sub('%', r'\%', ' '.join(answer_selection_content_jaccard)),
                                             'similar_sentence_content_full_sqad': re.sub('%', r'\%', ' '.join(similar_sentence_content_full_sqad_sent)),
                                             'similar_sentence_content_per_doc': re.sub('%', r'\%', ' '.join(similar_sentence_content_per_doc_sent)),
                                             'similar_sentence_content_fastext': re.sub('%', r'\%', ' '.join(similar_sentence_content_fastext)),
                                             'similar_sentence_content_jaccard': re.sub('%', r'\%', ' '.join(similar_sentence_content_fastext)),
                                             })
    except Exception as e:
        sys.stderr.write(f'Someting went wrong: {e} in r:{record.rec_id} url:{record.text}\n')
        sys.stderr.write(f'{traceback.format_exc()}\n')
    yield similar_senteces


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Similar senteces using TF-IDF')
    parser.add_argument('-n', '--number', type=int,
                        required=False, default=100,
                        help='Number of previous sentences used for searching similar sentences.'
                             ' For all similar sentences put "0".')
    parser.add_argument('-u', '--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('-p', '--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-d', '--db_path', type=str,
                        required=False, default='',
                        help='Database path')
    parser.add_argument('--sort_orig', action='store_true',
                        required=False, default=False,
                        help='Sort according cosine similarity on non enhanced vectors')
    parser.add_argument('-j', '--jaccard_n_gram_len', type=int,
                        required=False, default=3,
                        help='Jaccard n-gram length')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False,
                        help='Verbose mode')
    parser.add_argument('--export_tex', action='store_true',
                        required=False,
                        help='Export tech tables (not storing to databse)')

    args = parser.parse_args()

    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    try:
        sys.stderr.write('Loading DB\n')
        vocabulary, _, kb = db.get_dicts()

        sys.stderr.write('Precomputing tf-idf\n')
        # Opening stored tf-idf
        if path.exists(f"{dir_path}/tf_idf_full_sqad.p"):
            tf_idf_per_full_sqad = pickle.load(open(f"{dir_path}/tf_idf_full_sqad.p", "rb"))
        else:
            tf_idf_per_full_sqad = compute_tfidf(kb, vocabulary)
            pickle.dump(tf_idf_per_full_sqad, open(f"{dir_path}/tf_idf_full_sqad.p", "wb"))

        if path.exists(f"{dir_path}/tf_idf_per_doc.p"):
            tf_idf_per_doc = pickle.load(open(f"{dir_path}/tf_idf_per_doc.p", "rb"))
        else:
            tf_idf_per_doc = compute_tfidf_per_doc(kb, vocabulary)
            pickle.dump(tf_idf_per_doc, open(f"{dir_path}/tf_idf_per_doc.p", "wb"))

        sys.stderr.write('Computing similar sentences\n')
        for rid in db.get_all_records_id():
            record = db.get_record(rid)

            o_files = [{'file': f'{dir_path}/sim_sentences_data/{rid}_tf_idf_full.tex',
                        'sim': 'cos_similarity_tf_idf_full_sqad',
                        'as': 'answer_selection_content_full_sqad',
                        'ss': 'similar_sentence_content_full_sqad',
                        'flag': 'full',
                        'name': 'tf_idf_full'
                        },

                       {'file': f'{dir_path}/sim_sentences_data/{rid}_fasttext.tex',
                        'sim': 'cos_similarity_fasttext',
                        'as': 'answer_selection_content_fastext',
                        'ss': 'similar_sentence_content_fastext',
                        'flag': 'fasttext',
                        'name': 'fasttext'
                        },

                       {'file': f'{dir_path}/sim_sentences_data/{rid}_tf_idf_per_doc.tex',
                        'sim': 'cos_similarity_tf_idf_per_doc',
                        'as': 'answer_selection_content_per_doc',
                        'ss': 'similar_sentence_content_per_doc',
                        'flag': 'doc',
                        'name': 'tf_idf_doc'
                        },

                       {'file': f'{dir_path}/sim_sentences_data/{rid}_jaccard.tex',
                        'sim': 'jaccard_distance',
                        'as': 'answer_selection_content_jaccard',
                        'ss': 'similar_sentence_content_jaccard',
                        'flag': f'jaccard (len: {args.jaccard_n_gram_len})',
                        'name': 'jaccard'
                        }
                       ]

            for similar_sentences in find_similar_senteces(vocabulary, kb, tf_idf_per_full_sqad, tf_idf_per_doc, record, args.jaccard_n_gram_len, args.export_tex):
                for part in o_files:
                    sorted_sim_sentences = sorted(similar_sentences, key=lambda x: x[part['sim']], reverse=True)
                    if args.export_tex:
                        with open(part['file'], 'w') as f:
                            f.write('\\documentclass[12pt,margin=0cm,innermargin=0cm,blockverticalspace=0cm]{beamer}\n')
                            f.write('\\geometry{paperwidth=42cm,paperheight=300cm}')
                            f.write('\\usepackage[utf8]{inputenc}\n')
                            f.write('\\usepackage[T1]{fontenc}\n')
                            f.write('\\usepackage{xcolor}\n')
                            f.write('\\begin{document}\n')
                            f.write('\\begin{table}[t]\n')
                            f.write('\\centering\n')
                            f.write('\\caption{' + f'{rid} ' + f'{part["flag"]}' + '}\n')
                            f.write('\\begin{tabular}{r|r|p{36cm}}\n')
                            f.write(f'& & {{\\bf {sorted_sim_sentences[0][part["as"]]} }} \\\\ \n')
                            f.write('\\hline\n')
                            f.write('{\\bf id} & {\\bf sim} & {\\bf similar sentece} \\\\ \n')
                            f.write('\\hline\n')
                            for item in sorted_sim_sentences:
                                f.write(f'{item["idx"]} & {item[part["sim"]]:.3f} & {item[part["ss"]]} \\\\ \n')
                                f.write('\\hline\n')
                            f.write('\\end{tabular}\n')
                            f.write('\\end{table}\n')
                            f.write('\\end{document}\n')
                    else:
                        record.similar_answers[f'{part["name"]}'] = persistent.list.PersistentList([(x["idx"], x[part["sim"]]) for x in sorted_sim_sentences])
                        db._p_changed = True
                        transaction.commit()

        db.update()
        db._p_changed = True
        transaction.commit()
        db.close()
        db.close()
    except KeyboardInterrupt:
        db.close()
        sys.exit()


if __name__ == "__main__":
    main()
